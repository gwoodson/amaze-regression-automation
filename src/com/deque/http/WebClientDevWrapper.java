package com.deque.http;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * This code is public domain: you are free to use, link and/or modify it in any
 * way you want, for all purposes including commercial applications.
 * 
 * @see http://javaskeleton.blogspot.com/2010/07/avoiding-peer-not-authenticated-with.html
 * @author Simon and Mathias
 */
public class WebClientDevWrapper {

	/**
	 * Create a HttpClient which doesn't care about SSL certs
	 * 
	 * @param base
	 * @return
	 */
	public static HttpClient wrapClient(HttpClient base) {

		try {

			ClientConnectionManager ccm = base.getConnectionManager();

			ccm.getSchemeRegistry().register(
					new Scheme("https", 443, new SSLSocketFactory(
							new TrustSelfSignedStrategy(),
							SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)));

			return new DefaultHttpClient(ccm, base.getParams());

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
