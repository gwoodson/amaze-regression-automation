/**
 * 
 */
package com.deque.junit;

import static org.junit.Assert.*;

import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;

/**
 * Custom JUnit-style assertions
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class Assert {

	/**
	 * 
	 */
	public Assert() {

		// TODO Auto-generated constructor stub
	}

	public static void pass(String message) {

		assertTrue(message, true);
	}

	/**
	 * Assert that two WebElements are identical
	 * 
	 * @param actual
	 * @param expected
	 */
	public static void assertWebElement(WebElement actual, WebElement expected) {

		assertEquals(PrettyPrint.element(actual), PrettyPrint.element(expected));

		assertEquals(actual, expected);

	}

	/**
	 * Assert that two WebElements are identical
	 * 
	 * @param message
	 * @param actual
	 * @param expected
	 */
	public static void assertWebElement(String message, WebElement actual,
			WebElement expected) {

		assertEquals(message, PrettyPrint.element(actual),
				PrettyPrint.element(expected));

		assertEquals(message, actual, expected);

	}

}
