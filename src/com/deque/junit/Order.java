/**
 * 
 */
package com.deque.junit;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 
 * @see http://stackoverflow.com/questions/3089151/specifying-an-order
 *      -to-junit-4-tests-at-the-method-level-not-class-level
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Order {
	public int order();
}
