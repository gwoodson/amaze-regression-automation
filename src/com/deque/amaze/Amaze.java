/**
 * 
 */
package com.deque.amaze;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.deque.http.WebClientDevWrapper;

/**
 * A simple Amaze representation
 * 
 * TODO write tests -- don't be lazy
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class Amaze {

	private static Logger logger = Logger.getLogger(Amaze.class.getName());

	private JSONObject sites = null;
	private String runtime = null;

	private String server;
	private String sitesApi;
	private String runtimeApi;
	private String bclApi;
	private String overlaysApi;

	private final String EXTENSION_IFFE = "var amazeFlags = { extension: true };"
			+ "/* runtime */ %s \n"
			+ "/* bcl */ %s \n"
			+ "/* overlays */ setTimeout(function () { %s }, 0);";

	/**
	 * Get the sites object from the sites API
	 * 
	 * @return The sites
	 * @throws JSONException
	 * @throws IOException
	 */
	public JSONObject getSites() throws JSONException, IOException {

		if (this.sites == null) {
			String _sites = this.request(this.getSitesApi());

			logger.log(Level.INFO, String.format("Sites: %s", _sites));

			JSONObject sites = new JSONObject(_sites);
			this.setSites(sites);
		}

		return this.sites;

	}

	/**
	 * Set the sites
	 * 
	 * @param sites
	 *            The sites
	 * @throws JSONException
	 */
	public void setSites(JSONObject sites) throws JSONException {

		//logger.log(Level.INFO, sites.toString(4));

		this.sites = sites;
		this.setRuntime();

	}

	/**
	 * Set the runtime library from <code>this.sites</code>
	 * 
	 * @throws JSONException
	 */
	public void setRuntime() throws JSONException {

		String runtime = this.sites.getString("runtime");

		this.runtime = runtime;

	}

	/**
	 * Set the runtime library
	 * 
	 * @param runtime
	 *            The runtime library
	 */
	public void setRuntime(String runtime) {

		this.runtime = runtime;
	}

	/**
	 * Get the runtime library. If it has not been set, attempt to retrieve it
	 * from the <code>this.sites</code>.
	 * 
	 * @return
	 * @throws JSONException
	 */
	public String getRuntime() throws JSONException {

		if (this.runtime == null) {
			this.setRuntime();
		}

		return this.runtime;

	}

	/**
	 * @return The server URL
	 */
	public String getServer() {

		return this.server;

	}

	/**
	 * Set the server URL. Server URL must:
	 * <ul>
	 * <li>must be secure (HTTPS)</li>
	 * <li>must end with <code>/Amaze</code></li>
	 * </ul>
	 * 
	 * Will also set the other APIs which are dependent on the server URL.
	 * 
	 * TODO above validation
	 * 
	 * @param server
	 * @throws IOException
	 * @throws JSONException
	 */
	public void setServer(String server) throws JSONException, IOException {

		this.server = server;
		this.setRuntimeApi(server);
		this.setBclApi(server);
		this.setOverlaysApi(server);
		this.setSitesApi(server);
		this.getSites();

	}

	/**
	 * Get the sites API
	 * 
	 * @return The sites API
	 */
	public String getSitesApi() {

		return this.sitesApi;

	}

	/**
	 * Set the sites API
	 * 
	 * @param server
	 *            The server URL
	 */
	public void setSitesApi(String server) {

		this.sitesApi = server + "sites";

	}

	/**
	 * Get the runtime API
	 * 
	 * @return The runtime API
	 */
	public String getRuntimeApi() {

		return this.runtimeApi;
	}

	/**
	 * Set the runtime API
	 * 
	 * @param server
	 *            The server URL
	 */
	public void setRuntimeApi(String server) {

		this.runtime = server + "amazeme";

	}

	/**
	 * Get the BCL API
	 * 
	 * @return The BCL API
	 */
	public String getBclApi() {
		
		return this.bclApi;

	}

	/**
	 * Set the BCL API
	 * 
	 * @param server
	 *            The server URL
	 */
	public void setBclApi(String server) {

		this.bclApi = server + "bcl.js";

	}

	/**
	 * Get the overlays API
	 * 
	 * @return The overlays API
	 */
	public String getOverlaysApi() {

		return this.overlaysApi;

	}

	/**
	 * Set the overlays API
	 * 
	 * @param server
	 *            The server URL
	 */
	public void setOverlaysApi(String server) {

		this.overlaysApi = server + "overlays.js";

	}

	/**
	 * Construct a new Amaze instance
	 */
	public Amaze() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * Construct a new Amaze instance with the server URL predefined
	 * 
	 * @param server
	 *            The server URL
	 * @throws IOException
	 * @throws JSONException
	 */
	public Amaze(String server) throws JSONException, IOException {

		this.setServer(server);

	}

	/**
	 * Make a HTTP request, returning its body
	 * 
	 * @param url
	 *            The URL to request
	 * @return The response body
	 * @throws IOException
	 */
	private String request(String url) throws IOException {

		logger.log(Level.INFO, String.format("Requesting '%s'", url));

		HttpClient client = WebClientDevWrapper
				.wrapClient(new DefaultHttpClient());

		HttpGet get = new HttpGet(url);

		ResponseHandler<String> handler = new BasicResponseHandler();

		String result = client.execute(get, handler);

		return result;

	}

	/**
	 * Make the overlays API call, returning its response.
	 * 
	 * 
	 * <pre>
	 * <code>
	 * Amaze amaze = new Amaze("https://publicsites.dequeamaze.com/Amaze");
	 * 
	 * String overlays = amaze.getOverlays("facebook.com", "/");
	 * </code>
	 * </pre>
	 * 
	 * @param hostname
	 *            The hostname
	 * @param path
	 *            The path
	 * @return The overlays
	 * @throws IOException
	 */
	public String getOverlays(String hostname, String path) throws IOException {

		String api = String.format("%s?hostname=%s&path=%s",
				this.getOverlaysApi(), hostname, path);

		return this.request(api);

	}

	/**
	 * Get the BCL for a specific site. Assumes the sites API has been fetched
	 * and cached.
	 * 
	 * @param hostname
	 *            The hostname of the specific site
	 * @return The BCL
	 * @throws JSONException
	 * @throws SiteException
	 * @throws IOException
	 */
	public String getBcl(String hostname) throws JSONException, SiteException,
			IOException {
		
		if (this.sites == null) {
			this.getSites();
		}
		
		JSONArray sites = this.sites.getJSONArray("sites");
		
		/// leave the server alone and handle this logic here
		for (int index = 0; index < sites.length(); index += 1) {
			
			Site site = new Site(sites.getJSONObject(index));			
			
			if (site.matches(hostname)) {
				return site.getBcl();
			}
		}
		
		return null;

	}

	/**
	 * Get the BCL for a specific site. When <code>useApi</code> is
	 * <code>true</code>, will query the server for the BCL.
	 * 
	 * @param hostname
	 *            The hostname of the specific site
	 * @param useApi
	 *            Flag to query the server
	 * @return The BCL
	 * @throws SiteException
	 * @throws JSONException
	 * @throws IOException
	 */
	public String getBcl(String hostname, Boolean useApi) throws JSONException,
			SiteException, IOException {

		if (!useApi) {
			return this.getBcl(hostname);
		}

		String bcl = this.request(String.format("%s?hostname=%s",
					this.getBclApi(), hostname));

		return bcl;

	}

	/**
	 * Get the extension-style IFFE to inject into the DOM
	 * 
	 * @param hostname
	 *            The URL hostname
	 * @param path
	 *            The URL path
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 * @throws SiteException
	 */
	public String getExtensionIffe(String hostname, String path)
			throws JSONException, IOException, SiteException {
		
		String iffe = String.format(this.EXTENSION_IFFE, this.getRuntime(),
				this.getOverlays(hostname, path), this.getBcl(hostname));		
				
		return iffe;

	}

}
