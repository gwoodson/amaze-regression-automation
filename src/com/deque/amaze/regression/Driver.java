/**
 * 
 */
package com.deque.amaze.regression;

import java.net.MalformedURLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;

/**
 * Wrapper for {@link org.openqa.selenium.WebDriver} for running Amaze
 * regression tests
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public interface Driver {

	/**
	 * Wait for a JavaScript condition to result to exactly true.
	 * 
	 * @param condition
	 *            The JavaScript condition
	 */
	public void waitForJavascriptCondition(String condition);

	/**
	 * Wait for a JavaScript condition to result to exactly true
	 * 
	 * @param condition
	 *            The JavaScript condition
	 * @param timeout
	 *            The amount of time to wait before giving up
	 */
	public void waitForJavascriptCondition(String condition, long timeout);

	/**
	 * Inject Amaze into the page by adding a script tag to the DOM
	 */
	public void injectAmaze();

	/**
	 * Inject Amaze as the extension-style include (IFFE)
	 * 
	 * @param hostname
	 *            The current page's hostname (cats.com)
	 * @param path
	 *            The current page's path (/cats/dogs?eating=bananas)
	 * @throws Exception
	 */
	public void injectAmazeAsExtension(String hostname, String path)
			throws Exception;

	/**
	 * Inject an arbitrary script into the page
	 * 
	 * @param src
	 *            The location of the script
	 */
	public void injectScriptTag(String src);

	/**
	 * Find an element of the DOM. A wrapper for
	 * {@link WebDriver#findElement(By)}
	 * 
	 * @param selector
	 *            The selector
	 * @return
	 */
	public WebElement findElement(By selector);

	/**
	 * Find multiple elements of the DOM. A wrapper for
	 * {@link WebDriver#findElements(By)}
	 * 
	 * @param selector
	 *            The selector
	 * @return
	 */
	public List<WebElement> findElements(By selector);

	/**
	 * Wait for an element to become visible for 30 seconds
	 * 
	 * @param selector
	 *            The element selector
	 * @return The element
	 */
	public WebElement waitForElementVisible(final By selector);

	/**
	 * Wait for an element to become visible for a specified number of seconds
	 * 
	 * @param selector
	 *            The element selector
	 * @param timeout
	 *            The number of seconds to wait
	 * @return The element
	 */
	public WebElement waitForElementVisible(final By selector, long timeout);

	/**
	 * Wait for an element to become visible
	 * 
	 * @param element
	 *            The element
	 * @return The element
	 */
	public WebElement waitForElementVisible(final WebElement element);

	/**
	 * Wait for an element to become visible
	 * 
	 * @param element
	 *            The element
	 * @param timeout
	 *            The number of seconds to wait
	 * @return
	 */
	public WebElement waitForElementVisible(final WebElement element,
			long timeout);

	/**
	 * Wait for an element to stop being visible
	 * 
	 * @param selector
	 *            The element selector
	 * @return The element
	 */
	public WebElement waitForElementHidden(final By selector);

	/**
	 * Wait for an element to stop being visible
	 * 
	 * @param element
	 *            The element
	 * @return The element
	 */
	public WebElement waitForElementHidden(final WebElement element);

	/**
	 * Wait for an element to stop being visible
	 * 
	 * @param element
	 *            The element
	 * @param timeout
	 *            The number of seconds to wait
	 * @return The element
	 */
	public WebElement waitForElementHidden(final WebElement element,
			long timeout);

	/**
	 * Wait for an element to stop being visible for a specified number of
	 * seconds
	 * 
	 * @param selector
	 *            The element selector
	 * @param timeout
	 *            The number of seconds to wait
	 * @return The element
	 */
	public WebElement waitForElementHidden(final By selector, long timeout);

	/**
	 * Execute some arbitrary JavaScript
	 * 
	 * @param javascript
	 *            The JavaScript
	 * @return The result of the executed script
	 */
	public Object executeJavascript(String javascript);

	/**
	 * Get the current status of Amaze
	 * 
	 * @return
	 */
	public String getAmazeStatus();

	/**
	 * Navigate to a URL if we're not already there
	 * 
	 * @param url
	 *            The URL
	 * @throws MalformedURLException
	 *             When a bad URL is provided
	 */
	public void navigateTo(String url) throws MalformedURLException;

	/**
	 * Get the active (currently focused) element. Wraps
	 * {@link WebDriver#switchTo().activeElement()}
	 * 
	 * @return The active element
	 */
	public WebElement getActiveElement();

	/**
	 * Get the current URL of the driver
	 * 
	 * @return The current URL
	 */
	public String getCurrentUrl();

	/**
	 * Get the current title of the page
	 * 
	 * @return The page title
	 */
	public String getTitle();

}
