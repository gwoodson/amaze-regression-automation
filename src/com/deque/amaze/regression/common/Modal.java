/**
 * 
 */
package com.deque.amaze.regression.common;

import static com.deque.junit.Assert.assertWebElement;
import static com.deque.junit.Assert.pass;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.RegressionDriver;

/**
 * Generic modal verification class. Performs the following tests:
 * <ul>
 * <li>A "trigger" opens it</li>
 * <li>User may not <code>TAB</code> out of the modal</li>
 * <li>Focus is automatically set within the modal</li>
 * <li>Pressing <code>ESCAPE</code> closes the modal</li>
 * <li>When the modal is closed, focus is returned to the element which
 * instantiated it</li>
 * </ul>
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class Modal {

	private static Logger logger = Logger.getLogger(Modal.class.getName());

	private RegressionDriver driver;

	private WebElement trigger;
	private WebElement close;
	private WebElement modal;

	private By modalSelector;
	private By closeSelector;

	private int maxTabs;

	/**
	 * @return the driver
	 */
	public RegressionDriver getDriver() {

		return driver;
	}

	/**
	 * @param driver
	 *            the driver to set
	 */
	public void setDriver(RegressionDriver driver) {

		this.driver = driver;
	}

	/**
	 * @return the trigger
	 */
	public WebElement getTrigger() {

		return trigger;
	}

	/**
	 * @param trigger
	 *            the trigger to set
	 */
	public void setTrigger(WebElement trigger) {

		this.trigger = trigger;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {

		return close;
	}

	/**
	 * @param close
	 *            the close to set
	 */
	public void setClose(WebElement close) {

		this.close = close;
	}

	/**
	 * @return the modal
	 */
	public WebElement getModal() {

		return modal;
	}

	/**
	 * @param close
	 *            the close to set
	 */
	public void setModal(WebElement modal) {

		this.modal = modal;
	}

	/**
	 * @return the modalSelector
	 */
	public By getModalSelector() {

		return modalSelector;
	}

	/**
	 * @param modalSelector
	 *            the modalSelector to set
	 */
	public void setModalSelector(By modalSelector) {

		this.modalSelector = modalSelector;
	}

	/**
	 * @return the closeSelector
	 */
	public By getCloseSelector() {

		return closeSelector;
	}

	/**
	 * @param closeSelector
	 *            the closeSelector to set
	 */
	public void setCloseSelector(By closeSelector) {

		this.closeSelector = closeSelector;
	}

	/**
	 * @return the maxTabs
	 */
	public int getMaxTabs() {

		return maxTabs;
	}

	/**
	 * @param maxTabs
	 *            the maxTabs to set
	 */
	public void setMaxTabs(int maxTabs) {

		this.maxTabs = maxTabs;
	}

	/**
	 * Generic constructor for modal verification. Will default to 50 maximum
	 * tabs.
	 * 
	 * @param driver
	 *            The RegressionDriver
	 * @param trigger
	 *            The trigger selector (what opens the modal)
	 * @param modal
	 *            The modal selector (the modal itself)
	 * @param close
	 *            The modal's close button selector
	 * @throws InterruptedException
	 * @throws TimeoutException
	 */
	public Modal(RegressionDriver driver, WebElement trigger, By modal, By close)
			throws TimeoutException, InterruptedException {

		this.setDriver(driver);
		this.setTrigger(trigger);
		this.setModalSelector(modal);
		this.setCloseSelector(close);
		this.setMaxTabs(50);

	}

	/**
	 * Generic constructor for modal verification.
	 * 
	 * @param driver
	 *            The RegressionDriver
	 * @param trigger
	 *            The trigger selector (what opens the modal)
	 * @param modal
	 *            The modal selector (the modal itself)
	 * @param close
	 *            The modal's close button selector
	 * @param maxTabs
	 *            The maximum number of allowable tabs
	 * @throws InterruptedException
	 * @throws TimeoutException
	 */
	public Modal(RegressionDriver driver, WebElement trigger, By modal,
			By close, int maxTabs) throws TimeoutException,
			InterruptedException {

		this.setDriver(driver);
		this.setTrigger(trigger);
		this.setModalSelector(modal);
		this.setCloseSelector(close);
		this.setMaxTabs(maxTabs);

	}

	/**
	 * Run all assertions
	 * 
	 * @throws TimeoutException
	 * @throws InterruptedException
	 */
	public void verify() throws TimeoutException, InterruptedException {

		this.triggerOpensModal();
		this.autoFocusModal();
		this.circularTabbing();
		this.escapeClosesModal();

	}

	/**
	 * Verify that the trigger opens the modal when <code>ENTER</code> is
	 * pressed. Will throw if it doesn't.
	 * 
	 * @throws TimeoutException
	 */
	private void triggerOpensModal() throws TimeoutException {

		// send the keyboard RETURN event
		this.trigger.sendKeys(Keys.ENTER);

		logger.log(Level.INFO, "clicked modal trigger");

		// wait for the modal to open
		WebElement modal = this.driver
				.waitForElementVisible(this.modalSelector);

		logger.log(Level.INFO, "modal: " + PrettyPrint.element(modal));

		// store it; we need it later
		this.setModal(modal);

		// just so JUnit reports it
		pass("ENTER on trigger opens modal");

	}

	/**
	 * Focus should automatically be set <b>SOMEWHERE</b> within the modal, or the modal itself
	 */
	private void autoFocusModal() {

		// get the currently active (focused) element
		WebElement activeElement = this.driver.getActiveElement();

		if (this.modal.equals(activeElement)) {
			pass("Focus is set within the modal");
			return;
		}
		
		// make a selector from the element
		String activeSelector = PrettyPrint.element(activeElement);

		logger.log(Level.INFO, "active: " + activeSelector);

		// ensure the active element is contained within the modal (will throw
		// if not)
		this.modal.findElement(By.cssSelector(activeSelector));

		pass("Focus is set within modal");
	}

	/**
	 * Verify that a user is unable to TAB out of the modal
	 */
	private void circularTabbing() {

		WebElement start = this.modal.findElement(this.getCloseSelector());

		int counter = 0;
		int max = this.getMaxTabs();

		while (true) {
			logger.log(Level.INFO, String.format("tab #%d", counter));

			WebElement current = this.driver.getActiveElement();

			logger.log(
					Level.INFO,
					String.format("Active (current): %s",
							PrettyPrint.element(current)));

			current.sendKeys(Keys.TAB);

			WebElement next = this.driver.getActiveElement();

			logger.log(
					Level.INFO,
					String.format("tab from %s to %s",
							PrettyPrint.element(current),
							PrettyPrint.element(next)));

			if (next.equals(start)) {
				assertWebElement(this.driver.getActiveElement(), start);
				break;
			}

			counter += 1;

			if (counter > max) {
				fail("Tabbed too many times");
			}

		}

		pass("Circular tabbing works");

	}

	/**
	 * Verify that pressing <code>ESCAPE</code> within the modal closes the
	 * modal and returns focus to the trigger element.
	 * 
	 * @throws InterruptedException
	 */
	private void escapeClosesModal() throws InterruptedException {

		WebElement active = driver.getActiveElement();

		logger.log(
				Level.INFO,
				String.format("sending ESCAPE from %s",
						PrettyPrint.element(active)));

		active.sendKeys(Keys.ESCAPE);

		try {
			// wait for the modal to close
			// will throw if it doesn't
			this.driver.waitForElementHidden(this.modal);

		} catch (StaleElementReferenceException e) {
			pass("the modal closed");
		}

		// sleep for a moment; let the JS fire
		Thread.sleep(500);

		active = driver.getActiveElement();

		assertWebElement("focus should be returned to the trigger", active,
				this.trigger);

	}

}
