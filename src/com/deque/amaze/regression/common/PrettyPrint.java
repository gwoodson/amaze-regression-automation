/**
 * 
 */
package com.deque.amaze.regression.common;

import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Pretty print some stuff
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class PrettyPrint {

	private final static String BY_ID = "By.id: ";
	private final static String BY_CLASSNAME = "By.className: ";
	private final static String BY_TAGNAME = "By.tagName: ";
	private final static String BY_SELECTOR = "By.selector: ";
	private final static String BY_XPATH = "By.xpath: ";

	private final static String DOT = ".";
	private final static String HASH = "#";
	private final static String CLASS = "class";;
	private final static String ID = "id";
	private final static String SPACE = " ";
	private final static String ROLE = "role";
	private final static String TABINDEX = "tabindex";
	private final static String ACTION = "action";
	private final static String METHOD = "method";
	private final static String TYPE = "type";
	private final static String REL = "rel";
	private final static String ATTRIBUTE = "[%s=\"%s\"]";

	private final static String FORM = "form";
	private final static String A = "a";

	private final static String[] INPUTS = {
			"input", "button"
	};

	/**
	 * 
	 */
	public PrettyPrint() {

		// TODO Auto-generated constructor stub
	}

	public static Boolean isInput(String tagName) {

		return Arrays.asList(INPUTS).contains(tagName);
	}

	/**
	 * Verify a String is ok
	 * 
	 * @param value
	 * @return
	 */
	private static Boolean ok(String value) {

		return value != null && !value.isEmpty();
	}

	/**
	 * Pretty print a WebElement. Creates a CSS-like selector of the element
	 * using its class, id and tagName.
	 * 
	 * TODO handle more attributes (<code>[foo="bar"]</code>)
	 * 
	 * @param webElement
	 * @return
	 */
	public static String element(WebElement webElement) {

		String tagName = webElement.getTagName().toLowerCase();
		String result = tagName;

		String id = webElement.getAttribute(ID);
		// protect against both null and empty strings
		if (id != null && !id.isEmpty()) {
			result += HASH + id;
		}

		// div.class.stuff
		String className = webElement.getAttribute(CLASS);
		if (ok(className)) {
			// get all classes
			String[] classList = className.split(" ");

			// iterate through the classes
			for (String theClass : classList) {
				// remove superfluous whitespace
				theClass = theClass.trim();

				// if we've got a valid class, add it
				if (ok(theClass)) {
					result += DOT + theClass;
				}
			}

		}

		// div[role="dialog"]
		String role = webElement.getAttribute(ROLE);
		if (ok(role)) {
			result += String.format(ATTRIBUTE, ROLE, role);
		}

		// div[tabindex="-1"]
		String tabindex = webElement.getAttribute(TABINDEX);
		if (ok(tabindex)) {
			result += String.format(ATTRIBUTE, TABINDEX, tabindex);
		}

		// form
		if (tagName.equals(FORM)) {
			// form[action="/foo/bar/baz"]
			String action = webElement.getAttribute(ACTION);
			if (ok(action)) {
				result += String.format(ATTRIBUTE, ACTION, action);
			}

			// form[method="POST"]
			String method = webElement.getAttribute(METHOD);
			if (ok(method)) {
				result += String.format(ATTRIBUTE, METHOD, method);
			}

		}

		// input[type="password"]
		if (isInput(tagName)) {
			String type = webElement.getAttribute(TYPE);
			// valid type?
			if (ok(type)) {
				// add it
				result += String.format(ATTRIBUTE, TYPE, type);
			}

		}

		// a[rel="nofollow"]
		if (tagName.equals(A)) {
			String rel = webElement.getAttribute(REL);
			if (ok(rel)) {
				result += String.format(ATTRIBUTE, REL, rel);
			}
		}

		return result;

	}

	/**
	 * Pretty print a By selector.
	 * 
	 * @param selector
	 *            The selector
	 * @return The "stringified" selector
	 */
	public static String selector(By selector) {

		String tostring = selector.toString();

		/**
		 * TODO the below code is really ugly... this needs to be refactored
		 */

		if (tostring.startsWith(BY_ID)) {
			return tostring.replace(BY_ID, HASH);
		}

		if (tostring.startsWith(BY_CLASSNAME)) {
			tostring = tostring.replace(BY_CLASSNAME, DOT);
			return tostring.replaceAll(SPACE, DOT);
		}

		if (tostring.startsWith(BY_TAGNAME)) {
			return tostring.substring(BY_TAGNAME.length());
		}

		if (tostring.startsWith(BY_XPATH)) {
			return tostring.substring(BY_XPATH.length());
		}

		if (tostring.startsWith(BY_SELECTOR)) {
			return tostring.substring(BY_SELECTOR.length());
		}

		return selector.toString();
	}

}
