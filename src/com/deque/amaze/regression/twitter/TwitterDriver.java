/**
 * 
 */
package com.deque.amaze.regression.twitter;

import java.io.IOException;
import java.util.Properties;

import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.RegressionDriver;

/**
 * Regression driver specific for Twitter
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class TwitterDriver extends RegressionDriver {

	private final By email = By.id("signin-email");

	private final By password = By.id("signin-password");

	/**
	 * @param options
	 * @throws IOException
	 * @throws JSONException
	 */
	public TwitterDriver(Properties options) throws JSONException, IOException {

		super(options, "https://twitter.com");
	}

	/**
	 * @param options
	 * @param driver
	 * @throws IOException
	 * @throws JSONException
	 */
	public TwitterDriver(Properties options, WebDriver driver)
			throws JSONException, IOException {

		super(options, "https://twitter.com", driver);
	}

	/**
	 * Login to Twitter using the username and password specified by
	 * {@link RegressionDriver#getOptions()}
	 */
	public void login() {

		String username = super.getOption("twitter.username");
		String password = super.getOption("twitter.password");

		super.findElement(this.email).sendKeys(username);

		WebElement passwordInput = super.findElement(this.password);

		passwordInput.sendKeys(password);
		passwordInput.submit();

	}

	/**
	 * Check if Twitter has redirected us to the reCaptcha page
	 * 
	 * @return
	 */
	public Boolean isReCaptcha() {

		try {
			WebElement recaptcha = super.findElement(By
					.id("recaptcha_widget_div"));

			// for unit tests
			if (recaptcha == null) {
				return false;
			}

			return true;

		} catch (NoSuchElementException e) {
			return false;
		}
	}

	/**
	 * Wait for the page container to be present before doing anything else
	 */
	public void waitForPageContainer() {

		super.waitForElementVisible(By.id("page-container"));
	}

	/**
	 * Twitter has some sort of magic happening when changing URLs. We're
	 * clobbering the native RegressionDriver.navigateTo, which checks the
	 * current URL vs. the newly desired URL and won't navigate if they're the
	 * same. In twitter, we need to refresh the page regardless.
	 * 
	 * @param url The URL to navigate to
	 */
	public void navigateTo(String url) {

		super.getDriver().navigate().to(url);
	}
}
