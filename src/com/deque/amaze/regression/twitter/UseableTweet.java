/**
 * 
 */
package com.deque.amaze.regression.twitter;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class UseableTweet {

	private static Logger logger = Logger.getLogger(UseableTweet.class
			.getName());

	/**
	 * 
	 */
	public UseableTweet() {

	}

	/**
	 * Check if a tweet is expandable
	 * 
	 * @param tweet
	 * @return
	 */
	private static boolean isExpandable(WebElement tweet) {

		try {
			// find the expand button; will throw if it's not there
			WebElement expandLink = tweet.findElement(By
					.cssSelector("a.details.with-icn.js-details"));

			logger.log(Level.INFO,
					String.format("Found expand link: %s", expandLink));

			// expand the tweet; will throw if it can't
			expandLink.sendKeys(Keys.RETURN);

		} catch (Exception e) {
			return false;
		}

		return true;

	}

	private static boolean hasBeenRetweeted(WebElement tweet)
			throws InterruptedException {

		if (!isExpandable(tweet)) {
			return false;
		}

		try {

			Thread.sleep(500);

			// will throw if it can't find the link
			tweet.findElement(By.cssSelector("a.request-retweeted-popup"));
		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}

	private static boolean hasBeenFavorited(WebElement tweet)
			throws InterruptedException {

		if (!isExpandable(tweet)) {
			return false;
		}

		try {

			Thread.sleep(500);

			tweet.findElement(By.cssSelector("a.request-favorited-popup"));
		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}

	private static boolean findtweetByCSS(WebElement tweet, String name)
			throws InterruptedException {

		if (!isExpandable(tweet)) {
			return false;
		}

		try {

			Thread.sleep(500);

			tweet.findElement(By.cssSelector(name));
		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}
	
	private static List<WebElement> findTweets(TwitterDriver driver) {

		return driver.findElements(By.cssSelector("div.tweet"));
	}

	/**
	 * Find an expandable tweet
	 * 
	 * @param driver
	 *            The TwitterDriver
	 * @return
	 */
	public static WebElement expandable(TwitterDriver driver)
			throws NoSuchElementException {

		List<WebElement> tweets = findTweets(driver);

		// iterate through the tweets, checking for an expandable one
		for (int index = (tweets.size() - 1); index >= 0; index--) {
			WebElement tweet = tweets.get(index);

			logger.log(
					Level.INFO,
					String.format("Checking tweet#%d: %s", index,
							PrettyPrint.element(tweet)));

			String author = tweet.getAttribute("data-name").trim();

			logger.log(Level.INFO, String.format("Tweet author: %s", author));

			if (isExpandable(tweet)) {
				return tweet;
			}
		}

		throw new NoSuchElementException(
				"No tweets on the page appear to be exandable.");

	}

	public static WebElement retweeted(TwitterDriver driver) throws Exception {

		List<WebElement> tweets = findTweets(driver);

		// iterate through the tweets, checking for an expandable one
		for (int index = (tweets.size() - 1); index >= 0; index--) {
			WebElement tweet = tweets.get(index);

			logger.log(
					Level.INFO,
					String.format("Checking tweet#%d: %s", index,
							PrettyPrint.element(tweet)));

			String author = tweet.getAttribute("data-name").trim();

			logger.log(Level.INFO, String.format("Tweet author: %s", author));

			if (hasBeenRetweeted(tweet)) {
				return tweet;
			}
		}

		throw new NoSuchElementException(
				"No tweets on the page appear to have been retweeted.");
	}

	public static WebElement favorited(TwitterDriver driver) throws Exception {

		List<WebElement> tweets = findTweets(driver);

		// iterate through the tweets, checking for an expandable one
		for (int index = (tweets.size() - 1); index >= 0; index--) {
			WebElement tweet = tweets.get(index);

			logger.log(
					Level.INFO,
					String.format("Checking tweet#%d: %s", index,
							PrettyPrint.element(tweet)));

			String author = tweet.getAttribute("data-name").trim();

			logger.log(Level.INFO, String.format("Tweet author: %s", author));

			if (hasBeenFavorited(tweet)) {
				return tweet;
			}
		}

		throw new NoSuchElementException(
				"No tweets on the page appear to have been retweeted.");
	}
	
	public static WebElement getTweetByCSSName(TwitterDriver driver, String name) throws Exception {

		List<WebElement> tweets = findTweets(driver);

		// iterate through the tweets, checking for an expandable one
		for (int index = (tweets.size() - 1); index >= 0; index--) {
			WebElement tweet = tweets.get(index);

			logger.log(
					Level.INFO,
					String.format("Checking tweet#%d: %s", index,
							PrettyPrint.element(tweet)));

			String author = tweet.getAttribute("data-name").trim();

			logger.log(Level.INFO, String.format("Tweet author: %s", author));

			if (findtweetByCSS(tweet, name)) {
				return tweet;
			}
		}

		throw new NoSuchElementException(
				"No tweets on the page appear to have cssElement:  " + name);
	}

}
