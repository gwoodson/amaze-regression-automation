/**
 * 
 */
package com.deque.amaze.regression;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.deque.amaze.Amaze;
import com.deque.amaze.SiteException;
import com.deque.amaze.regression.common.PrettyPrint;

/**
 * Driver for Amaze regression tests
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class RegressionDriver implements Driver {

	private static Logger logger = Logger.getLogger(RegressionDriver.class
			.getName());


	private WebDriver driver;
	private String baseUrl;
	private Properties options;
	private String amazeServerUrl;
	private Amaze amaze;

	private final String BROWSER = "browser";

	private final String CHROME = "chrome";
	private final String CHROME_BINARY = "chrome.binary";
	private final String WEBDRIVER_CHROME_BINARY = "webdriver.chrome.driver";

	private final String FIREFOX = "firefox";
	private final String FIREFOX_BINARY = "firefox.binary";
	private final String FIREFOX_PROFILE = "firefox.profile";

	public String SAFARI = "safari";
	private String SAFARI_BINARY = "safari.binary";

	/**
	 * The server property
	 */
	private final String AMAZE_SERVER = "amaze.server";

	/**
	 * The default server URL
	 * 
	 */
	private final String DEFAULT_AMAZE_SERVER_URL = "https://publicsites.dequeamaze.com/";

	/**
	 * Format string to inject a script-tag into the page
	 */
	private final String INJECT_SCRIPT = "(function () {"
			+ "var script = document.createElement('script');"
			+ "script.type = 'text/javascript';"
			+ "script.src = '%s';"
			+ "(document.head || document.body || document.documentElement).appendChild(script);"
			+ "}());";

	/**
	 * The /amazeme API
	 */
	private final String AMAZEME = "amazeme";

	/**
	 * JavaScript condition for checking if Amaze is defined and part of the DOM
	 */
	private final String IS_AMAZE_DEFINED = "return window.amaze !== undefined;";

	/**
	 * JavaScript to retrieve the current status of Amaze
	 */
	private final String AMAZE_STATUS = "return window.amaze.status;";

	/**
	 * JavaScript condition to check if Amaze has completed or is monitoring the
	 * DOM for mutations.
	 */
	private final String IS_AMAZE_READY = "return window.amaze.status === 'DONE'"
			+ " || window.amaze.status === 'MONITORING'"
			+ " || window.amaze.status === 'READY';";

	/**
	 * @return the driver
	 */
	public WebDriver getDriver() {

		return driver;
	}

	/**
	 * @param driver
	 *            the driver to set
	 */
	public void setDriver(WebDriver driver) {

		this.driver = driver;
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {

		return baseUrl;
	}

	/**
	 * @param baseUrl
	 *            the baseUrl to set
	 * @throws MalformedURLException
	 */
	public void setBaseUrl(String baseUrl) throws MalformedURLException {

		@SuppressWarnings("unused")
		URL u = new URL(baseUrl);

		this.baseUrl = baseUrl;
	}

	/**
	 * @return the options
	 */
	public Properties getOptions() {

		return options;
	}

	/**
	 * Get an option from the provided Properties
	 * 
	 * @param option
	 *            The option key
	 * @return
	 */
	public String getOption(String option) {

		return this.options.getProperty(option);
	}

	/**
	 * @param options
	 *            the options to set
	 */
	public void setOptions(Properties options) {

		this.options = options;
	}

	/**
	 * @return the amazeServerUrl
	 */
	public String getAmazeServerUrl() {

		return amazeServerUrl;
	}

	/**
	 * @param amazeServerUrl
	 *            the amazeServerUrl to set
	 * @throws MalformedURLException
	 *             When an invalid URL is set
	 */
	public void setAmazeServerUrl(String amazeServerUrl)
			throws MalformedURLException {

		URL u = new URL(amazeServerUrl);

		if (!this.options.getProperty("allow.http", "false").equals("true")) {
			if (!u.getProtocol().equalsIgnoreCase("https")) {
				throw new MalformedURLException(
						"Invalid Amaze server URL: must be HTTPS");
			}
		}

		if (amazeServerUrl.endsWith("/")) {
			this.amazeServerUrl = amazeServerUrl;
		} else {
			this.amazeServerUrl = amazeServerUrl + "/";
		}

	}

	/**
	 * Create a RegressionDriver with a specific WebDriver
	 * 
	 * @param options
	 *            The options
	 * @param baseUrl
	 *            The baseUrl
	 * @param driver
	 *            The custom WebDriver
	 * @throws IOException
	 * @throws JSONException
	 */
	public RegressionDriver(Properties options, String baseUrl, WebDriver driver)
			throws JSONException, IOException {

		this.setBaseUrl(baseUrl);
		this.setOptions(options);

		this.setAmazeServerUrl(options.getProperty(this.AMAZE_SERVER,
				this.DEFAULT_AMAZE_SERVER_URL));

		this.setDriver(driver);

		this.driver.get(this.baseUrl);

		this.amaze = new Amaze(this.amazeServerUrl);
	}

	/**
	 * Creates a new driver
	 * 
	 * @param options
	 *            The options
	 * @param baseUrl
	 *            The baseUrl
	 * @throws IOException
	 * @throws JSONException
	 * 
	 */
	public RegressionDriver(Properties options, String baseUrl)
			throws JSONException, IOException {

		this.setBaseUrl(baseUrl);

		this.setOptions(options);

		this.setAmazeServerUrl(options.getProperty(this.AMAZE_SERVER,
				this.DEFAULT_AMAZE_SERVER_URL));

		String browserName = options.getProperty(this.BROWSER, this.CHROME);
		if (browserName.equals(this.CHROME)) {

			String chromeBinary = options.getProperty(this.CHROME_BINARY);
			System.setProperty(this.WEBDRIVER_CHROME_BINARY, chromeBinary);
			this.setDriver(new ChromeDriver());

		} else if (browserName.equals(this.FIREFOX)) {

			ProfilesIni profilesIni = new ProfilesIni();
			String firefoxProfile = options.getProperty(this.FIREFOX_PROFILE);
			File firefoxBinary = new File(
					options.getProperty(this.FIREFOX_BINARY));

			FirefoxProfile profile = profilesIni.getProfile(firefoxProfile);
			this.setDriver(new FirefoxDriver(new FirefoxBinary(firefoxBinary),
					profile));

		}else if (browserName.equals(this.SAFARI)) {
			
			try {
				this.setDriver(new SafariDriver());
				Thread.sleep(3000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		this.driver.get(this.baseUrl);
		this.amaze = new Amaze(this.amazeServerUrl);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.deque.amaze.regression.Driver#getCurrentUrl()
	 */
	@Override
	public String getCurrentUrl() {

		return this.driver.getCurrentUrl();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#waitForJavascriptCondition(java.lang
	 * .String)
	 */
	@Override
	public void waitForJavascriptCondition(final String condition) {

		this.waitForJavascriptCondition(condition, 30);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#waitForJavascriptCondition(java.lang
	 * .String, long)
	 */
	@Override
	public void waitForJavascriptCondition(final String condition, long timeout) {

		Wait<WebDriver> wait = new WebDriverWait(this.driver, timeout);

		ExpectedCondition<Boolean> conditionPasses = new ExpectedCondition<Boolean>() {

			@Override
			public Boolean apply(WebDriver driver) {

				JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;

				Boolean result = (Boolean) javascriptExecutor
						.executeScript(condition);

				logger.log(Level.INFO, String.format(
						"Waiting for `%s` to become truthy; received %s.",
						condition, result));

				return result;
			}

		};

		wait.until(conditionPasses);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.deque.amaze.regression.Driver#injectAmaze()
	 */
	@Override
	public void injectAmaze() {

		String libraryUrl = this.amazeServerUrl + this.AMAZEME;

		this.injectScriptTag(libraryUrl);

		this.waitForAmazeReady();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.deque.amaze.regression.Driver#injectScriptTag(java.lang.String)
	 */
	@Override
	public void injectScriptTag(String src) {

		String javascript = String.format(this.INJECT_SCRIPT, src);

		this.executeJavascript(javascript);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#findElement(org.openqa.selenium.By)
	 */
	@Override
	public WebElement findElement(By selector) {

		return this.driver.findElement(selector);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#findElements(org.openqa.selenium.By)
	 */
	@Override
	public List<WebElement> findElements(By selector) {

		return this.driver.findElements(selector);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#waitForElementVisible(org.openqa.selenium
	 * .By)
	 */
	@Override
	public WebElement waitForElementVisible(final By selector) {

		return this.waitForElementVisible(selector, 30);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#waitForElementVisible(org.openqa.selenium
	 * .By, long)
	 */
	@Override
	public WebElement waitForElementVisible(final By selector, long timeout) {

		Wait<WebDriver> wait = new WebDriverWait(this.driver, timeout);

		ExpectedCondition<WebElement> elementIsVisible = new ExpectedCondition<WebElement>() {

			private final String prettySelector = PrettyPrint
					.selector(selector);

			@Override
			public WebElement apply(WebDriver driver) {

				WebElement element = driver.findElement(selector);

				logger.log(Level.INFO, String.format(
						"Waiting for %s to become visible...",
						this.prettySelector));

				if (element.isDisplayed()) {
					return element;
				}

				return null;

			}

		};

		return wait.until(elementIsVisible);
	}

	@Override
	public WebElement waitForElementVisible(final WebElement element) {

		return this.waitForElementVisible(element, 30);

	}

	@Override
	public WebElement waitForElementVisible(final WebElement element,
			long timeout) {

		Wait<WebDriver> wait = new WebDriverWait(this.driver, timeout);

		ExpectedCondition<WebElement> elementIsHidden = new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver driver) {

				logger.log(Level.INFO,
						String.format("Waiting for %s to hide...", element));

				if (element.isDisplayed()) {
					return element;
				}

				return null;
			}
		};

		return wait.until(elementIsHidden);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#waitForElementHidden(org.openqa.selenium
	 * .By)
	 */
	@Override
	public WebElement waitForElementHidden(final By selector) {

		return this.waitForElementHidden(selector, 30);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#waitForElementHidden(org.openqa.selenium
	 * .By, long)
	 */
	@Override
	public WebElement waitForElementHidden(final By selector, long timeout) {

		Wait<WebDriver> wait = new WebDriverWait(this.driver, timeout);

		ExpectedCondition<WebElement> elementIsHidden = new ExpectedCondition<WebElement>() {

			private final String prettySelector = PrettyPrint
					.selector(selector);

			@Override
			public WebElement apply(WebDriver driver) {

				WebElement element = driver.findElement(selector);

				logger.log(Level.INFO, String.format(
						"Waiting for %s to hide...", this.prettySelector));

				if (element.isDisplayed()) {
					return null;
				}

				return element;

			}

		};

		return wait.until(elementIsHidden);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#waitForElementHidden(org.openqa.selenium
	 * .WebElement)
	 */
	@Override
	public WebElement waitForElementHidden(final WebElement element) {

		return this.waitForElementHidden(element, 30);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#waitForElementHidden(org.openqa.selenium
	 * .WebElement, long)
	 */
	@Override
	public WebElement waitForElementHidden(final WebElement element,
			long timeout) {

		Wait<WebDriver> wait = new WebDriverWait(this.driver, timeout);

		logger.log(Level.INFO, "Element: " + PrettyPrint.element(element));

		ExpectedCondition<WebElement> elementIsHidden = new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver driver) {

				logger.log(
						Level.INFO,
						String.format("Waiting for %s to hide...",
								PrettyPrint.element(element)));

				if (element.isDisplayed()) {
					return null;
				}

				return element;

			}
		};

		return wait.until(elementIsHidden);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#executeJavascript(java.lang.String)
	 */
	@Override
	public Object executeJavascript(String javascript) {

		JavascriptExecutor js = (JavascriptExecutor) this.driver;

		return (Object) js.executeScript(javascript);

	}

	/**
	 * Wait for Amaze to have run the injections
	 */
	private void waitForAmazeReady() {

		this.waitForJavascriptCondition(this.IS_AMAZE_DEFINED, 100);

		this.waitForJavascriptCondition(this.IS_AMAZE_READY, 100);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.deque.amaze.regression.Driver#getAmazeStatus()
	 */
	@Override
	public String getAmazeStatus() {

		return (String) this.executeJavascript(this.AMAZE_STATUS);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.deque.amaze.regression.Driver#navigateTo(java.lang.String)
	 */
	@Override
	public void navigateTo(String url) throws MalformedURLException {

		@SuppressWarnings("unused")
		URL _url = new URL(url);

		if (this.getCurrentUrl().equalsIgnoreCase(url)) {
			return;
		}

		this.driver.navigate().to(url);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.deque.amaze.regression.Driver#getActiveElement()
	 */
	@Override
	public WebElement getActiveElement() {

		return this.driver.switchTo().activeElement();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.deque.amaze.regression.Driver#getTitle()
	 */
	@Override
	public String getTitle() {

		return this.driver.getTitle();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.deque.amaze.regression.Driver#injectAmazeAsExtension(java.lang.String
	 * , java.lang.String)
	 */
	@Override
	public void injectAmazeAsExtension(String hostname, String path)
			throws JSONException, IOException, SiteException,
			InterruptedException {
		
		// URL encode the path, as it may contain sneaky characters (spent 2+
		// hours trying to debug this stupid shit)
		path = URLEncoder.encode(path, "UTF-8");
		
		String iffe = this.amaze.getExtensionIffe(hostname, path);

		iffe = StringEscapeUtils.escapeEcmaScript(iffe);

		String js = "(function (d) {" + "var s = d.createElement('script')"
				+ "  , e = d.head || d.body || d.documentElement"
				+ "  , t = d.createTextNode('%s');"

				+ "s.id = 'amaze-selenium-script';"
				+ "s.type = 'text/javascript';"
				+ "s.setAttribute('data-foo', 'foobar');" + "s.appendChild(t);"
				+ "e.appendChild(s);" + "}(document));";

		js = String.format(js, iffe);
		
		//System.out.println("js: " + js);

		this.executeJavascript(js);

		this.waitForAmazeReady();

		// sleeping for a second solves all the world's problems
		Thread.sleep(1000);
	}
}


