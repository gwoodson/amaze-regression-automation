/**
 * 
 */
package com.deque.amaze.regression.chase;

import java.io.IOException;
import java.util.Properties;

import org.json.JSONException;
import org.openqa.selenium.WebDriver;

import com.deque.amaze.regression.RegressionDriver;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class ChaseDriver extends RegressionDriver {

	static final String BASE_URL = "https://chase.com";
	
	/**
	 * @param options
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public ChaseDriver(Properties options) throws JSONException, IOException {

		super(options, BASE_URL);
	}
	
	/**
	 * @param options
	 * @param driver
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public ChaseDriver(Properties options, WebDriver driver) throws JSONException, IOException {

		super(options, BASE_URL, driver);
	}

}
