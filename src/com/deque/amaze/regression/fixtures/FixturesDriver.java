package com.deque.amaze.regression.fixtures;

import java.io.IOException;
import java.util.Properties;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.RegressionDriver;
import com.deque.amaze.regression.Driver;

/**
 * Regression driver specific for Facebook
 * 
 * @author Greta Woodson <greta.woodson@deque.com>
 */
@SuppressWarnings("unused")
public class FixturesDriver extends RegressionDriver {

	/**
	 * @param options
	 * @throws IOException
	 * @throws JSONException
	 */
	public FixturesDriver(Properties options) throws JSONException, IOException {

		super(options, "http://localhost:5000");
	}

	/**
	 * @param options
	 * @param driver
	 * @throws IOException
	 * @throws JSONException
	 */
	public FixturesDriver(Properties options, WebDriver driver)
			throws JSONException, IOException {

		super(options, "http://localhost:5000", driver);
	}
	
	public void navigateTo(String url) {

		super.getDriver().navigate().to(url);
	}

}
