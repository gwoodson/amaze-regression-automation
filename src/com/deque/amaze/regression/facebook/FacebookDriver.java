package com.deque.amaze.regression.facebook;

import java.io.IOException;
import java.util.Properties;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.deque.amaze.regression.RegressionDriver;
import com.deque.amaze.regression.RegressionEventDriver;

/**
 * Regression driver specific for Facebook
 * 
 * @author Greta Woodson <greta.woodson@deque.com>
 */
@SuppressWarnings("unused")
public class FacebookDriver extends RegressionDriver {

	private final By email = By.id("email");

	private final By password = By.id("pass");
	
	private final By logonButton = By.id("loginbutton");

	private Boolean loggedIn = false;

	private JSONObject site = null;

	public Boolean isLoggedIn() {

		return this.loggedIn;
	}

	/**
	 * @param options
	 * @throws IOException
	 * @throws JSONException
	 */
	public FacebookDriver(Properties options) throws JSONException, IOException {

		super(options, "http://facebook.com");
	}

	/**
	 * @param options
	 * @param driver
	 * @throws IOException
	 * @throws JSONException
	 */
	public FacebookDriver(Properties options, EventFiringWebDriver driver)
			throws JSONException, IOException {

		super(options, "http://facebook.com", driver);
	}

	/**
	 * Login to Facebook using the username and password specified by
	 * {@link RegressionDriver#getOptions()}
	 */
	public void login() throws Exception {
		
			String username = super.getOption("facebook.username1");
			String password = super.getOption("facebook.password1");
	
			WebElement usernameInput = super.findElement(this.email);		
			usernameInput.clear();
			usernameInput.sendKeys(username);
	
			WebElement passwordInput = super.findElement(this.password);
			WebElement login = super.findElement(this.logonButton);
			passwordInput.sendKeys(password);
			passwordInput.submit();
	
			Thread.sleep(2500L);
		
			this.loggedIn = true;
		
	}

		public void logoff() throws Exception {

			this.waitForElementVisible(By.id("navAccountLink"));

			WebElement UserNav = findElement(By.id("navAccountLink"));
			UserNav.click();

			this.waitForElementVisible(By.id("logout_form"));

			WebElement UserLogoff = findElement(By
					.id("logout_form"));
			UserLogoff.submit();

			this.loggedIn = false;
		}

	}
