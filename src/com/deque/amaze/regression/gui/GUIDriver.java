package com.deque.amaze.regression.gui;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Properties;

import org.json.JSONException;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.RegressionDriver;
import com.deque.amaze.regression.facebook.FacebookDriver;

public class GUIDriver extends RegressionDriver {

	public static GUIDriver driver;
	
	/**
	 * @param options
	 * @throws IOException
	 * @throws JSONException
	 */
	public GUIDriver(Properties options) throws JSONException, IOException {

		super(options, "http://localhost:8000/woodson");
	}

	/**
	 * @param options
	 * @param driver
	 * @throws IOException
	 * @throws JSONException
	 */
	public GUIDriver(Properties options, WebDriver driver)
			throws JSONException, IOException {

		super(options, "http://localhost:8000/woodson", driver);
	}

	/**
	 * Wait for the page container to be present before doing anything else
	 */
	public void waitForPageContainer() {

		super.waitForElementVisible(By.id("page-container"));
	}

	/**	 
	 * 
	 * @param url The URL to navigate to
	 */
	public void navigateTo(String url) {

		super.getDriver().navigate().to(url);
	}
}

