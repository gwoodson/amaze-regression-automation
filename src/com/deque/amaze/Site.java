/**
 * 
 */
package com.deque.amaze;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A representation of an Amaze site
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class Site {

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(Site.class.getName());
	
	private JSONObject site = null;

	private String bcl = null;
	private String description;
	private String name;
	private String regex;

	private Pattern pattern;

	/**
	 * @return The JSON representation of a site
	 */
	public JSONObject getSite() {

		return this.site;
	}

	/**
	 * @param site
	 *            The JSON representation of a site to set
	 * @throws JSONException 
	 */
	public void setSite(JSONObject site) throws JSONException {

		//logger.log(Level.INFO, site.toString(4));
		this.site = site;
	}

	/**
	 * @return the bcl
	 * @throws SiteException
	 * @throws JSONException
	 */
	public String getBcl() throws SiteException, JSONException {
		
		if (this.bcl == null) {

			if (this.site == null) {
				throw new SiteException(
						"Cannot get BCL if there is no JSON representation provided");
			}

			String bcl = this.site.getString("bcl");
			this.setBcl(bcl);
		}
		
		logger.log(Level.INFO, bcl);

		return bcl;
	}

	/**
	 * @param bcl
	 *            the bcl to set
	 */
	public void setBcl(String bcl) {
		
		this.bcl = bcl;
	}

	/**
	 * @return the description
	 * @throws SiteException
	 * @throws JSONException
	 */
	public String getDescription() throws SiteException, JSONException {

		if (this.description == null) {
			if (this.site == null) {
				throw new SiteException(
						"Cannot get the Site description if there is no JSON representation provided");
			}

			String description = this.site.getString("description");
			this.setDescription(description);
		}

		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {

		this.description = description;
	}

	/**
	 * @return the name
	 * @throws SiteException
	 * @throws JSONException
	 */
	public String getName() throws SiteException, JSONException {

		if (this.name == null) {
			if (this.site == null) {
				throw new SiteException(
						"Cannot get Site name if there is no JSON representation provided");
			}

			String name = this.site.getString("name");
			this.setName(name);
		}
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {

		this.name = name;
	}

	/**
	 * @return the regex
	 * @throws SiteException
	 * @throws JSONException
	 */
	public String getRegex() throws SiteException, JSONException {

		if (this.regex == null) {
			if (this.site == null) {
				throw new SiteException(
						"Cannot get Site regex if there is no JSON respresentation provided");
			}
			
			JSONObject regex = this.site.getJSONObject("regex");
			String includeRegex = regex.getString("include");
			
			this.setRegex(includeRegex);
		}
		return regex;
	}

	/**
	 * @param regex
	 *            the regex to set
	 */
	public void setRegex(String regex) {

		this.regex = regex;
		this.pattern = Pattern.compile(regex);
	}

	/**
	 * Construct a new Site
	 */
	public Site() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * Construct a new Site based on the provided JSONObject
	 * 
	 * @param site
	 *            The site representation as JSON
	 * @throws JSONException
	 */
	public Site(JSONObject site) throws JSONException {

		this.setSite(site);
		JSONObject regex = site.getJSONObject("regex");
		this.setRegex(regex.getString("include"));
		
		this.setBcl(site.getString("bcl"));			
		logger.log(Level.INFO, bcl);
		//TODO set name, description, etc.
	}

	/**
	 * Check if a Site matches a hostname
	 * 
	 * @param hostname
	 *            The hostname to check
	 * @return Whether or not the hostname matches
	 */
	public Boolean matches(String hostname) {

		Matcher matcher = this.pattern.matcher(hostname);

		return matcher.matches();
	}
}
