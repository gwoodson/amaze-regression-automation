/**
 * 
 */
package com.deque.amaze;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class SiteException extends Exception {

	/**
	 * Automagically generated version UID
	 */
	private static final long serialVersionUID = 8742328660781069088L;

	/**
	 * 
	 */
	public SiteException() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public SiteException(String message) {

		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public SiteException(Throwable cause) {

		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SiteException(String message, Throwable cause) {

		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public SiteException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {

		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
