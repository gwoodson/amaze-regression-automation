# Amaze Selenium Regressions

## Setup

NOTE: I have only ever attempted to setup/run these tests with Eclipse.  If you don't have Eclispe installed, get it.  Personally, I prefer [STS](http://www.springsource.org/spring-tool-suite-download) because it has most everything you'll need pre-installed.

### Install Dependencies

If a dependency is out of date, **do not manually add a JAR to classpath**.  Use maven.  Maven is your friend.

#### Maven
We're using Maven for dependency mapping.  Maven is awesome.

Install the following:

- [Maven](http://maven.apache.org/) be sure to add Maven to PATH
- [Maven/Eclipse plugin](http://maven.apache.org/plugins/maven-eclipse-plugin/)

#### JUnit 4
Handled by Maven.

Used for making assertions and running suites.


#### Mockito
Handled by Maven.

Used as our mocking framework (internal unit testing)

#### Selenium 2
Handled by Maven.

Used for running regressions.

#### Download chromedriver

http://code.google.com/p/chromedriver/downloads/list

Used by test scripts for launching Google Chrome.

### Import Project

Using your newly installed Eclipse plugin, import the repository as a Maven project.

![Existing Maven Project](http://i.imgur.com/DqQ8m.png)



### Configure the Tests

#### Twitter

Open the file `test/com/deque/amaze/twitter/twitter.properties-example` and add your password, username and path to ChromeDriver.  If you are testing against your local Amaze server, be sure to add its URL too.  Once you've added your specific settings, rename the file to `twitter.properties`.

**DO NOT COMMIT YOUR PROPERTIES FILE**

Example:

```
amaze.server=https://localhost/Amaze
twitter.password=mypassword
twitter.username=my.username@email.com
driver.binary=/Users/you/path/to/chromedriver
```

#### Facebook

As Facebook's security policies require a different approach to dynamic remediation, different test configuration is required.

Facebook regressions also require multiple user accounts, as the chat mechanism is being remediated.  A minimum of two accounts is required.

You must, once again, create a properties file which contains configuration directives.  There is an example for you at `test/com/deque/amaze/facebook/facebook.properties-example`.

The required directives are:

- `amaze.server`: The amaze server to fetch overlays from
- `facebook.username1` and `facebook.username2`: The two usernames which will be used
- `facebook.password1` and `facebook.password2`: The corresponding passwords for the above usernames

A new directive has been introducted in order to override Facebook's security policies: `allow.http`.  This directive will allow the Amaze server to run over HTTP, rather ran requiring HTTPS.  Setting `allow.http` to anything other than `true`, or omitting the directive all together tells the RegressionDriver to only allow secured servers.

Your properties file must be saved as `test/com/deque/amaze/facebook/facebook.properties`.

##### Development

If you are running regressions against a development server (for example, _localhost_), you may not use HTTPS.  Your properties file should look something like this:

```
allow.http=true
amaze.server=http://localhost/Amaze
facebook.username1=myusername
facebook.username2=mysecondusername
facebook.password1=mypassword
facebook.password2=mysecondpassword
```

##### Production

Production environments should use HTTPS, but this is not absolutely necessary.  If you are not using HTTP, your properties file may look like this:

```
amaze.server=https://publicsites.dequeamaze.com/Amaze
facebook.username1=myusername
[...]
```