package com.deque.screening;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

@RunWith(Suite.class)
@SuiteClasses({
	CreateNewUser.class
})
public class ScreeningSuite {
	
	public static WebDriver driver;

	@BeforeClass
	public static void setUpBeforeClass() {

		driver = new FirefoxDriver();
		driver.navigate().to("localhost:3000");

	}

	@AfterClass
	public static void tearDownAfterClass() {

		System.out.println("ScreeningSuite->tearDownAfterClass()");
	}

	@Before
	public static void beforeTest() {

		System.out.println("ScreeningSuite->beforeTest()");
	}

}
