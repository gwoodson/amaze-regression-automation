/**
 * 
 */
package com.deque.amaze.regression.twitter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class TwitterDriverTest {

	private static TwitterDriver tweetDriver;
	private static WebDriver webDriver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("twitter.username", "bob");
		properties.setProperty("twitter.password", "bob backwards");
		properties.setProperty("amaze.server", "https://dogs.com/Amaze/");

		webDriver = mock(WebDriver.class);

		tweetDriver = new TwitterDriver(properties, webDriver);

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	public void login() {

		By email = By.id("signin-email");
		By password = By.id("signin-password");

		WebElement p = mock(WebElement.class);
		WebElement e = mock(WebElement.class);

		when(webDriver.findElement(email)).thenReturn(e);
		when(webDriver.findElement(password)).thenReturn(p);

		tweetDriver.login();

		verify(p, times(1)).sendKeys("bob backwards");
		verify(e, times(1)).sendKeys("bob");
		verify(p, times(1)).submit();

	}

	@Test
	public void isReCaptcha() {

		By recaptcha = By.id("recaptcha_widget_div");
		WebElement div = mock(WebElement.class);

		when(webDriver.findElement(recaptcha)).thenReturn(div);

		assertTrue(tweetDriver.isReCaptcha());
	}

	@Test
	public void isRecaptchaThrows() {

		By recaptcha = By.id("recaptcha_widget_div");

		when(webDriver.findElement(recaptcha)).thenReturn(null);

		assertFalse(tweetDriver.isReCaptcha());

	}

	@Test
	public void waitForPageContainer() {

		// TODO how do i test waits?
	}

}
