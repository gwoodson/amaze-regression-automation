package com.deque.amaze.regression.common;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;

public class PrettyPrintTest {

	@Test
	public void elementTagName() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		when(div.getAttribute(anyString())).thenReturn(null);

		String pretty = PrettyPrint.element(div);

		assertEquals("div", pretty);

	}

	@Test
	public void elementClassName() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		when(div.getAttribute("class")).thenReturn("dogs and cats");

		String pretty = PrettyPrint.element(div);

		assertEquals("div.dogs.and.cats", pretty);

	}

	@Test
	public void anchorRel() {

		WebElement anchor = mock(WebElement.class);
		when(anchor.getTagName()).thenReturn("A");
		when(anchor.getAttribute("rel")).thenReturn("nofollow");

		String pretty = PrettyPrint.element(anchor);
		assertEquals("a[rel=\"nofollow\"]", pretty);
	}

	@Test
	public void elementClassNameWithWhitespace() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		// just to verify we're not adding [rel=""] on DIVs
		when(div.getAttribute("rel")).thenReturn("nofollow");
		when(div.getAttribute("class")).thenReturn(
				"dogs  and         cats    \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t    ");

		String pretty = PrettyPrint.element(div);

		assertEquals("div.dogs.and.cats", pretty);
	}

	@Test
	public void elementId() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		when(div.getAttribute("id")).thenReturn("cats-and-dogs");

		String pretty = PrettyPrint.element(div);

		assertEquals("div#cats-and-dogs", pretty);

	}

	@Test
	public void elementIdAndClass() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		when(div.getAttribute("id")).thenReturn("cats-and-dogs");
		when(div.getAttribute("class")).thenReturn("dogs and cats");
		// verify we're not adding [type=""] on divs
		when(div.getAttribute("type")).thenReturn("cats-and-dogs");

		String pretty = PrettyPrint.element(div);

		assertEquals("div#cats-and-dogs.dogs.and.cats", pretty);
	}

	@Test
	public void elementType() {

		WebElement input = mock(WebElement.class);
		when(input.getTagName()).thenReturn("INPUT");
		when(input.getAttribute("type")).thenReturn("cats-and-dogs");

		String pretty = PrettyPrint.element(input);
		assertEquals("input[type=\"cats-and-dogs\"]", pretty);
	}

	@Test
	public void elementIdClassAndType() {

		WebElement input = mock(WebElement.class);
		when(input.getTagName()).thenReturn("INPUT");
		when(input.getAttribute("type")).thenReturn("cats-and-dogs");
		when(input.getAttribute("id")).thenReturn("cats-and-dogs");
		when(input.getAttribute("class")).thenReturn("dogs and cats");

		String pretty = PrettyPrint.element(input);
		assertEquals(
				"input#cats-and-dogs.dogs.and.cats[type=\"cats-and-dogs\"]",
				pretty);
	}

	@Test
	public void anchorIdClassRel() {

		WebElement anchor = mock(WebElement.class);
		when(anchor.getTagName()).thenReturn("A");
		when(anchor.getAttribute("rel")).thenReturn("nofollow");
		when(anchor.getAttribute("id")).thenReturn("cats");
		when(anchor.getAttribute("class")).thenReturn("snakes and muffins");

		String pretty = PrettyPrint.element(anchor);

		assertEquals("a#cats.snakes.and.muffins[rel=\"nofollow\"]", pretty);

	}

	@Test
	public void divRoleDialog() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		when(div.getAttribute("role")).thenReturn("dialog");

		String pretty = PrettyPrint.element(div);

		assertEquals("div[role=\"dialog\"]", pretty);
	}

	@Test
	public void divTabIndex() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		when(div.getAttribute("tabindex")).thenReturn("-1");

		String pretty = PrettyPrint.element(div);

		assertEquals("div[tabindex=\"-1\"]", pretty);
	}

	@Test
	public void divIdClassTypeRole() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		when(div.getAttribute("id")).thenReturn("cats-and-dogs");
		when(div.getAttribute("class")).thenReturn("dogs and cats");
		// verify we're not adding [type=""] on divs
		when(div.getAttribute("type")).thenReturn("cats-and-dogs");
		when(div.getAttribute("role")).thenReturn("things and junk");

		String pretty = PrettyPrint.element(div);

		assertEquals(
				"div#cats-and-dogs.dogs.and.cats[role=\"things and junk\"]",
				pretty);
	}

	@Test
	public void formAction() {

		WebElement form = mock(WebElement.class);
		when(form.getTagName()).thenReturn("FORM");
		when(form.getAttribute("action")).thenReturn("/cats/dogs");

		String pretty = PrettyPrint.element(form);

		assertEquals("form[action=\"/cats/dogs\"]", pretty);

	}

	@Test
	public void formMethod() {

		WebElement form = mock(WebElement.class);
		when(form.getTagName()).thenReturn("FORM");
		when(form.getAttribute("method")).thenReturn("POST");

		String pretty = PrettyPrint.element(form);

		assertEquals("form[method=\"POST\"]", pretty);

	}

	@Test
	public void formActionMethod() {

		WebElement form = mock(WebElement.class);
		when(form.getTagName()).thenReturn("FORM");
		when(form.getAttribute("action")).thenReturn("/cats/dogs");
		when(form.getAttribute("method")).thenReturn("POST");

		String pretty = PrettyPrint.element(form);

		assertEquals("form[action=\"/cats/dogs\"][method=\"POST\"]", pretty);

	}

	@Test
	public void divIdClassTypeRoleActionMethod() {

		WebElement div = mock(WebElement.class);
		when(div.getTagName()).thenReturn("DIV");
		when(div.getAttribute("id")).thenReturn("cats-and-dogs");
		when(div.getAttribute("class")).thenReturn("dogs and cats");
		// verify we're not adding [type=""] on divs
		when(div.getAttribute("type")).thenReturn("cats-and-dogs");
		when(div.getAttribute("role")).thenReturn("things and junk");
		// verify we're not adding [action=""] on divs
		when(div.getAttribute("action")).thenReturn("/cats/dogs");
		// verify we're not adding [method=""] on divs
		when(div.getAttribute("method")).thenReturn("POST");

		String pretty = PrettyPrint.element(div);

		assertEquals(
				"div#cats-and-dogs.dogs.and.cats[role=\"things and junk\"]",
				pretty);
	}

	@Test
	public void selectorId() {

		By id = By.id("cats");

		String pretty = PrettyPrint.selector(id);

		assertEquals("#cats", pretty);
	}

	@Test
	public void selectorClassName() {

		By className = By.className("cats");

		String pretty = PrettyPrint.selector(className);

		assertEquals(".cats", pretty);
	}

	@Test
	public void selectorTagName() {

		By tagName = By.tagName("cats");

		String pretty = PrettyPrint.selector(tagName);

		assertEquals("cats", pretty);
	}

	@Test
	public void selectorCssSelector() {

		By css = By.cssSelector("cats#cats[cats='cats'].cats");

		String pretty = PrettyPrint.selector(css);

		assertEquals("cats#cats[cats='cats'].cats", pretty);

	}

	@Test
	public void selectorXpath() {

		By xpath = By.xpath("//a[last()]");
		String pretty = PrettyPrint.selector(xpath);
		assertEquals("//a[last()]", pretty);
	}

}
