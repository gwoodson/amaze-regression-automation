/**
 * 
 */
package com.deque.amaze.regression.samples;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class Sample2 {

	public final static String URL = "https://twitter.com";

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		System.out.println("Sample2->setUpBeforeClass()");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		System.out.println("Sample2->tearDownAfterClass()");
	}

	@Test
	public void test() {

		System.out.println("Sample2->test()");
		assertTrue(true);
	}

}
