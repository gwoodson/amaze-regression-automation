package com.deque.amaze.regression.samples;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
		Sample1.class, Sample2.class
})
public class SampleSuite {

	@BeforeClass
	public static void setUpBeforeClass() {

		System.out.println("SampleSuite->setUpBeforeClass()");

	}

	@AfterClass
	public static void tearDownAfterClass() {

		System.out.println("SampleSuite->tearDownAfterClass()");
	}

	@Before
	public static void beforeTest() {

		System.out.println("SampleSuite->beforeTest()");
	}

}
