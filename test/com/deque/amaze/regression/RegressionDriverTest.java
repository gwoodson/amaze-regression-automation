package com.deque.amaze.regression;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;

public class RegressionDriverTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void setBaseUrl() throws Exception {

		thrown.expect(MalformedURLException.class);
		new RegressionDriver(new Properties(), "cats and dogs");

	}

	@Test
	public void setAmazeServerUrlHTTPS() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("driver.binary", "moose");
		properties.setProperty("amaze.server", "http://doggies.com/Amaze");

		thrown.expect(MalformedURLException.class);
		thrown.expectMessage("Invalid Amaze server URL: must be HTTPS");
		new RegressionDriver(properties, "http://dogs.com/Amaze");
	}

	@Test
	public void setAmazeServerUrlSlashAmaze() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("driver.binary", "moose");
		properties.setProperty("amaze.server", "https://doggies.com/dogs");

		thrown.expect(MalformedURLException.class);
		thrown.expectMessage("Invalid Amaze server URL: must contain /Amaze");

		new RegressionDriver(properties, "https://dogs.com/doggies");

	}

	@Test
	public void getOption() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("amaze.server", "https://doggies.com/Amaze");
		properties.setProperty("moose", "esoom");

		WebDriver driver = mock(WebDriver.class);

		//

		RegressionDriver regression = new RegressionDriver(properties,
				"https://doggies.com/dogs", driver);
		verify(driver).get("https://doggies.com/dogs");

		assertEquals("esoom", regression.getOption("moose"));

	}

	@Test
	public void getCurrentUrl() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("amaze.server", "https://doggies.com/Amaze");

		WebDriver driver = mock(WebDriver.class);

		RegressionDriver regression = new RegressionDriver(properties,
				"https://doggies.com/dogs", driver);
		verify(driver).get("https://doggies.com/dogs");
		when(driver.getCurrentUrl()).thenReturn("https://doggies.com/dogs");

		assertEquals("https://doggies.com/dogs", regression.getCurrentUrl());

		verify(driver).getCurrentUrl();

	}

	@Test
	public void waitForJavascriptCondition() {

		// TODO no idea how to test this. we're creating another object
		// (WebDriverWait) with the driver, then doing some WebDriverWait
		// stuff...

	}

	@Test
	public void injectAmaze() {

		// TODO no idea how to test this method...

	}

	@Test
	public void injectScriptTag() {

		// TODO no idea how to test this either...

	}

	@Test
	public void findElement() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("amaze.server", "https://doggies.com/Amaze");

		WebDriver driver = mock(WebDriver.class);

		RegressionDriver regression = new RegressionDriver(properties,
				"https://doggies.com/dogs", driver);

		By selector = By.id("cats");
		WebElement element = mock(WebElement.class);

		when(driver.findElement(selector)).thenReturn(element);

		assertEquals(element, regression.findElement(selector));

		verify(driver).findElement(selector);

	}

	@Test
	public void findElements() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("amaze.server", "https://doggies.com/Amaze");

		WebDriver driver = mock(WebDriver.class);

		RegressionDriver regression = new RegressionDriver(properties,
				"https://doggies.com/dogs", driver);

		By selector = By.id("cats");
		List<WebElement> elements = new ArrayList<WebElement>();
		WebElement element = mock(WebElement.class);
		elements.add(element);

		when(driver.findElements(selector)).thenReturn(elements);

		assertEquals(elements, regression.findElements(selector));

		verify(driver).findElements(selector);

	}

	@Test
	public void executeJavascript() {

		// TODO no idea how to test this

	}

	@Test
	public void waitForAmazeReady() {

		// TODO figure out how to test this
	}

	@Test
	public void getAmazeStatus() {

		// TODO figure out how to test this
	}

	@Test
	public void navigateTo() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("amaze.server", "https://doggies.com/Amaze");

		WebDriver driver = mock(WebDriver.class);

		Navigation navigation = mock(WebDriver.Navigation.class);
		when(driver.navigate()).thenReturn(navigation);
		when(driver.getCurrentUrl()).thenReturn("hi");

		RegressionDriver regression = new RegressionDriver(properties,
				"https://doggies.com/dogs", driver);

		regression.navigateTo("https://milk.com");

		verify(navigation).to("https://milk.com");

	}

	@Test
	public void navigateToSameURL() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("amaze.server", "https://doggies.com/Amaze");

		WebDriver driver = mock(WebDriver.class);

		Navigation navigation = mock(WebDriver.Navigation.class);
		when(driver.navigate()).thenReturn(navigation);
		when(driver.getCurrentUrl()).thenReturn("https://milk.com");

		RegressionDriver regression = new RegressionDriver(properties,
				"https://doggies.com/dogs", driver);

		regression.navigateTo("https://milk.com");

		verify(navigation, never()).to("https://milk.com");

	}

	@Test
	public void getActiveElement() throws Exception {

		Properties properties = new Properties();
		properties.setProperty("amaze.server", "https://doggies.com/Amaze");

		WebDriver driver = mock(WebDriver.class);
		WebElement expected = mock(WebElement.class);
		TargetLocator locator = mock(TargetLocator.class);
		when(locator.activeElement()).thenReturn(expected);

		when(driver.switchTo()).thenReturn(locator);

		RegressionDriver regression = new RegressionDriver(properties,
				"https://doggies.com/dogs", driver);

		assertEquals(expected, regression.getActiveElement());
		verify(locator, times(1)).activeElement();
	}

}
