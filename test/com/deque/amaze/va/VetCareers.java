package com.deque.amaze.va;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class VetCareers {
	
	private static Logger logger = Logger.getLogger(VetCareers.class.getName());

	private static FacebookDriver driver = VASuite.driver;
	
	private static Helper helper = new Helper();
	private static AboutHelper about = new AboutHelper();	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers");
		if (!driver.isLoggedIn()) {
			driver.login();		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers");
		
		String expectedHeader = "h2";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
	}	
	
	@Test
	@Order(order = 2)
	public void verifyAboutPage() throws MalformedURLException, Exception {	
		
		driver.navigateTo("http://www.facebook.com/vacareers/info");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/info");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		about.verifyHistoryModal(driver);
				
	}
	
	@Test
	@Order(order = 3)
	public void disclaimerPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_174701229285682");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_174701229285682");		
		
		String expectedHeader = "h2";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
	
	@Test
	@Order(order = 4)
	public void whyChooseVAPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_281345961916187");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_281345961916187");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
	
	@Test
	@Order(order = 5)
	public void aboutVAPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_328569820487499");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_328569820487499");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
	
	@Test
	@Order(order = 6)
	public void spotLightJobsPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_377548388937047");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_377548388937047");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		helper.verifyPoliteness(driver);
		helper.verifyAssertiveness(driver);
	}
	
	@Test
	@Order(order = 7)
	public void welcomeToVAPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_159632840825127");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_159632840825127");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
	
	@Test
	@Order(order = 8)
	public void studentsInternPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_156377511136735");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_156377511136735");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
	
	@Test
	@Order(order = 9)
	public void VeteransPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_268454319879253");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_268454319879253");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
	
	@Test
	@Order(order = 10)
	public void VAVideoPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_258210650908394");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_258210650908394");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		helper.verifyPoliteness(driver);
		helper.verifyAssertiveness(driver);			
		
	}
	
	@Test
	@Order(order = 11)
	public void thankVATodayPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_276709865706578");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_276709865706578");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
	}

	@Test
	@Order(order = 12)
	public void thankUNursesPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/vacareers/app_130448620362989");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_130448620362989");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		helper.verifyPoliteness(driver);
		helper.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order = 13)
	public void hiringHeroes() throws MalformedURLException, Exception {
		
		//
		driver.navigateTo("http://www.facebook.com/vacareers/app_430147160412409");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/app_430147160412409");		
		
		String expectedHeader = "h2";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		helper.verifyPoliteness(driver);
		helper.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order=14)
	public void likesPage() throws MalformedURLException, Exception {
		//
		driver.navigateTo("http://www.facebook.com/vacareers/likes");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/vacareers/likes");		
		
		String expectedHeader = "h3";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		//helper.verifySkiplink(driver, skipLink, expectedHeader);
		helper.verifyLikes(driver);
	}
	
}
