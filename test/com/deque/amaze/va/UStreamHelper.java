package com.deque.amaze.va;

import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.facebook.FacebookDriver;

public class UStreamHelper {
	
	private static Logger logger = Logger.getLogger(UStreamHelper.class
			.getName());
	
	private static Helper helper = new Helper();
	private static ModalHelper modalhelper = new ModalHelper();	

	public UStreamHelper() {

	}
		
	public static void verifyShareModal(FacebookDriver driver) throws Exception {		
		
		WebElement trigger = driver.findElement(By.cssSelector("a.button"));

		By modalSelector = By.cssSelector("div.pop_container");
		By closeSelector = By
				.cssSelector("input.button");		
		
		modalhelper.verify(driver, trigger, modalSelector, closeSelector, 100);
		
	}
	
	

}
