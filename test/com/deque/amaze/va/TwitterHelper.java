package com.deque.amaze.va;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;

public class TwitterHelper {	
		
	private static Logger logger = Logger.getLogger(TwitterHelper.class
			.getName());

	/**
	 * 
	 */
	public TwitterHelper() {		
	}
	
	public void goToURL(TwitterDriver driver, String url) throws JSONException, IOException, InterruptedException {
		
		Thread.sleep(1500L);
		driver.navigateTo(url);
		driver.injectAmaze();
		driver.waitForPageContainer();
	}
	
	/**
	 * Verify off-screen Politeness 
	 */
	public void verifyPoliteness(TwitterDriver driver) {
		
		WebElement polite = driver.findElement(By.id("amaze_liveregions_polite"));	
		
		logger.log(
				Level.INFO,
				String.format("Found amaze live regions police : %s",
						PrettyPrint.element(polite)));
		
		String politeClassname = polite.getAttribute("class");
		String politeAriaLive = polite.getAttribute("aria-live");
		
		//Verify Classname
		assertTrue(politeClassname.contains("offscreen"));
		
		//Verify Aria-Live attribute
		assertTrue(politeAriaLive.equals("polite"));
		
	}
	
	/**
	 * Verify off-screen Assertive 
	 */
	public void verifyAssertiveness(TwitterDriver driver) {
		
		WebElement assertive = driver.findElement(By.id("amaze_liveregions_assertive"));
		
		logger.log(
				Level.INFO,
				String.format("Found amaze live regions assertive : %s",
						PrettyPrint.element(assertive)));
		
		String assertiveClassname = assertive.getAttribute("class");
		String assertiveAriaLive = assertive.getAttribute("aria-live");
		
		//Verify Classname
		assertTrue(assertiveClassname.contains("offscreen"));
		
		//Verify Aria-Live attribute
		assertTrue(assertiveAriaLive.equals("assertive"));
		
	}
	
	
	/**
	 * Description:  Verify Skiplink
	 * 			
	 */
	public void verifySkiplink(TwitterDriver driver, String expectedHeader) throws InterruptedException {
		
		WebElement skiplink = returnSkipLink(driver);
		skipLinkHref(skiplink);
		skipToThisExistance(driver);
		verifySkipToLink(driver,skiplink);
		skipToThisHeading(driver, expectedHeader);
		skipToThisHeadingTabIndex(driver);

	}

	/**
	 * Description:  return skiplink classname
	 * 			
	 */
	public WebElement returnSkipLink(TwitterDriver driver) {
		
		//Verify skiplink existence
		WebElement link = driver.findElement(By.id("amaze-skip-to-tweets-link"));

		return link;
	}
	
	/**
	 * Description:  return skiplink 
	 * 			
	 */
	public WebElement skipLinkExistance(TwitterDriver driver) {
		
		//Verify skiplink existence
		WebElement link = driver.findElement(By.id("amaze-skip-to-tweets-link"));
		
		return link;
		
	}
	
	/**
	 * Description:  skiplink href link 
	 * 			
	 */
	public void skipLinkHref(WebElement link) {
		
		String href = link.getAttribute("href");
		
		assertTrue(href.contains("amaze-skip"));
		
		logger.log(Level.INFO,
				String.format("Found skipLink href: %s",
						href));
		
	}
	
	public void skipToThisExistance(TwitterDriver driver) {

		WebElement skipto = driver.findElement(By.id("amaze-skip-to-this"));
		
		assertTrue(skipto.getAttribute("id").equalsIgnoreCase("amaze-skip-to-this"));

	}
	
	public void verifySkipToLink(TwitterDriver driver, WebElement skiplink) throws InterruptedException {
		
		skiplink.sendKeys(Keys.ENTER);
		
		Thread.sleep(5000L);
		
		String skipto = driver.getActiveElement().getAttribute("id");
		
		assertEquals("amaze-skip-to-this", skipto);
		
	}

	public void skipToThisHeading(TwitterDriver driver, String expected) {		 

		WebElement heading = driver.findElement(By.id("amaze-skip-to-this"));
		
		assertEquals(expected, heading.getTagName().toLowerCase());

	}

	public void skipToThisHeadingTabIndex(TwitterDriver driver) {

		WebElement heading = driver.findElement(By.id("amaze-skip-to-this"));
		String tabIndex = heading.getAttribute("tabindex");
		
		assertEquals("-1", tabIndex);

	}
	

}
