package com.deque.amaze.va;

import static com.deque.junit.Assert.assertWebElement;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;

public class ModalHelper {
	
	private static Logger logger = Logger.getLogger(ModalHelper.class
			.getName());

	/**
	 * 
	 */
	public ModalHelper() {

		// TODO Auto-generated constructor stub
	}

	private static WebElement openModal(FacebookDriver driver,
			WebElement trigger, By modalSelector) {

		trigger.sendKeys(Keys.ENTER);

		try {
			WebElement modal = driver.findElement(modalSelector);

			logger.log(
					Level.INFO,
					String.format("Found possible modal: %s",
							PrettyPrint.element(modal)));

			// wait for the "has-content" class; it suggests the modal is fully
			// loaded and ready
			while (!modal.getAttribute("class").contains("has-content")) {
				Thread.sleep(10);
			}

			logger.log(Level.INFO, String.format("Found modal: %s",
					PrettyPrint.element(modal)));

			return modal;

		} catch (Exception e) {
			logger.log(Level.WARNING, e.getMessage());
		}

		// if we couldn't find the modal, just let the driver handle this
		return driver.waitForElementVisible(modalSelector);

	}

	private static void tabUntil(FacebookDriver driver, WebElement modal,
			WebElement until, int maxTabs) throws Exception {

		logger.log(
				Level.INFO,
				String.format("Attempting to tab until %s",
						PrettyPrint.element(until)));

		int count = 0;

		while (!driver.getActiveElement().equals(until)) {

			if (count > maxTabs) {
				throw new Exception(String.format("tabbed too many times (%d)",
						count));
			}

			WebElement currentlyActive = driver.getActiveElement();

			logger.log(Level.INFO, String.format(
					"Currently active (at tab#%d): %s", count, currentlyActive));

			/**
			 * selenium is really, really stupid. for whatever reason, it cannot
			 * send keys to the
			 * <code><div role="button" aria-live="..." tabindex="0" /></code>
			 * we're adding, so we've got to hack around it. We're grabbing its
			 * parent, the grabbing the span immediately next to it. This is not
			 * something I'm proud of.
			 */
			if (PrettyPrint.element(currentlyActive).equals(
					"div[role=\"button\"][tabindex=\"0\"]")) {
				// grab the parent element
				WebElement parent = currentlyActive.findElement(By.xpath(".."));

				currentlyActive = parent.findElement(By.tagName("span"));
			}

			try {
				currentlyActive.sendKeys(Keys.TAB);
			} catch (ElementNotVisibleException e) {
				logger.log(Level.WARNING, e.getMessage());
			}

			count++;
		}

		assertWebElement(driver.getActiveElement(), until);
	}

	private static void tabUntil(FacebookDriver driver, WebElement modal,
			By untilSelector, int maxTabs) throws Exception {

		WebElement until = modal.findElement(untilSelector);

		tabUntil(driver, modal, until, maxTabs);
	}

	private static void escapeClosesModal(FacebookDriver driver,
			WebElement trigger, WebElement modal) throws InterruptedException {

		WebElement someAnchor = modal.findElement(By.tagName("a"));
		logger.log(
				Level.INFO,
				String.format("Sending ESCAPE from %s",
						PrettyPrint.element(someAnchor)));

		someAnchor.sendKeys(Keys.ESCAPE);

		// let the js fire
		Thread.sleep(100);

		assertWebElement("The trigger should regain focus",
				driver.getActiveElement(), trigger);

	}

	private static void closeButtonResetsFocus(FacebookDriver driver,
			WebElement trigger, WebElement modal, By closeSelector)
			throws InterruptedException {

		WebElement close = modal.findElement(closeSelector);

		close.sendKeys(Keys.RETURN);

		// let the js fire
		Thread.sleep(100);

		assertWebElement("The trigger should regain focus",
				driver.getActiveElement(), trigger);

	}

	public static void verify(FacebookDriver driver, WebElement trigger,
			By modalSelector, By closeSelector, int maxTabs) throws Exception {

		WebElement modal = openModal(driver, trigger, modalSelector);

		logger.log(Level.INFO,
				String.format("Found modal: %s", PrettyPrint.element(modal)));

		tabUntil(driver, modal, closeSelector, maxTabs);

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Attempting to verify circular tabbing");

		tabUntil(driver, modal, active, maxTabs);

		logger.log(Level.INFO, "Attempting to ESC closes modal");

		escapeClosesModal(driver, trigger, modal);

		modal = openModal(driver, trigger, modalSelector);

		logger.log(Level.INFO, String.format("Re-opened modal: %s",
				PrettyPrint.element(modal)));

		logger.log(Level.INFO,
				"Attempting to verify the close button will reset focus");

		closeButtonResetsFocus(driver, trigger, modal, closeSelector);

		logger.log(
				Level.INFO,
				String.format("Modal (%s) appears to work :)",
						PrettyPrint.selector(modalSelector)));

	}


}
