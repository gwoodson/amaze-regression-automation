package com.deque.amaze.va;

import static com.deque.junit.Assert.pass;
import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class TwitVetBenefits {

	private static Logger logger = Logger.getLogger(TwitVetBenefits.class.getName());

	private static TwitterDriver twitterdriver = TwitVASuite.twitterDriver;
	
	private static TwitterHelper twitterHelper = new TwitterHelper();
	private static FindTweet findTweet = new FindTweet();
	private static TwitterModalHelper twitterModal = new TwitterModalHelper();
	private static WebElement retweet;
	private static WebElement favoriteTweet;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		String url = "https://twitter.com/VAVetBenefits";
		twitterHelper.goToURL(twitterdriver, url);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
	}

	@Test
	@Order(order = 1)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
		
		String expectedHeader = "h1";		
		WebElement skipLink = twitterHelper.skipLinkExistance(twitterdriver);
		
		twitterHelper.verifySkiplink(twitterdriver, expectedHeader);
		
	}	
	
	@Test
	@Order(order = 2)
	public void findFavoritedTweet() throws Exception {

		Thread.sleep(1500L);
		
		favoriteTweet = findTweet.favorited(twitterdriver);

		logger.log(
				Level.INFO,
				String.format("Found favorited tweet: %s",
						PrettyPrint.element(favoriteTweet)));
		String className = favoriteTweet.getAttribute("class");

		className = " " + className + " ";

		if (className.contains(" opened-tweet ")) {
			pass("Tweet already expanded");
			return;
		}

		WebElement expandLink = favoriteTweet.findElement(By
				.cssSelector("a.details.with-icn.js-details"));

		expandLink.sendKeys(Keys.RETURN);

	}

	@Test
	@Order(order = 3)
	public void verifyModal() throws Exception {

		WebElement trigger = favoriteTweet.findElement(By
				.cssSelector("a.request-favorited-popup"));

		By modalSelector = By.cssSelector("#activity-popup-dialog");
		By closeSelector = By
				.cssSelector("button.modal-btn.modal-close.js-close");

		twitterModal.verify(twitterdriver, trigger, modalSelector,
				closeSelector, 200);

	}
	
	@Test
	@Order(order = 4)
	public void findRetweetedTweet() throws Exception {

		retweet = findTweet.retweeted(twitterdriver);

		logger.log(
				Level.INFO,
				String.format("Found retweeted tweet: %s",
						PrettyPrint.element(retweet)));
		
		String className = retweet.getAttribute("class");

		className = " " + className + " ";

		if (className.contains(" opened-tweet ")) {
			pass("Tweet already expanded");
			return;
		}

		WebElement expandLink = retweet.findElement(By
				.cssSelector("a.details.with-icn.js-details"));

		expandLink.sendKeys(Keys.RETURN);

	}

	@Test
	@Order(order = 5)
	public void verifyRetweetedModal() throws Exception {

		WebElement trigger = retweet.findElement(By
				.cssSelector("a.request-retweeted-popup"));

		By modalSelector = By.cssSelector("#activity-popup-dialog");
		By closeSelector = By
				.cssSelector("button.modal-btn.modal-close.js-close");

		twitterModal.verify(twitterdriver, trigger, modalSelector,
				closeSelector, 200);

	}

}
