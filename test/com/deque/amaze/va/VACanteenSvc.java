package com.deque.amaze.va;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class VACanteenSvc {
	
	private static Logger logger = Logger.getLogger(VACanteenSvc.class.getName());

	private static FacebookDriver driver = VASuite.driver;
	
	private static Helper helper = new Helper();
	private static AboutHelper about = new AboutHelper();


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/VACanteenService");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService");
		
		String expectedHeader = "h2";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
	}	
	
	@Test
	@Order(order = 2)
	public void verifyAboutPage() throws MalformedURLException, Exception {	
		
		driver.navigateTo("http://www.facebook.com/VACanteenService/info");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService/info");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		about.mapPopup(driver, skipLink);
				
	}
	
	@Test
	@Order(order = 3)
	public void otherVAPages() throws MalformedURLException, Exception {
		//
		driver.navigateTo("http://www.facebook.com/VACanteenService/app_265971486778642");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService/app_265971486778642");		
		
		String expectedHeader = "h2";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
		
	@Test
	//@Ignore
	@Order(order = 4)
	public void likesPage() throws MalformedURLException, Exception {
		//
		driver.navigateTo("http://www.facebook.com/VACanteenService/likes");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService/likes");		
		
		String expectedHeader = "h3";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		//helper.verifySkiplink(driver, skipLink, expectedHeader);
		helper.verifyLikes(driver);
	}
	
	@Test
	@Order(order = 5)
	public void disclaimerPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/VACanteenService/app_174701229285682");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService/app_174701229285682");		
		
		String expectedHeader = "h2";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
	
	@Test
	@Order(order = 6)
	public void vaNotes() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/VACanteenService/notes");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService/notes");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
	}
	
	@Test
	@Order(order = 7)
	public void liveStream() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/VACanteenService/app_142371818162");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService/app_142371818162");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		helper.verifyPoliteness(driver);
		helper.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order = 8)
	public void verifyPageMap() throws MalformedURLException, Exception {	
		
		driver.navigateTo("http://www.facebook.com/VACanteenService/page_map");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService/page_map");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		about.mapPopup(driver, skipLink);
				
	}	
	
	@Test
	@Order(order = 9)
	public void crisisInfo() throws MalformedURLException, Exception {
		
		//
		driver.navigateTo("https://www.facebook.com/VACanteenService/app_343868232343472");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VACanteenService/app_343868232343472");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}

}
