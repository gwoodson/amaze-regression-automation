package com.deque.amaze.va;

import static org.junit.Assert.fail;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.deque.amaze.regression.Driver;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.amaze.regression.twitter.TwitterDriver;

@RunWith(Suite.class)
@SuiteClasses({ 
	VetBenefits.class,
	VANationCem.class,
	GIBillEdu.class, 
	VACanteenSvc.class, 
	VetAffairs.class,
	VetHealth.class,
	VAWelcomeHm.class,
	VetCareers.class,
	VAPTSD.class,
	VAInnovation.class	
	})

public class VASuite {
	
	public static FacebookDriver driver;
	//public static TwitterDriver twitterDriver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(VASuite.class
				.getResourceAsStream("va.properties"));

		driver = new FacebookDriver(options);
		
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		//driver.getDriver().quit();

	}

}
