package com.deque.amaze.va;

import static org.junit.Assert.fail;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.deque.amaze.regression.twitter.TwitterDriver;


@RunWith(Suite.class)
@SuiteClasses({ 
	TwitVetBenefits.class, 
	TwitVetAffairs.class,
	TwitPTSDInfo.class, 
	TwitVetCanteen.class,
	TwitVetHealth.class, 
	TwitVetInnovation.class, 
	TwitVetCareers.class, 
	TwitOEFOIF.class,
	TwitVACemeteries.class, 
	TwitPost911GIBill.class
	
})

public class TwitVASuite {
	
	public static TwitterDriver twitterDriver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(TwitVASuite.class
				.getResourceAsStream("va.properties"));

		twitterDriver = new TwitterDriver(options);
		
		twitterDriver.login();

		if (twitterDriver.isReCaptcha()) {
			fail("Landed on the reCaptcha page -- cannot continue tests.");

			// there's got to be a better way of handling this; exiting the
			// process is so... not good.
			System.exit(1);
		}
		
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		twitterDriver.getDriver().quit();

	}

}
