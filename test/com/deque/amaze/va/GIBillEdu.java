package com.deque.amaze.va;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class GIBillEdu {
	
	private static Logger logger = Logger.getLogger(GIBillEdu.class.getName());

	private static FacebookDriver driver = VASuite.driver;
	
	private static Helper helper = new Helper();
	private static UStreamHelper ustream = new UStreamHelper();
	private static AboutHelper about = new AboutHelper();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/gibillEducation");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/gibillEducation");
		
		String expectedHeader = "h2";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
	}	
	
	@Test
	@Order(order = 2)
	public void verifyAboutPage() throws MalformedURLException, Exception {	
		
		driver.navigateTo("http://www.facebook.com/gibillEducation/info");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/gibillEducation/info");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		about.mapPopup(driver, skipLink);
				
	}
	
	@Test
	@Order(order = 3)
	public void vaNotes() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/gibillEducation/notes");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/gibillEducation/notes");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
	}
	
	@Test
	@Order(order = 4)
	public void faqAssistPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/gibillEducation/app_291165214231737");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/gibillEducation/app_291165214231737");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);

		helper.verifyPoliteness(driver);
		helper.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order = 5)
	public void verifyPageMap() throws MalformedURLException, Exception {	
		
		driver.navigateTo("http://www.facebook.com/gibillEducation/page_map");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/gibillEducation/page_map");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		about.mapPopup(driver, skipLink);
				
	}
	
	@Test
	@Order(order=6)
	public void likesPage() throws MalformedURLException, Exception {
		//
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/likes");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/likes");		
		
		String expectedHeader = "h3";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		//helper.verifySkiplink(driver, skipLink, expectedHeader);
		helper.verifyLikes(driver);
	}
	
}
