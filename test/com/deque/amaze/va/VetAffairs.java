package com.deque.amaze.va;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import com.deque.amaze.facebook.FacebookSuite;
import com.deque.amaze.facebook.HelpModal;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class VetAffairs {
	
	private static Logger logger = Logger.getLogger(VetAffairs.class.getName());

	private static FacebookDriver driver = VASuite.driver;
	
	private static Helper helper = new Helper();
	private static UStreamHelper ustream = new UStreamHelper();
	private static AboutHelper about = new AboutHelper();	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
		
		driver.navigateTo("http://facebook.com/VeteransAffairs");
		if (!driver.isLoggedIn()) {
			driver.login();		}
		
		driver.injectAmazeAsExtension("facebook.com", "/VeteransAffairs");

		String expectedHeader = "h2";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
	}	
	
	@Test
	@Ignore
	@Order(order = 2)
	public void verifyAboutPage() throws MalformedURLException, Exception {	
		
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/info");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/info");		
				
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		about.mapPopup(driver, skipLink);
		about.verifyHistoryModal(driver);
		about.verifyFoursquare(driver);
				
	}
	
	@Test
	@Order(order = 3)
	public void otherVAPages() throws MalformedURLException, Exception {
		//
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/app_265971486778642");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/app_265971486778642");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
		
	@Test
	@Order(order = 4)
	public void likesPage() throws MalformedURLException, Exception {
		//
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/likes");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/likes");		
		
		String expectedHeader = "h3";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		helper.verifyLikes(driver);
	}
	
	@Test
	@Order(order = 5)
	public void disclaimerPage() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/app_174701229285682");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/app_174701229285682");		
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}
	
	@Test
	@Order(order = 6)
	public void vaNotes() throws MalformedURLException, Exception {
		
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/notes");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/notes");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
	}
	
	@Test
	@Order(order = 7)
	public void vaEvents() throws MalformedURLException, Exception {
		
		//
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/events");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/events");		
		
		String expectedHeader = "h3";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
	}

	@Test
	@Order(order = 8)
	public void vaUStream() throws MalformedURLException, Exception {
		//
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/app_196506863720166");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/app_196506863720166");		
		
		driver.waitForElementVisible(By.id("mainContainer"));
		
		String expectedHeader = "div";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);		
		
		//ustream.verifyShareModal(driver);

	}
	
	@Test
	@Order(order = 9)
	public void verifyPageMap() throws MalformedURLException, Exception {	
		
		driver.navigateTo("http://www.facebook.com/VeteransAffairs/page_map");
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
		driver.injectAmazeAsExtension("www.facebook.com", "/VeteransAffairs/page_map");		
		
		String expectedHeader = "h4";		
		WebElement skipLink = helper.skipLinkExistance(driver);
		
		helper.verifySkiplink(driver, skipLink, expectedHeader);
		
		about.mapPopup(driver, skipLink);
		about.verifyHistoryModal(driver);
		about.verifyFoursquare(driver);
				
	}

}
