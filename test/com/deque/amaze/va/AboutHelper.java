package com.deque.amaze.va;

import static org.junit.Assert.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.*;

import com.deque.amaze.facebook.FacebookSuite;
import com.deque.amaze.facebook.HelpModal;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.amaze.regression.twitter.VerifyModal;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

public class AboutHelper {
	
	private static Logger logger = Logger.getLogger(AboutHelper.class
			.getName());
	
	private static Helper helper = new Helper();
	private static ModalHelper modalhelper = new ModalHelper();	

	public AboutHelper() {

	}
	
	/*
	 * Verify Map Popup 
	 * Scenario:
	 * 		About page opens with map displayed.  
	 * 		User tabs off skiplink to About header.
	 * 		User sends keys:  Shift+Tab and tabs back to map 
	 */
	public void mapPopup(FacebookDriver driver, WebElement skiplink) throws Exception {
		
		Thread.sleep(2500L);
		
		skiplink.sendKeys(Keys.ENTER);
		
		driver.getActiveElement().sendKeys(Keys.SHIFT, Keys.TAB);
		
		Thread.sleep(1500L);
		
		driver.getActiveElement().sendKeys(Keys.ESCAPE);
		
		Thread.sleep(1500L);
		
		//check for map to be hidden to verify map closed
		
		//Open Map
		WebElement openMap = driver.findElement(By.cssSelector("div.fbAggregatedMapPin"));
		
		openMap.click();
		
		Thread.sleep(1500L);
		
		WebElement closeButton = driver.findElement(By.cssSelector("div.closeButton")).findElement(By.tagName("a"));
		
		logger.log(
				Level.INFO,
				String.format("Found Close button link: %s",
						PrettyPrint.element(closeButton)));
		
		helper.shiftTabUntilClose(driver, closeButton, 100);
		
		closeButton.sendKeys(Keys.ENTER);
				
	}
	
	public void verifyHistoryModal(FacebookDriver driver) throws Exception {

		List<WebElement> links = driver.findElements(By.cssSelector("span.fbProfileBylineLabel"));
		
		String name = links.get(2).findElement(By.tagName("a")).getClass().getName();
		
		logger.log(Level.INFO,
				String.format("Found history href class: %s",
						name));
		
		WebElement trigger = links.get(2).findElement(By.tagName("a"));

		By modalSelector = By.cssSelector("div.uiContextualLayer._53il.uiContextualLayerBelowCenter");
		
		By closeSelector = By
				.cssSelector("a.accessible_elem.layer_close_elem");

		modalhelper.verify(driver, trigger, modalSelector,
				closeSelector, 200); 
		
	}
	
	public void verifyFoursquare(FacebookDriver driver) throws Exception {
		
		//
		WebElement fourSquare = driver.findElement(By.cssSelector("li.pagesElsewhere")).findElement(By.tagName("a"));
		
		logger.log(
				Level.INFO,
				String.format("Found FourSquare link: %s",
						PrettyPrint.element(fourSquare)));
		
		helper.tabUntil(driver, fourSquare, 100);
	}	

}
