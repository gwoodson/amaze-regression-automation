package com.deque.amaze.testuff;

import static org.junit.Assert.*;

import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.Security;

import javax.management.MalformedObjectNameException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class proof {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testuff_pass() throws Exception { 
		  try { 
		    String lab_name = "sanity";
		    String test_ID = "1";
		// replace test ID with correct value from Testuff
		    String test_status = "passed";
		    String tester_name = "Greta Woodson";
		    String comment = "comment text";
		    String token = "OxzYi2WEIclNTbBaMabblDbi";
		    // replace token with your own token value from Testuff API settings screen

		    //System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol"); 
		    //Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider()) ; 
		    String postBody = 
		      "token=" + URLEncoder.encode(token, "UTF-8") +
		      "&lab_name=" + URLEncoder.encode(lab_name, "UTF-8") + 
		      "&test_id=" + URLEncoder.encode(test_ID, "UTF-8") + 
		      "&status=" + URLEncoder.encode(test_status, "UTF-8") + 
		      "&tester_name=" + URLEncoder.encode(tester_name, "UTF-8") + 
		      "&comment=" + URLEncoder.encode(comment, "UTF-8"); 

		    // call Testuff server
		    URL url = new URL("https://service4.testuff.com/api/v0/run/?lab=sanity/ -u greta.woodson@deque.com:andrew01");
		    // replace company_id and host name values from Testuff API settings screen
		    URLConnection conn = url.openConnection();
		    conn.setDoOutput(true); 
		    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream()); 
		    wr.write(postBody); 
		    wr.flush();
		    wr.close();
		  } 
		  catch ( Exception e) {System.out.println(e.toString());} 
		}

}
