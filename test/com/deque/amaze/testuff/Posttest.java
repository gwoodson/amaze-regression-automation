package com.deque.amaze.testuff;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;
	 
	public class Posttest {
	 
	    /**
	     * @param args the command line arguments
	     */
	    public static enum STATUS {
	        passed, maybe, failed
	    };
	    final static String login = "greta.woodson@deque.com";
	    final static String pass = "andrew01";
	    // Change serviceX to the correct data center 
	    final static String url = "https://service4.testuff.com/api/v0/run/?lab=sanity/ -u greta.woodson@deque.com:andrew01 --insecure";
	 
	    public static void main(String[] args) throws MalformedURLException, IOException {
	        // TODO code application logic here
	 	    	
	        //if(args.length==10)
	        try {
	            postTestuff("1",STATUS.valueOf("passed"), "1", "1", "1"
	                    , "sanity", "", "", "some comment", "1");
	        } catch (Exception e) {
	            System.out.println(e);
	        }
	       // else{
	        //    System.out.println("Usage: java Posttest test_id status steps_failed steps_passed steps_maybe lab_name version run_configuration comment branch_name");
	        //    System.out.println("All arguments are required and in order, use \"\" for null values.");
	       // }
	         
	    }
	 
	    public static void postTestuff(String test_id, STATUS status, String steps_failed, String steps_passed, String steps_maybe,
	            String lab_name, String version, String run_configuration, String comment, String branch_name) throws MalformedURLException, IOException {
	 
	        Map<String, String> m = new HashMap<String, String>();
	        m.put("test_id", test_id);
	        m.put("status", status.toString());
	        m.put("steps_failed", steps_failed);
	        m.put("steps_passed", steps_passed);
	        m.put("steps_maybe", steps_maybe);
	        m.put("lab_name", lab_name);
	        m.put("version", version);
	        m.put("run_configuration", run_configuration);
	        m.put("branch_name", branch_name);
	        m.put("comment", comment);
	 
	        JSONObject j = new JSONObject(m);
	 
	        System.out.println(j.toString());
	         
	        byte[] data = j.toString().getBytes();
	        
	        URL u = new URL(url);
	        HttpsURLConnection conn = (HttpsURLConnection) u.openConnection();
	 
	        String encoding = new sun.misc.BASE64Encoder().encode((login + ":" + pass).getBytes());
	        conn.setRequestProperty("Content-Type", "application/json");
	        conn.setRequestProperty("Content-Length", "" + data.length);
	        conn.addRequestProperty("Authorization", "Basic " + encoding);
	        conn.addRequestProperty("Accept", "application/xml");
	        conn.setDoInput(true);
	        conn.setDoOutput(true);
	        conn.setRequestMethod("POST");
	        System.out.println(encoding);
	        System.out.println(data.length);
	        OutputStream os = conn.getOutputStream();
	        os.write(data);
	        os.flush();
	 
	        // Get the response
	        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        System.out.println("Response code:" + conn.getResponseCode());
	        String line;
	        while ((line = rd.readLine()) != null) {
	            System.out.println(line);
	        }
	        rd.close();
	        Map<String, List<String>> headerFields = conn.getHeaderFields();
	        for (String s : headerFields.keySet()) {
	            System.out.println(s + ":" + headerFields.get(s));
	        }
	    }
	}



