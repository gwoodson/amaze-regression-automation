/**
 * 
 */
package com.deque.amaze.chase;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.RegressionDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * Verify Login labels are broken font size = 0
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 * @author Greta Woodson <greta.woodson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class LoginLabelsBroken {

	private static Logger logger = Logger.getLogger(LoginLabelsBroken.class
			.getName());

	private static RegressionDriver driver = ChaseSuite.driver;

	private static WebElement usernameLabel;
	private static WebElement passwordLabel;
	private static String fontsize = "0px";

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		// not injecting Amaze; this is verifying things are broken, not fixed

		driver.waitForElementVisible(By.name("homeLogonForm"));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	@Order(order = 1)
	public void findInputs() {

		driver.findElement(By.id("usr_name_home"));

		driver.findElement(By.id("usr_password_home"));

	}

	@Test
	@Order(order = 2)
	public void findLabels() {

		usernameLabel = driver.findElement(By
				.cssSelector("label[for='usr_name_home']"));

		passwordLabel = driver.findElement(By
				.cssSelector("label[for='usr_password_home']"));

	}

	/*
	 * Verify labels font size is zero -voice over will not announce when
	 * font-size is zero
	 */
	@Test
	@Order(order = 3)
	public void userlabelHasProperFontSize() {

		// Verify Username
		String usernameFontSize = usernameLabel.getCssValue("font-size");

		logger.log(Level.INFO, "Username Label actual font-size = " + usernameFontSize);

		assertEquals("username font-size:  ", fontsize, usernameFontSize);

	}

	@Test
	@Order(order = 4)
	public void passwordlabelHasProperFontSize() {

		// Verify Password

		String passwordFontSize = passwordLabel.getCssValue("font-size");

		logger.log(Level.INFO, "Password Label actual font-size = " + passwordFontSize);

		assertEquals("password font-size:  ", fontsize, passwordFontSize);

	}

}
