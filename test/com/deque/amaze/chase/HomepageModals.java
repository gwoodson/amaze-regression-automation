/**
 * 
 */
package com.deque.amaze.chase;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.RegressionDriver;
import com.deque.amaze.regression.common.Modal;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class HomepageModals {

	private static Logger logger = Logger.getLogger(HomepageModals.class
			.getName());

	private static RegressionDriver driver = ChaseSuite.driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.injectAmaze();

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	@Order(order = 1)
	public void contactUsModal() throws Exception {

		WebElement trigger = driver.findElement(By
				.cssSelector("a.chasejs-flyoutlink.chaseui-contactlink"));

		logger.log(Level.INFO, "Trigger: " + PrettyPrint.element(trigger));

		Modal modal = new Modal(driver, trigger,
				By.id("chasejs-contexthelpfloater"),
				By.cssSelector("a.chaseui-button.chasejs-flyoutclose"
						+ ".chaseui-flyoutclose.chaseui-icon-close"));

		modal.verify();

	}

	@Test
	@Order(order = 2)
	public void resourcesModals() throws Exception {

		List<WebElement> triggers = driver
				.findElements(By
						.cssSelector("a.dynamically-built-ajax-flyout-link.sliding-door-button"));
		for (WebElement trigger : triggers) {
			Modal modal = new Modal(
					driver,
					trigger,
					By.id("chasejs-contexthelpfloater"),
					By.cssSelector("a.chaseui-button.chasejs-flyoutclose.chaseui-flyoutclose"));
			modal.verify();
		}
	}
}
