/**
 * 
 */
package com.deque.amaze.chase;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.deque.amaze.regression.chase.ChaseDriver;

/**
 * Suite runner. Uses {@link com.deque.amaze.regression.chase.ChaseDriver} to
 * interact with Selenium.
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(Suite.class)
@SuiteClasses({
	LoginLabelsBroken.class,
	LoginLabelsFixed.class,
	HomepageModals.class
})
public class ChaseSuite {

	public static ChaseDriver driver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(ChaseSuite.class.getResourceAsStream("chase.properties"));

		driver = new ChaseDriver(options);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		driver.getDriver().quit();

	}

}
