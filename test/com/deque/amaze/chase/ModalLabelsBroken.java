package com.deque.amaze.chase;

import static org.junit.Assert.*;

import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.deque.amaze.regression.RegressionDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;
/**
 * Verify Login labels are broken font size = 0
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 * @author Greta Woodson <greta.woodson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class ModalLabelsBroken {
	
	private static Logger logger = Logger.getLogger(ModalLabelsBroken.class
			.getName());

	private static RegressionDriver driver = ChaseSuite.driver;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void test() {
		fail("Not yet implemented");
	}

}
