package com.deque.amaze.twitter;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;

/**
 * Validates the an individual status page by performing the following:
 * <ol>
 * <li>Randomly choose a tweet from the home page</li>
 * <li>Navigating to the tweet's "permalink/status page"</li>
 * <li>Clicks the subject tweet's "retweeted" button, opening modal</li>
 * <li>Verifies keyboard functionality within the modal</li>
 * </ol>
 * 
 * @author Greta Woodson <greta.woodson@deque.com> & Stephen Mathieson
 *         <stephen.mathieson@deque.com>
 */
public class StatusPage {

	private static Logger logger = Logger.getLogger(StatusPage.class.getName());

	private static TwitterDriver driver = TwitterSuite.driver;
	private static WebElement modal;
	private static WebElement permaTweet;
	private static WebElement retweetActivityLink;

	private static List<WebElement> tweets;
	private static WebElement tweet;

	/**
	 * Navigate to the home page, but don't inject Amaze. We're doing that at
	 * {@link StatusPage#statusPageLoaded()} instead.
	 * 
	 * @throws MalformedURLException
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws MalformedURLException {

		driver.navigateTo("https://twitter.com/");
		driver.waitForPageContainer();

	}

	/**
	 * Gather all tweets on the page
	 */
	@Test
	public void gatherAllTweets() {

		tweets = driver.findElements(By.cssSelector("div.tweet"));

		logger.log(Level.INFO, String.format("Found %d tweets", tweets.size()));

	}

	/**
	 * Choose a random tweet
	 */
	@Test
	public void chooseRandomTweet() {

		Random random = new Random();
		int index = random.nextInt(tweets.size());

		tweet = tweets.get(index);

		logger.log(
				Level.INFO,
				String.format("Choose tweet #%d: %s", index,
						PrettyPrint.element(tweet)));

	}

	/**
	 * Expand the randomly choosen tweet
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void expandChoosenTweet() throws InterruptedException {

		WebElement expandLink = tweet.findElement(By
				.cssSelector("a.with-icn.js-details"));

		logger.log(Level.INFO,
				"Expand link: " + PrettyPrint.element(expandLink));

		expandLink.sendKeys(Keys.RETURN);

		Thread.sleep(5000);

		String itemId = tweet.getAttribute("data-item-id");
		String selector = "div.tweet[data-item-id='" + itemId + "']";
		By expandedBox = By.cssSelector(selector
				+ " a.permalink-link.js-permalink.js-nav");

		logger.log(Level.INFO, PrettyPrint.selector(expandedBox));

		driver.waitForElementVisible(expandedBox);

	}

	/**
	 * Get the "permalink" and click it
	 * 
	 * @throws MalformedURLException
	 */
	@Test
	public void getToStatusPage() throws MalformedURLException {

		WebElement link = tweet.findElement(By
				.cssSelector("a.permalink-link.js-permalink.js-nav"));

		logger.log(Level.INFO, "Link: " + PrettyPrint.element(link));

		String href = link.getAttribute("href");
		logger.log(Level.INFO, "Permalink URL: " + href);

		driver.navigateTo(href);
	}

	/**
	 * Wait for the status page to load, then inject Amaze
	 */
	@Test
	public void statusPageLoaded() {

		driver.waitForPageContainer();
		driver.injectAmaze();

	}

	/**
	 * All user-styles should be removed
	 */
	@Test(expected = NoSuchElementException.class)
	public void removesUserStyles() {

		driver.findElement(By.className("js-user-style"));
	}

	/**
	 * All user-background styles should be removed
	 */
	@Test(expected = NoSuchElementException.class)
	public void removesUserBackgroundImages() {

		driver.findElement(By.className("js-user-style-bg-img"));
	}

	/**
	 * Get the "main" tweet container; will throw if it's not present
	 */
	@Test
	public void getPermaTweet() {

		// throws if not found
		permaTweet = driver.findElement(By
				.cssSelector(".permalink-tweet-container .permalink-tweet"));

	}

	/**
	 * Get the retweeted link; will throw and fail the test if the link is not
	 * present
	 */
	@Test
	public void getRetweetedLink() {

		// throws if not found
		retweetActivityLink = permaTweet.findElement(By
				.cssSelector("a.request-retweeted-popup"));

	}

	/**
	 * Open the modal by sending a RETURN key event to the
	 * "retweet activity link"
	 */
	@Test
	public void openActivityDialog() {

		retweetActivityLink.sendKeys(Keys.RETURN);

		driver.waitForElementVisible(By
				.cssSelector("#activity-popup-dialog h3.modal-title"));

		modal = driver.findElement(By.id("activity-popup-dialog"));

	}

	/**
	 * Wait for all of the "activity" content to load
	 */
	@Test
	public void waitForActivityContent() {

		// wait for the loading GIF to leave
		driver.waitForElementHidden(By
				.cssSelector("#activity-popup-dialog .loading"));

	}

	/**
	 * Verify the title was automagically focused via sheer awesomeness also
	 * cats
	 */
	@Test
	public void autoFocusTitle() {

		WebElement title = modal.findElement(By.cssSelector("h3.modal-title"));

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active element: " + PrettyPrint.element(active));

		assertEquals(title, active);

	}

	/**
	 * Continue to tab until one of the two conditions occur:
	 * <ul>
	 * <li>10 seconds elapses (bad)</li>
	 * <li>We reach the close button (good)</li>
	 * </ul>
	 */
	@Test(timeout = 10000)
	public void tabUntilClose() {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		int counter = 0;

		while (!driver.getActiveElement().equals(close)) {
			logger.log(Level.INFO, String.format("Tab #%d", counter));

			driver.getActiveElement().sendKeys(Keys.TAB);

			logger.log(
					Level.INFO,
					"Currently active element: "
							+ PrettyPrint.element(driver.getActiveElement()));

			counter += 1;
		}

		WebElement active = driver.getActiveElement();

		assertEquals(close, active);
	}

	/**
	 * Validate that pressing "RETURN" on the close button will exit the modal
	 */
	@Test
	public void returnOnClose() {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		close.sendKeys(Keys.RETURN);

		// verify the modal has closed -- will throw if it hasn't
		driver.waitForElementHidden(By
				.cssSelector("#activity-popup-dialog h3.modal-title"));

	}

	/**
	 * Validate that upon closing the modal, focus is returned to the element
	 * which instantiated it
	 */
	@Test
	public void focusIsReturned() {

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active: " + PrettyPrint.element(active));

		assertEquals(retweetActivityLink, active);

	}

}
