/**
 * 
 */
package com.deque.amaze.twitter;

import static org.junit.Assert.*;
import static com.deque.junit.Assert.assertWebElement;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.RegressionDriver;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class GlobalOverlay {

	private static RegressionDriver driver = TwitterSuite.driver;

	/**
	 * Navigate to the correct URL and inject Amaze before launching any of the
	 * tests
	 * 
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");

		driver.injectAmaze();
	}

	/**
	 * Validation for the inserted `li#amaze-skip-to-tweets`
	 */
	@Test
	public void skipLinkListItem() {

		WebElement li = driver.findElement(By.id("amaze-skip-to-tweets"));

		String nodeName = li.getTagName().toLowerCase();

		assertEquals("Should be a LI", "li", nodeName);

		WebElement parent = li.findElement(By.xpath(".."));

		nodeName = parent.getTagName().toLowerCase();

		assertEquals("Parent should be an UL", "ul", nodeName);
	}

	/**
	 * Multiple assertions:
	 * 
	 * <ul>
	 * <li>skip-link should be an anchor</li>
	 * <li>skip-link should be first child of #amaze-skip-to-this</li>
	 * <li>skip-link should have the correct HREF</li>
	 * <li>when pressing ENTER, the skip link should focus the page-header</li>
	 * <li>page-header should be an H1</li>
	 * <li>page-header should be inserted by Amaze</li>
	 * </ul>
	 */
	@Test
	public void skipLinkAnchor() {

		WebElement anchor = driver.findElement(By
				.id("amaze-skip-to-tweets-link"));

		assertEquals("a", anchor.getTagName().toLowerCase());

		WebElement parent = anchor.findElement(By.xpath(".."));

		assertEquals("Parent should be an LI", "li", parent.getTagName()
				.toLowerCase());
		assertEquals("Parent's ID should be 'amaze-skip-to-tweets'",
				"amaze-skip-to-tweets", parent.getAttribute("id"));

		String href = anchor.getAttribute("href");

		assertTrue("The skip-link should point to #amaze-skip-to-this",
				href.endsWith("#amaze-skip-to-this"));

		anchor.sendKeys(Keys.RETURN);

		WebElement skipToThis = driver.findElement(By.id("amaze-skip-to-this"));

		assertEquals("h1", skipToThis.getTagName().toLowerCase());

		String skipClassName = " " + skipToThis.getAttribute("class") + " ";

		assertTrue(skipClassName.contains(" amaze "));

		WebElement activeElement = driver.getActiveElement();

		assertEquals(skipToThis, activeElement);
	}

	/**
	 * Gathers all tweets on the page, then makes many assertions on each of
	 * them:
	 * 
	 * <ul>
	 * <li>first-child should be an offscreen div</li>
	 * <li>offscreen div should contain the next "New Tweet by {username}"</li>
	 * </ul>
	 */
	@Test
	public void offscreenNewTweetDiv() {

		List<WebElement> tweets = driver.findElements(By
				.cssSelector("div.tweet"));

		for (WebElement tweet : tweets) {

			WebElement firstChild = tweet.findElement(By
					.cssSelector("div.amaze-offscreen"));

			assertWebElement("offscreen-div should be first child of a tweet",
					tweet, firstChild.findElement(By.xpath("..")));

			String author = tweet.getAttribute("data-name");

			String expected = "New Tweet by " + author.trim();

			String actual = firstChild.getText().trim();

			assertEquals(expected, actual);

		}

	}

	/**
	 * All user-styles should be removed
	 */
	@Test(expected = NoSuchElementException.class)
	public void removesUserStyles() {

		driver.findElement(By.className("js-user-style"));
	}

	/**
	 * All user-background styles should be removed
	 */
	@Test(expected = NoSuchElementException.class)
	public void removesUserBackgroundImages() {

		driver.findElement(By.className("js-user-style-bg-img"));
	}

}
