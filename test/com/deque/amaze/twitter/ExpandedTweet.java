/**
 * 
 */
package com.deque.amaze.twitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static com.deque.junit.Assert.assertWebElement;

import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.Modal;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.amaze.regression.twitter.UseableTweet;

import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * Verify an "expanded" tweet in multiple steps:
 * <ul>
 * <li>Randomly choose a tweet to expand</li>
 * <li>Expand said tweet</li>
 * <li>Open the tweet's "retweeted" modal</li>
 * <ul>
 * <li>Verify auto-focus and tabbing within the modal</li>
 * <li>Verify ESC and RETURN (on the close button) will close the modal</li>
 * <li>Verify focus is reset when the modal is closed</li>
 * </ul>
 * <li>Open the tweet's "favorited" modal</li>
 * <ul>
 * <li>Verify auto-focus and tabbing within the modal</li>
 * <li>Verify ESC and RETURN (on the close button) will close the modal</li>
 * <li>Verify focus is reset when the modal is closed</li>
 * </ul>
 * </ul>
 * 
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class ExpandedTweet {

	private static Logger logger = Logger.getLogger(ExpandedTweet.class
			.getName());

	private static TwitterDriver driver = TwitterSuite.driver;

	private static String author;
	private static List<WebElement> tweets;
	private static WebElement tweet;
	private static WebElement modal;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();

	}

	@Test
	@Order(order = 1)
	public void gatherTweets() {

		tweets = driver.findElements(By.cssSelector("div.tweet"));

		int count = tweets.size();

		logger.log(Level.INFO, String.format("Found %d tweets", count));

	}

	/*@Ignore
	@Test
	@Order(order = 2)
	public void getTweet() {

		Random random = new Random();

		int index = random.nextInt(tweets.size());

		tweet = tweets.get(index);

		logger.log(
				Level.INFO,
				String.format("Chose tweet#%d: %s", index,
						PrettyPrint.element(tweet)));

		author = tweet.getAttribute("data-name").trim();

		logger.log(Level.INFO, String.format("Tweet author: %s", author));
	}
	
	@Test
	@Order(order = 3)
	public void expandTweet() throws InterruptedException {

		String itemId = tweet.getAttribute("data-item-id");
		String selector = "div.tweet[data-item-id='" + itemId + "']";

		WebElement expandLink = tweet.findElement(By
				.cssSelector("a.with-icn.js-details"));

		logger.log(Level.INFO, PrettyPrint.element(expandLink));

		expandLink.sendKeys(Keys.RETURN);

		By expandedBox = By
				.cssSelector(selector + " a.request-retweeted-popup");

		logger.log(Level.INFO, PrettyPrint.selector(expandedBox));

		driver.waitForElementVisible(expandedBox);

		logger.log(Level.INFO, "Tweet expanded! O_O");

		// refresh the tweet; WebDriver keeps the element cached
		tweet = driver.findElement(By.cssSelector(selector));

	}
*/
	@Test
	@Order(order = 2)
	public void getTweet() throws Exception {

		//Random random = new Random();

		//int index = random.nextInt(tweets.size());

		tweet = UseableTweet.retweeted(driver);

		//logger.log(
		//		Level.INFO,
		//		String.format("Chose tweet#%d: %s", PrettyPrint.element(tweet)));

		author = tweet.getAttribute("data-name").trim();

		logger.log(Level.INFO, String.format("Tweet author: %s", author));
	}
	//
	//
	//
	//
	// a bunch of helper/dry methods ... these should be moved into a "common"
	// class, as they're done in most regressions
	//
	//
	//
	//
	//

	/**
	 * Open either the "retweeted" or the "favorited" modal based on which link
	 * is provided
	 * 
	 * @param link
	 *            The link which opens the modal
	 */
	private static void openModal(WebElement link) {

		logger.log(Level.INFO,
				String.format("Link: %s", PrettyPrint.element(link)));

		link.sendKeys(Keys.RETURN);

		driver.waitForElementVisible(By
				.cssSelector("#activity-popup-dialog .modal-title"));

	}

	/**
	 * Press ESCAPE on the title of the modal, then wait for it to close
	 */
	private static void modalEscape() {

		WebElement header = modal.findElement(By.className("modal-title"));

		header.sendKeys(Keys.ESCAPE);

		driver.waitForElementHidden(By
				.cssSelector("#activity-popup-dialog .modal-title"));
	}

	/**
	 * Send a RETURN keypress to the close button of a modal
	 */
	private static void modalReturnOnClose() {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		close.sendKeys(Keys.RETURN);

		driver.waitForElementHidden(By
				.cssSelector("#activity-popup-dialog .modal-title"));
	}

	/**
	 * Keep tabbing until we've reached the "close" button on the modal
	 */
	private static void tabUntilModalClose() {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close.js-close"));
		int counter = 0;

		logger.log(Level.INFO, "Close button: " + PrettyPrint.element(close));

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO,
				"Currently active element: " + PrettyPrint.element(active));

		// keep sending a TAB to the active element until we reach the "close"
		// button
		while (!driver.getActiveElement().equals(close)) {

			logger.log(Level.INFO, String.format("Tab #%d", counter));

			WebElement currentlyActive = driver.getActiveElement();
			String selector = PrettyPrint.element(currentlyActive);

			logger.log(Level.INFO,
					String.format("Currently focued: %s", selector));

			// selenium is fucking stupid and cannot sendkeys to the <div />
			// we've had to add.
			// we're "skipping" this element
			if (selector.equals("div[role=\"button\"][tabindex=\"0\"]")) {
				WebElement parent = currentlyActive.findElement(By.xpath(".."));
				currentlyActive = parent.findElement(By
						.cssSelector("span.user-dropdown"));
			}

			// div[role=button][aria-label][tabindex=0]

			try {
				currentlyActive.sendKeys(Keys.TAB);
			} catch (ElementNotVisibleException e) {
				logger.log(Level.WARNING, e.getMessage());
			}

			WebElement active2 = driver.getActiveElement();
			logger.log(Level.INFO,
					"Currently active element: " + PrettyPrint.element(active2));

			counter += 1;
		}

		assertWebElement(close, driver.getActiveElement());

	}

	/**
	 * Wait for the modal's content to dynamically load
	 */
	private void waitForModalContent() {

		// wait for the loading GIF to leave
		driver.waitForElementHidden(By
				.cssSelector("#activity-popup-dialog .loading"));

	}

	@Test
	@Order(order = 3)
	public void openRetweetedModal() {

		logger.log(Level.INFO, PrettyPrint.element(tweet));

		openModal(tweet
				.findElement(By.cssSelector("a.request-retweeted-popup")));

		modal = driver.findElement(By.id("activity-popup-dialog"));

		logger.log(Level.INFO, "Retweeted modal opened");

	}

	/**
	 * Wait for all of the "activity" content to load
	 */
	@Test
	@Order(order = 4)
	public void waitForRetweetedModal() {

		waitForModalContent();

	}

	@Test
	@Order(order = 5)
	public void offscreenNewTweetDiv() {

		WebElement offscreen = modal.findElement(By
				.cssSelector("div.amaze-offscreen"));

		String text = offscreen.getText().trim().toLowerCase();

		assertTrue(text.contains("new tweet by"));

		String tabIndex = offscreen.getAttribute("tabindex");
		assertEquals("0", tabIndex);
	}

	@Test
	@Order(order = 6)
	public void headingIsFocused() {

		WebElement activeElement = driver.getActiveElement();

		WebElement expected = modal.findElement(By.className("modal-title"));

		assertEquals(expected, activeElement);

	}

	@Test
	@Order(order = 7)
	public void tabFromHeadingToOffscreenDiv() {

		WebElement header = modal.findElement(By.className("modal-title"));

		WebElement offscreen = modal.findElement(By
				.cssSelector("div.amaze-offscreen"));

		header.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();

		assertEquals(offscreen, activeElement);

	}

	/**
	 * Verify that pressing ESCAPE closes the modal
	 */
	@Test
	@Order(order = 8)
	public void escapeOnRetweetedModal() {

		modalEscape();
	}

	@Test
	@Order(order = 9)
	public void reopenRetweetedModal() {

		openModal(tweet
				.findElement(By.cssSelector("a.request-retweeted-popup")));

	}

	/**
	 * Wait for all of the "activity" content to load
	 */
	@Test
	@Order(order = 10)
	public void rewaitForModalContent() {

		waitForModalContent();

	}

	/**
	 * Keep tabbing until we either reach the close button or 8 seconds elapses
	 * greta - I removed the timeout because timeout was occurring too soon
	 */
	// @Test(timeout = 8000)
	@Test
	@Order(order = 11)
	public void retweetedTabUntilClose() {

		tabUntilModalClose();

	}

	@Test
	@Order(order = 12)
	public void retweetedReturnOnClose() {

		modalReturnOnClose();

	}

}
