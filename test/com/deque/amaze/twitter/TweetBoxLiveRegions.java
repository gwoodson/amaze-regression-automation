/**
 * 
 */
package com.deque.amaze.twitter;

import static org.junit.Assert.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class TweetBoxLiveRegions {

	private static Logger logger = Logger.getLogger(TweetBoxLiveRegions.class
			.getName());

	private static TwitterDriver driver = TwitterSuite.driver;

	private static final int MAX_TWEET_LENGTH = 140;
	private static final String MESSAGE = "Hello world!";

	private static WebElement newTweetButton;
	private static WebElement textbox;
	private static WebElement liveRegion;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();
	}

	@Test
	@Order(order = 1)
	public void findNewTweetButton() {

		newTweetButton = driver.findElement(By.id("global-new-tweet-button"));

		logger.log(
				Level.INFO,
				"located `new tweet button`: "
						+ PrettyPrint.element(newTweetButton));

	}

	@Test
	@Order(order = 2)
	public void findLiveRegion() {

		/*
		 * liveRegion = driver.findElement(By.id("amaze_polite"));
		 * 
		 * logger.log( Level.INFO, "located polite live region: " +
		 * PrettyPrint.element(liveRegion));
		 */
	}

	@Test
	@Order(order = 3)
	public void enterLaunchesModal() {

		// ENTER on the button
		newTweetButton.sendKeys(Keys.ENTER);

		// wait for the modal to open
		driver.waitForElementVisible(By.id("global-tweet-dialog"));

	}

	@Test
	@Order(order = 4)
	public void autoFocusPseudoTextbox() {

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "active:" + PrettyPrint.element(active));

		assertTrue(active.getAttribute("role").equals("textbox"));

		assertTrue(active.getAttribute("class").contains("rich-editor"));

		textbox = active;

	}

	@Test
	@Order(order = 5)
	public void typeIntoTextbox() throws InterruptedException {

		// clear the existing "tweet", if there is one
		textbox.clear();
		
		textbox.sendKeys(MESSAGE);

		// sleep 1 and a half seconds, allowing the timeout in
		// `amaze.utils.tweetBox` to elapse and time for the overlay to add the
		// announcement
		Thread.sleep(1500);

	}

	@Test
	@Order(order = 6)
	public void typingProducesPoliteAssertion() {

		String expectedMessage = String.format("%d characters left",
				(MAX_TWEET_LENGTH - MESSAGE.length()));

		logger.log(Level.INFO,
				"expecting an assertion which contains the text \""
						+ expectedMessage + "\".");

		liveRegion = driver.findElement(By.id("amaze_liveregions_polite"));

		logger.log(
				Level.INFO,
				"located polite live region: "
						+ PrettyPrint.element(liveRegion));

		// grab all announcements, as there may be more than one in the document
		// (new tweet loaded, etc.)
		List<WebElement> announcements = liveRegion.findElements(By
				.cssSelector("p"));

		logger.log(Level.INFO,
				String.format("Found %d assertions", announcements.size()));

		// iterate through the announcements, checking their text
		for (WebElement announcement : announcements) {

			String text = announcement.getText().trim();

			logger.log(Level.INFO,
					"assertion: " + PrettyPrint.element(announcement));

			logger.log(Level.INFO, String.format("assertion text: %s", text));

			// found the correct text?
			if (text.equals(expectedMessage)) {
				return;
			}
		}

		fail("Found no matching assertions");
	}

}
