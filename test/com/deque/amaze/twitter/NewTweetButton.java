/**
 * 
 */
package com.deque.amaze.twitter;

import static com.deque.junit.Assert.assertWebElement;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class NewTweetButton {

	private static Logger logger = Logger.getLogger(NewTweetButton.class
			.getName());

	private static TwitterDriver driver = TwitterSuite.driver;

	private static WebElement newTweetButton;
	private static WebElement modal;
	private static WebElement tweetBox;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();

	}

	@Test
	@Order(order = 1)
	public void buttonExists() {

		newTweetButton = driver.findElement(By
				.cssSelector("button#global-new-tweet-button"));

	}

	@Test
	@Order(order = 2)
	public void openNewTweetModal() {

		// click the button
		newTweetButton.click();

		// wait for the modal to be visible

		driver.waitForElementVisible(By
				.cssSelector("#global-tweet-dialog h3.modal-title"));

		modal = driver.findElement(By.id("global-tweet-dialog"));

	}

	@Test
	@Order(order = 3)
	public void tweetBoxIsFocused() {

		tweetBox = modal.findElement(By.id("tweet-box-global"));

		WebElement activeElement = driver.getActiveElement();

		assertWebElement(tweetBox, activeElement);

	}

	@Test
	@Order(order = 4)
	public void tabToImageSelector() throws InterruptedException {

		WebElement imageSelector = modal.findElement(By
				.cssSelector("button.btn"));

		tweetBox.sendKeys(Keys.TAB);
		
		Thread.sleep(3000L);

		WebElement activeElement = driver.getActiveElement();

		assertWebElement(imageSelector, activeElement);

	}

	@Test
	@Order(order = 5)
	public void tabToLocation() {

		WebElement imageSelector = modal.findElement(By
				.cssSelector("button.btn"));

		WebElement locationButton = modal.findElement(By
				.cssSelector("button.geo-picker-btn"));

		imageSelector.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();

		assertWebElement(locationButton, activeElement);

	}

	@Test
	@Order(order = 6)
	public void tabToClose() {

		WebElement locationButton = modal.findElement(By
				.cssSelector("button.geo-picker-btn"));

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		locationButton.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();

		logger.log(Level.INFO, String.format(
				"%s -> NewTweetButton tabToClose -> %s", close, activeElement));

		assertWebElement(close, activeElement);
	}

	@Test
	@Order(order = 7)
	public void tabToTweetBox() {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		close.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();

		assertWebElement(tweetBox, activeElement);

	}

	@Test
	@Order(order = 8)
	public void tabToTweetButton() {

		WebElement locationButton = modal.findElement(By
				.cssSelector("button.geo-picker-btn"));

		WebElement tweetButton = modal.findElement(By
				.cssSelector("button.tweet-action"));

		tweetBox.sendKeys("hello world");

		locationButton.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();

		assertWebElement(tweetButton, activeElement);

	}

	@Test
	@Order(order = 9)
	public void tabToCloseFromTweetButton() {

		WebElement tweetButton = modal.findElement(By
				.cssSelector("button.tweet-action"));

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		tweetButton.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();

		assertWebElement(close, activeElement);
	}

	@Test
	@Order(order = 10)
	public void closeOnEsc() {

		// tweetBox.sendKeys(Keys.ESCAPE);

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		close.click();

		// wait for the modal to hide
		driver.waitForElementHidden(By
				.cssSelector("#global-tweet-dialog h3.modal-title"));
		modal = driver.findElement(By.id("global-tweet-dialog"));

		String display = modal.getCssValue("display");
		assertEquals("none", display.toLowerCase());
		assertFalse(modal.isDisplayed());

	}

	@Test
	@Order(order = 11)
	public void resetFocusToButton() {

		WebElement activeElement = driver.getActiveElement();

		assertWebElement(newTweetButton, activeElement);

	}

	@Test
	@Order(order = 12)
	public void enterFiresClick() {

		// re-open the modal
		openNewTweetModal();

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		close.sendKeys(Keys.RETURN);

		driver.waitForElementHidden(By
				.cssSelector("#global-tweet-dialog h3.modal-title"));

	}

}
