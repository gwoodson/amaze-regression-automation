/**
 * 
 */
package com.deque.amaze.twitter;

import static org.junit.Assert.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * Verify the "Direct Messages" modal in multiple phases:
 * <ul>
 * <li>Navigating to the modal</li>
 * <li>Verify its title is automagically focused by Christmas elves</li>
 * <li>Verify tab index within the modal</li>
 * <li>Verify the focus is reset when the modal is closed</li>
 * </ul>
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class DirectMessages {

	private static TwitterDriver driver = TwitterSuite.driver;

	private static Logger logger = Logger.getLogger(DirectMessages.class
			.getName());

	private static WebElement modal;
	private static WebElement dropDownToggle;
	private static WebElement dropDownMenu;
	private static WebElement directMessageToggle;

	/**
	 * Navigate to the homepage, inject Amaze then wait until the page has
	 * finished loading
	 * 
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();

	}

	/**
	 * Open the user dropdown menu
	 * 
	 * @throws InterruptedException
	 *             When we're not able to sleep
	 */
	@Test
	@Order(order = 1)
	public void openDropdown() throws InterruptedException {

		dropDownToggle = driver.findElement(By.id("user-dropdown-toggle"));

		// selenium intermittently fails here; sleeping seems to solve all
		// selenium problems
		Thread.sleep(250);

		logger.log(Level.INFO,
				"Drop down toggle: " + PrettyPrint.element(dropDownToggle));

		dropDownToggle.sendKeys(Keys.RETURN);

	}

	/**
	 * Wait for the dropdown menu to become present/visible
	 */
	@Test
	@Order(order = 2)
	public void waitForDropDown() {

		dropDownMenu = driver.waitForElementVisible(By
				.cssSelector("#user-dropdown ul.dropdown-menu"));

	}

	/**
	 * Tab through the menu until one of the following occurs:
	 * <ul>
	 * <li>1 second elaspses</li>
	 * <li>We reach the "direct messages" link</li>
	 * </ul>
	 */
	@Test(timeout = 1000)
	@Order(order = 3)
	public void tabToDriectMessages() {

		directMessageToggle = dropDownMenu.findElement(By
				.cssSelector("li.messages a.js-dm-dialog"));
		int counter = 0;

		while (!driver.getActiveElement().equals(directMessageToggle)) {
			logger.log(Level.INFO, String.format("Tab #%d", counter));

			driver.getActiveElement().sendKeys(Keys.TAB);

			logger.log(
					Level.INFO,
					"Currently active element: "
							+ PrettyPrint.element(driver.getActiveElement()));

			counter += 1;
		}

	}

	/**
	 * Open the "direct messages" modal
	 * 
	 * @throws InterruptedException
	 */
	@Test
	@Order(order = 4)
	public void openModal() throws InterruptedException {

		directMessageToggle.sendKeys(Keys.RETURN);

		// wait for the modal's title to appear
		driver.waitForElementVisible(By
				.cssSelector("#dm_dialog .modal-header h3"));

		// sleep, letting the overlay run
		Thread.sleep(1000);

		modal = driver.findElement(By.id("dm_dialog"));
	}

	/**
	 * Verify the title has been automagically focused by Christmas elves
	 */
	@Test
	@Order(order = 5)
	public void autoFocusTitle() {

		WebElement title = modal
				.findElement(By.cssSelector(".modal-header h3"));

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active element: " + PrettyPrint.element(active));
		assertEquals(title, active);
	}

	/**
	 * Verify tab order: title -> new message
	 */
	@Test
	@Order(order = 6)
	public void tabFromTitleToNewMessage() {

		WebElement newMessage = modal.findElement(By
				.cssSelector("#dm_dialog .dm-new-button"));

		WebElement title = modal
				.findElement(By.cssSelector(".modal-header h3"));

		title.sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active element: " + PrettyPrint.element(active));

		assertEquals(newMessage, active);
	}

	/**
	 * Tab from the "new message" button until we reach the "learn more" link.
	 * We're not setting a timeout here, as there as a variable number of
	 * elements in tab index between the two.
	 */
	@Test
	@Order(order = 7)
	public void tabUntilLearnMore() {

		WebElement learnMore = modal.findElement(By
				.cssSelector(".modal-content a.learn-more"));
		int counter = 0;

		while (!driver.getActiveElement().equals(learnMore)) {
			logger.log(Level.INFO, String.format("Tab #%d", counter));

			driver.getActiveElement().sendKeys(Keys.TAB);

			logger.log(
					Level.INFO,
					"Currently active element: "
							+ PrettyPrint.element(driver.getActiveElement()));

			counter += 1;
		}

		WebElement active = driver.getActiveElement();

		assertEquals(learnMore, active);
	}

	/**
	 * Verify the close button makes sense to keyboard users. We've added some
	 * offscreen text to it, as well as set its tabIndex
	 */
	@Test
	@Order(order = 8)
	public void closeButton() {

		WebElement close = modal.findElement(By
				.cssSelector("div.twttr-dialog-close"));

		logger.log(Level.INFO, "Close button: " + PrettyPrint.element(close));

		String tabIndex = close.getAttribute("tabindex");

		assertEquals("-1", tabIndex);

		// this will throw if the offscreen DIV isn't there
		close.findElement(By.cssSelector("div.amaze-offscreen"));
	}

	/**
	 * Verify tab order: learn more -> close button
	 * This test case is no longer valid
	 */
	@Ignore
	@Test
	@Order(order = 9)
	public void tabFromLearnMoreToClose() {

		WebElement close = modal.findElement(By
				.cssSelector("div.twttr-dialog-close"));

		WebElement learnMore = modal.findElement(By
				.cssSelector(".modal-content a.learn-more"));

		learnMore.sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO,
				"Currently active in tabFromLearnMoreToClose: " + PrettyPrint.element(active));

		assertEquals(close, active);

	}

	/**
	 * Verify tab order: close -> title
	 * This test case is no longer valid
	 */
	@Ignore
	@Test
	@Order(order = 10)
	public void tabFromCloseToTitle() {

		WebElement close = modal.findElement(By
				.cssSelector("div.twttr-dialog-close"));

		WebElement title = modal
				.findElement(By.cssSelector(".modal-header h3"));

		close.sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO,
				"Currently active: " + PrettyPrint.element(active));

		assertEquals(title, active);

	}

	/**
	 * Verify the modal closes when RETURN is pressed on the close button
	 */
	@Test
	@Order(order = 11)
	public void returnOnClose() {

		WebElement close = modal.findElement(By
				.cssSelector("div.twttr-dialog-close"));

		close.sendKeys(Keys.RETURN);

		// will throw if the modal never hides
		driver.waitForElementHidden(By
				.cssSelector("#dm_dialog .modal-header h3"));
	}

	/**
	 * Verify focus is reset properly
	 */
	@Test
	@Order(order = 12)
	public void resetFocus() {

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO,
				"Currently active: " + PrettyPrint.element(active));

		assertEquals(dropDownToggle, active);

	}

}
