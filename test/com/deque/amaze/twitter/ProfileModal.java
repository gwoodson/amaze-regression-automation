/**
 * 
 */
package com.deque.amaze.twitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static com.deque.junit.Assert.assertWebElement;

import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class ProfileModal {

	private static Logger logger = Logger.getLogger(UserDropDown.class
			.getName());

	private static TwitterDriver driver = TwitterSuite.driver;

	private static By modalSelector = By.id("profile_popup");

	/**
	 * The list of profile links
	 */
	private static List<WebElement> profileLinks;
	/**
	 * The profile link we opened
	 */
	private static WebElement profileLink;
	/**
	 * The profile modal
	 */
	private static WebElement modal;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");

		driver.injectAmaze();

		driver.waitForPageContainer();
	}

	@Test
	@Order(order = 1)
	public void profileLinksExist() {

		profileLinks = driver
				.findElements(By
						.cssSelector("div.tweet .js-action-profile.js-user-profile-link"));
	}

	/**
	 * Grab a random profile link out of the list and click it
	 */
	@Test
	@Order(order = 2)
	public void openProfileModal() {

		Random random = new Random();
		int index = random.nextInt(profileLinks.size());

		profileLink = profileLinks.get(index);

		logger.log(Level.INFO, String.format("Choose profile link #%d (%s)",
				index, PrettyPrint.element(profileLink)));

		profileLink.sendKeys(Keys.RETURN);

		driver.waitForElementVisible(By
				.cssSelector("#profile_popup .modal-title"));

		modal = driver.findElement(modalSelector);
	}

	@Test
	@Order(order = 3)
	public void wasteSomeTime() throws InterruptedException {

		Thread.sleep(1000);
	}

	@Test
	@Order(order = 4)
	public void autoFocusTitle() {

		WebElement title = modal.findElement(By.className("modal-title"));
		assertEquals("-1", title.getAttribute("tabindex"));

		WebElement active = driver.getActiveElement();

		assertWebElement(title, active);

	}

	/**
	 * Find the last anchor contained in the modal and send an ESCAPE key event
	 * to it
	 */
	@Test
	@Order(order = 5)
	public void escapeClosesModal() {

		WebElement someInnerElement = modal
				.findElement(By.xpath("//a[last()]"));

		logger.log(Level.INFO,
				"some anchor: " + PrettyPrint.element(someInnerElement));

		someInnerElement.sendKeys(Keys.ESCAPE);

		driver.waitForElementHidden(By
				.cssSelector("#profile_popup .modal-title"));

		modal = driver.findElement(modalSelector);

		assertFalse(modal.isDisplayed());

	}

	/**
	 * This is working when tested manually, but for whatever reason, within
	 * this test, the activeElement is #amaze-skip-to-tweets-link rather than
	 * what we're expecting. I cannot explain this...
	 * Greta - I CAN EXPLAIN...  when user tabs to profilelink of tweeter focus is returned 
	 * to profile link.  In this regression the profile link is launched automagically
	 * on initial open of landing page...  and skip link the first stop on the initial open
	 */
	@Test
	@Order(order = 6)
	public void returnFocusToProfileLink() throws InterruptedException {

		// i'm tryin' everything ...
		Thread.sleep(1000);

		WebElement active = driver.getActiveElement();

		String actual = PrettyPrint.element(active);
		String expected = PrettyPrint.element(profileLink);

		assertEquals(expected, actual);

	}

	@Test
	@Order(order = 7)
	public void reopenModal() {

		profileLink.sendKeys(Keys.RETURN);

		driver.waitForElementVisible(By
				.cssSelector("#profile_popup .modal-title"));

		modal = driver.findElement(modalSelector);

	}

	/**
	 * Keep tabbing until we either reach the close button or 8 seconds elapses
	 * @throws InterruptedException 
	 */
	@Ignore
	@Test(timeout = 8000)
	@Order(order = 8)
	public void tabUntilClose() throws InterruptedException {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close.js-close"));
		int counter = 0;

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO,
				"Currently active element: " + PrettyPrint.element(active));

		// keep sending a TAB to the active element until we reach the "close"
		// button
		while (!driver.getActiveElement().equals(close)) {

			logger.log(Level.INFO, String.format("Tab #%d", counter));
			
			Thread.sleep(5000L);

			driver.getActiveElement().sendKeys(Keys.TAB);

			WebElement active2 = driver.getActiveElement();
			logger.log(Level.INFO,
					"Currently active element: " + PrettyPrint.element(active2));

			counter += 1;
		}

		assertWebElement(close, driver.getActiveElement());

	}

	@Test
	@Order(order = 9)
	public void returnOnClose() {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close.js-close"));

		close.sendKeys(Keys.RETURN);

		driver.waitForElementHidden(By
				.cssSelector("#profile_popup .modal-title"));

		modal = driver.findElement(modalSelector);

	}

	@Test
	@Order(order = 10)
	public void returnFocus() {

		WebElement activeElement = driver.getActiveElement();

		logger.log(
				Level.INFO,
				"Currently active element: "
						+ PrettyPrint.element(activeElement));

		assertWebElement(profileLink, activeElement);

	}

	/**
	 * Ignoring because I don't know what this is doing, nor do I know why it's
	 * here. Furthermore, it's sleeping for an unreasonable amount of time and
	 * slowing down the suite.
	 * 
	 * @throws InterruptedException
	 */
	@Test
	@Order(order = 11)
	public void openAccountProfile() throws InterruptedException {

		Thread.sleep(2000L);

		WebElement accountlink = driver.findElement(By
				.xpath("//*[@id='page-container']/div[1]/div[1]/div[1]/a"));

		accountlink.sendKeys(Keys.ENTER);

		Thread.sleep(2000L);

		driver.waitForElementVisible(By.xpath("//*[@id='doc']"));

		WebElement userprofile = driver.findElement(By.xpath("/html/body"));

		userprofile.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();

		WebElement expected = driver.findElement(By
				.xpath("//*[@id='amaze-skip-to-tweets-link']"));

		assertEquals(expected, activeElement);

		logger.log(
				Level.INFO,
				"Currently active element: "
						+ PrettyPrint.element(activeElement)
						+ " Expected element:  " + expected);

	}

}
