/**
 * 
 */
package com.deque.amaze.twitter;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class ShortcutsModal {

	private static TwitterDriver driver = TwitterSuite.driver;

	private static Logger logger = Logger.getLogger(ShortcutsModal.class
			.getName());

	private static WebElement modal;
	private static WebElement dropDownToggle;
	private static WebElement dropDownMenu;
	private static WebElement keyboardShortcutsModal;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();

	}

	@Test
	public void openDropdown() {

		dropDownToggle = driver.findElement(By.id("user-dropdown-toggle"));

		logger.log(Level.INFO,
				"Drop down toggle: " + PrettyPrint.element(dropDownToggle));

		dropDownToggle.sendKeys(Keys.RETURN);

	}

	@Test
	public void waitForDropDown() {

		dropDownMenu = driver.waitForElementVisible(By
				.cssSelector("#user-dropdown ul.dropdown-menu"));

	}

	@Test(timeout = 1000)
	public void tabToKeyboardShortcuts() {

		keyboardShortcutsModal = dropDownMenu.findElement(By
				.cssSelector("li .js-keyboard-shortcut-trigger"));
		int counter = 0;

		while (!driver.getActiveElement().equals(keyboardShortcutsModal)) {
			logger.log(Level.INFO, String.format("Tab #%d", counter));

			driver.getActiveElement().sendKeys(Keys.TAB);

			logger.log(
					Level.INFO,
					"Currently active element: "
							+ PrettyPrint.element(driver.getActiveElement()));

			counter += 1;
		}

	}

	@Test
	public void openModal() throws InterruptedException {

		keyboardShortcutsModal.sendKeys(Keys.RETURN);

		// wait for the modal's title to appear
		driver.waitForElementVisible(By
				.cssSelector("#keyboard-shortcut-dialog .modal-header h3.modal-title"));

		// sleep, letting the overlay run
		Thread.sleep(1000);

		modal = driver.findElement(By.id("keyboard-shortcut-dialog"));

	}

	@Test
	public void autoFocusTitle() {

		WebElement title = modal.findElement(By
				.cssSelector(".modal-header h3.modal-title"));

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active element: " + PrettyPrint.element(active));
		assertEquals(title, active);

	}

	@Test
	public void tabFromTitleToActions() {

		WebElement table = modal.findElement(By
				.xpath("//*[@id=\"keyboard-shortcut-menu\"]/table[1]"));

		WebElement heading = table.findElement(By.tagName("th"));

		String text = heading.getText().trim().toLowerCase();

		assertEquals("actions", text);

		driver.getActiveElement().sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active element: " + PrettyPrint.element(active));
		assertEquals(heading, active);

	}

	@Test
	public void tabFromActionsToNavigation() {

		WebElement table = modal.findElement(By
				.xpath("//*[@id=\"keyboard-shortcut-menu\"]/table[2]"));

		WebElement heading = table.findElement(By.tagName("th"));

		String text = heading.getText().trim().toLowerCase();

		assertEquals("navigation", text);

		driver.getActiveElement().sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active element: " + PrettyPrint.element(active));
		assertEquals(heading, active);

	}

	@Test
	public void tabFromNavigationToTimelines() {

		WebElement table = modal.findElement(By
				.xpath("//*[@id=\"keyboard-shortcut-menu\"]/table[3]"));

		WebElement heading = table.findElement(By.tagName("th"));

		String text = heading.getText().trim().toLowerCase();

		assertEquals("timelines", text);

		driver.getActiveElement().sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active element: " + PrettyPrint.element(active));
		assertEquals(heading, active);

	}

	@Test
	public void tabFromTimelinesToClose() {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));
		driver.getActiveElement().sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO, "Active element: " + PrettyPrint.element(active));
		assertEquals(close, active);

	}

	@Test
	public void returnOnClose() {

		WebElement close = modal.findElement(By
				.cssSelector("button.modal-close"));

		close.sendKeys(Keys.RETURN);

		// throws if it doesn't hide
		driver.waitForElementHidden(By
				.cssSelector("#keyboard-shortcut-dialog .modal-header h3.modal-title"));

	}

	@Test
	public void resetFocus() {

		WebElement active = driver.getActiveElement();

		logger.log(Level.INFO,
				"Currently active: " + PrettyPrint.element(active));

		assertEquals(dropDownToggle, active);
	}
}
