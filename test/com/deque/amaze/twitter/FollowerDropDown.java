package com.deque.amaze.twitter;

import static org.junit.Assert.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class FollowerDropDown {
	
	private static Logger logger = Logger.getLogger(FollowerDropDown.class
			.getName());

	private static TwitterDriver driver = TwitterSuite.driver;
	
	private static List<WebElement> followers;
	private static WebElement followerslink;
	private static WebElement summarylink;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void findSummaryLink() {
		
		summarylink = driver.findElement(By
				.cssSelector("a.account-summary.account-summary-small.js-nav"));
		
		logger.log(
				Level.INFO,
				String.format("Found summary (profile) link : %s",
						PrettyPrint.element(summarylink)));
	}
	
	@Test
	@Order(order = 2)
	public void clickSummaryLink() {
		
		summarylink.sendKeys(Keys.ENTER);
		
		logger.log(
				Level.INFO,
				String.format("Send key RETURN from follower'summary link : %s",
						PrettyPrint.element(summarylink)));
		
	}
	
	@Test
	@Order(order = 4)
	public void findFollowersLink() throws InterruptedException {
		
		Thread.sleep(5000L);
		
		driver.waitForElementVisible(By.cssSelector("a.list-link.js-nav"));
		
		followerslink = driver.findElement(By
				.cssSelector("a.list-link.js-nav").partialLinkText("Followers"));
		
		logger.log(
				Level.INFO,
				String.format("Found Follower's link : %s",
						PrettyPrint.element(followerslink)));
		
	}
	
	@Test
	@Order(order = 5)
	public void clickFollowersLink() {
		
		followerslink.sendKeys(Keys.ENTER);
		
		logger.log(
				Level.INFO,
				String.format("Send key RETURN from follower's link : %s",
						PrettyPrint.element(followerslink)));
		
	}
	
	@Test
	@Order(order = 6)
	public void findFollowers() {
		
		followers = driver.findElements(By.cssSelector("li.js-stream-item.stream-item.stream-item"));
		
		logger.log(
				Level.INFO,
				String.format("Number of followers : %s",
						followers.size()));
		
	}
	
	@Ignore
	@Test
	@Order(order = 7)
	public void findFollowerDropdowns() {
		
		WebElement streamitem = followers.get(1).findElement(By.cssSelector("div.user-actions.btn-group.following.can-dm.including  "));
		
		logger.log(
				Level.INFO,
				String.format("Found Stream Item 1: %s",
						PrettyPrint.element(streamitem)));
		
	}



}
