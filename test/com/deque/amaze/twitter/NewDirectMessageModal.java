/**
 * 
 */
package com.deque.amaze.twitter;

import static com.deque.junit.Assert.*;
import static org.junit.Assert.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * Verify both the direct messages modal and its inner-modal, the new direct
 * message modal
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class NewDirectMessageModal {

	private static TwitterDriver driver = TwitterSuite.driver;

	private static Logger logger = Logger.getLogger(NewDirectMessageModal.class
			.getName());

	private static WebElement dropdown;
	private static WebElement directMessagesModal;
	private static WebElement newDirectMessageModal;
	private static WebElement profileLink;
	private static WebElement followerLink;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	/**
	 * Tab until something gains focus.
	 * 
	 * @param element
	 *            The element we want to gain focus
	 * @param maxTabs
	 *            The maximum number of tabs
	 */
	private static void tabUntil(WebElement element, int maxTabs) {

		int count = 0;

		while (!driver.getActiveElement().equals(element)) {

			if (count > maxTabs) {
				fail(String.format("Tabbed too many times: %d", count));
			}

			WebElement active = driver.getActiveElement();
			logger.log(Level.INFO, String.format(
					"Currently active (tab #%d): %s", count,
					PrettyPrint.element(active)));
			active.sendKeys(Keys.TAB);
			count++;
		}

		assertWebElement(driver.getActiveElement(), element);

	}

	/**
	 * Keep tabbing until we've reached the dropdown toggle button
	 * @throws InterruptedException 
	 */
	@Test
	@Order(order = 1)
	public void testTabToUserDropdownToggle() throws InterruptedException {
		
		Thread.sleep(3000L);		

		WebElement toggle = driver.findElement(By.id("user-dropdown-toggle"));

		tabUntil(toggle, 20);

		assertWebElement("Active element should be the dropdown toggle",
				driver.getActiveElement(), toggle);
	}

	/**
	 * Verify that pressing RETURN on the toggle will open the dropdown menu
	 */
	@Test
	@Order(order = 2)
	public void testReturnOpensDropdown() {

		WebElement toggle = driver.getActiveElement();
		toggle.sendKeys(Keys.RETURN);

		dropdown = driver.waitForElementVisible(By
				.cssSelector(".global-nav .container ul.dropdown-menu"));
	}

	/**
	 * Verify we can tab to the direct messages link within the dropdown menu
	 */
	@Test
	@Order(order = 3)
	public void testTabToDirectMessagesLink() {

		WebElement link = dropdown.findElement(By
				.cssSelector("a.js-dm-dialog"));
		
		/*WebElement link = dropdown.findElement(By
				.cssSelector("li.messages a.js-dm-dialog"));*/

		tabUntil(link, 5);

		assertWebElement("Active element should be the messages link",
				driver.getActiveElement(), link);

	}

	/**
	 * Verify pressing RETURN on the link opens the modal
	 * @throws InterruptedException 
	 */
	@Test
	@Order(order = 3)
	public void testReturnOpensDirectMessagesModal() throws InterruptedException {

		WebElement link = driver.getActiveElement();
		//link.sendKeys(Keys.RETURN);
		link.sendKeys(Keys.ENTER);
		
		
		Thread.sleep(5000L);

		directMessagesModal = driver.waitForElementVisible(By
				.cssSelector("#dm_dialog"));
	}

	/**
	 * Verify the modal's title is automatically put into focus
	 * 
	 * @throws InterruptedException
	 */
	@Test
	@Order(order = 4)
	public void testModalTitleGainsFocus() throws InterruptedException {

		// let the JS fire
		Thread.sleep(400);

		WebElement expected = directMessagesModal.findElement(By
				.cssSelector(".modal-header h3"));
		WebElement actual = driver.getActiveElement();

		assertWebElement("The title should be focued", actual, expected);

	}

	/**
	 * Verify the DM modal doesn't allow people to tab out of it
	 * @throws InterruptedException 
	 */
	@Test
	@Order(order = 5)
	public void testModalKeepsFocusWhenTabbing() throws InterruptedException {

		Thread.sleep(3000L);
		
		WebElement start = driver.getActiveElement();
		
		start.sendKeys(Keys.TAB);

		tabUntil(start, 100);

		assertWebElement(start, driver.getActiveElement());

	}

	/**
	 * Verify we can tab to the new message button
	 */
	@Test
	@Order(order = 6)
	public void testTabToNewMessage() {

		WebElement newMessage = directMessagesModal.findElement(By
				.cssSelector(".dm-new-button"));
		tabUntil(newMessage, 100);

	}

	/**
	 * Verify pressing RETURN on the new message button opens the "nested" new
	 * direct message modal
	 */
	@Test
	@Order(order = 7)
	public void testReturnOnNewMessageButtonOpensNewModal() {

		WebElement newMessage = directMessagesModal.findElement(By
				.cssSelector(".dm-new-button"));

		assertWebElement("new message button should have focus",
				driver.getActiveElement(), newMessage);

		newMessage.sendKeys(Keys.RETURN);

		newDirectMessageModal = driver.waitForElementVisible(By
				.cssSelector("#dm_dialog_new"));
	}

	/**
	 * Verify ARIA labels have been added to:
	 * <ul>
	 * <li>the recipient input</li>
	 * <li>the close button</li>
	 * <li>the message content input</li>
	 * </ul>
	 */
	@Test
	@Order(order = 8)
	public void testAriaLabelsAdded() {

		// selector.query('.twttr-dialog-close', messageModal),

		WebElement close = newDirectMessageModal.findElement(By
				.cssSelector(".twttr-dialog-close"));
		WebElement recipient = newDirectMessageModal.findElement(By
				.cssSelector(".dm-to-input.twttr-directmessage-input"));
		WebElement content = newDirectMessageModal.findElement(By
				.cssSelector("#tweet-box-dm-new-conversation"));

		assertFalse(close.getAttribute("aria-label").isEmpty());
		assertFalse(recipient.getAttribute("aria-label").isEmpty());
		assertFalse(content.getAttribute("aria-label").isEmpty());

	}

	/**
	 * Verify when the new direct message modal is opened, focus is put on the
	 * first input (the "to whom/recipient" sort of thingy)
	 */
	@Test
	@Order(order = 9)
	public void testMessageRecipientGainsFocus() {

		WebElement expected = newDirectMessageModal.findElement(By
				.cssSelector(".dm-to-input.twttr-directmessage-input"));

		assertWebElement(driver.getActiveElement(), expected);

	}

	

	/**
	 * Verify clicking the "Direct Messages" link puts focus back in the direct
	 * messages modal's title
	 * 
	 * @throws InterruptedException
	 */
	@Test
	@Order(order = 11)
	public void testDirectMessageButtonPutsFocusOnFirstModalTitle()
			throws InterruptedException {

		WebElement link = newDirectMessageModal.findElement(By
				.cssSelector(".modal-header h3 a"));

		link.sendKeys(Keys.RETURN);

		// let JS fire
		Thread.sleep(500);

		WebElement expected = directMessagesModal.findElement(By
				.cssSelector(".modal-header h3"));

		assertWebElement(expected, driver.getActiveElement());

	}

	/**
	 * Verify pressing ESCAPE closes the modal
	 */
	@Test
	@Order(order = 12)
	public void testEscapeClosesModal() {

		driver.getActiveElement().sendKeys(Keys.ESCAPE);

		driver.waitForElementHidden(By.cssSelector("#dm_dialog"));

	}

	/**
	 * Verify focus has been put on the dropdown toggle
	 * 
	 * @throws InterruptedException
	 */
	@Test
	@Order(order = 13)
	public void testFocusSetOnDropdownToggle() throws InterruptedException {

		// let JS fire
		Thread.sleep(200);

		WebElement expected = driver.findElement(By.id("user-dropdown-toggle"));

		assertWebElement(driver.getActiveElement(), expected);

	}
}
