package com.deque.amaze.twitter;

import static com.deque.junit.Assert.pass;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.VerifyModal;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.amaze.regression.twitter.UseableTweet;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * Verify a "retweeted" modal via
 * {@link com.deque.amaze.regression.twitter.RetweetsFavoriesModal}
 * 
 * @author Greta Woodson <greta.woodson@deque.com>
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class RetweetModal {

	private static Logger logger = Logger.getLogger(RetweetModal.class
			.getName());

	private static TwitterDriver driver = TwitterSuite.driver;

	private static WebElement tweet;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");

		driver.injectAmaze();

	}

	@Test
	@Order(order = 1)
	public void findRetweetedTweet() throws Exception {

		tweet = UseableTweet.retweeted(driver);

		logger.log(
				Level.INFO,
				String.format("Found retweeted tweet: %s",
						PrettyPrint.element(tweet)));
	}

	@Test
	@Order(order = 2)
	public void expandTweetIfNecessary() {

		String className = tweet.getAttribute("class");

		className = " " + className + " ";

		if (className.contains(" opened-tweet ")) {
			pass("Tweet already expanded");
			return;
		}

		WebElement expandLink = tweet.findElement(By
				.cssSelector("a.details.with-icn.js-details"));

		expandLink.sendKeys(Keys.RETURN);

	}

	@Test
	@Order(order = 3)
	public void verifyModal() throws Exception {

		WebElement trigger = tweet.findElement(By
				.cssSelector("a.request-retweeted-popup"));

		By modalSelector = By.cssSelector("#activity-popup-dialog");
		By closeSelector = By
				.cssSelector("button.modal-btn.modal-close.js-close");

		VerifyModal.verify(driver, trigger, modalSelector,
				closeSelector, 200);

	}

}
