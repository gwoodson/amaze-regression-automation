package com.deque.amaze.twitter;

import static com.deque.junit.Assert.pass;
import static org.junit.Assert.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
//import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.VerifyModal;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.amaze.regression.twitter.UseableTweet;

import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * Verify a "Photo/Media" tweet in multiple steps: Open a tweet which contains 
 * a media container this implies an image is present
 * Tab through expanded photo
 * 
 * 
 * @author greta woodson
 */
@RunWith(OrderedRunner.class)
public class MediaTweet {

	private static Logger logger = Logger.getLogger(MediaTweet.class
			.getName());

	private static String tweetType = "i.sm-image";   
	
	private static TwitterDriver driver = TwitterSuite.driver;
	private VerifyModal iframe = new VerifyModal();
	
	private static WebElement tweet;
	private static String author;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/*
	 * Find a tweet containing a photo
	 * print Tweeter's name
	 */
	@Test
	@Order(order = 1)
	public void findPhotoTweet() throws Exception {
		
		tweet = UseableTweet.getTweetByCSSName(driver, tweetType);
		
		String tweetText = tweet.getText();
		
		System.out.println(tweetText);
		
		if( tweet.equals(null) ) {
			
			logger.log(
					Level.INFO, 
					String.format("Did not find tweet with media container. Tweet: %s", tweet));
			
		} else {
			
			//get/print Tweeter's name
			author = tweet.getAttribute("data-name").trim();

			logger.log(
					Level.INFO, 
					String.format("Found tweet with media container. Tweet author: %s", author));
			
		}
		
		
		
	}

	/*
	 * Pre-Condition:  Tweet verified as a photo tweet.
	 * Test Scenario:  Expand Photo tweet.  Photo tweets are not displayed in a modal
	 */
	@Test
	@Order(order = 2)
	public void expandTweetIfNecessary() {
		
		if( tweet.equals(null) ) {
			
			logger.log(
					Level.INFO, 
					String.format("Did not find tweet with media container. Tweet: %s", tweet));
			
		} else {

			String className = tweet.getAttribute("class");
	
			className = " " + className + " ";
	
			if (className.contains(" opened-tweet ")) {
				pass("Tweet already expanded");
				return;
			}
	
			WebElement expandLink = tweet.findElement(By
					.cssSelector("a.details.with-icn.js-details"));
	
			expandLink.sendKeys(Keys.RETURN);
			
			logger.log(
					Level.INFO, 
					String.format("Tweet: %s", tweet));
		
		}

	}

	/*
	 * Pre-Condition:  	
	 * 		- Tweet verified as a photo tweet.
	 * 		- Tweet is expanded
	 * 					
	 * Test Scenario:  Verify photo/media container exists
	 */
	@Test
	@Order(order = 3)
	public void verifyModal() throws Exception {

		WebElement trigger = tweet.findElement(By
				.cssSelector("a.twitter-timeline-link.media-thumbnail"));

		By modalSelector = By.cssSelector("div.swift-media-gallery.no-grid");
		By closeSelector = By
				.cssSelector("button.modal-btn");

		iframe.verifyPhoto(driver, trigger, modalSelector,
				closeSelector, 200);

	}

	
}
