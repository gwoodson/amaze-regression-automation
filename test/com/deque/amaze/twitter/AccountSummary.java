package com.deque.amaze.twitter;

import static org.junit.Assert.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class AccountSummary {
	
	private static Logger logger = Logger.getLogger(AccountSummary.class
			.getName());

	private static TwitterDriver driver = TwitterSuite.driver;
	private static WebElement summarylink;
	private static WebElement photoGallery;
	private static List<WebElement> photoList;
	private static List<WebElement> videoList;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void findSummaryLink() {
		
		summarylink = driver.findElement(By
				.cssSelector("a.account-summary.account-summary-small.js-nav"));
		
		logger.log(
				Level.INFO,
				String.format("Found summary (profile) link : %s",
						PrettyPrint.element(summarylink)));
	}
	
	@Test
	@Order(order = 2)
	public void clickSummaryLink() throws InterruptedException {
		
		summarylink.sendKeys(Keys.ENTER);
		
		Thread.sleep(5000L);
		
		WebElement navHome = driver.findElement(By
				.cssSelector("a.js-nav"));
		
		navHome.sendKeys(Keys.TAB);
				
	}
	
	@Test
	@Order(order = 3)
	public void findMediaGallery() throws InterruptedException {
		
		Thread.sleep(5000L);
		
		photoGallery = driver.findElement(By
				.cssSelector("div.media-row-content.photo-list"));
		
		logger.log(
				Level.INFO,
				String.format("Found Photos : %s",
						PrettyPrint.element(photoGallery)));		
				
	}
	
	@Test
	@Order(order = 4)
	public void findPhotos() throws InterruptedException {
		
		Thread.sleep(3000L);
		
		photoList = photoGallery.findElements(By.cssSelector("a.media-thumbnail.js-nav"));
		
		int numPhotos = photoList.size();
				
		logger.log(
				Level.INFO,
				String.format("Photos found: %s",
						numPhotos));
				
	}
	
	@Test
	@Order(order = 5)
	public void findVideos() throws InterruptedException {
		
		Thread.sleep(3000L);
		
		videoList = photoGallery.findElements(By.cssSelector("a.media-thumbnail.video.js-nav"));
		
		int numVideos = videoList.size();
				
		logger.log(
				Level.INFO,
				String.format("Videos found: %s",
						numVideos));
		
	}
				
	@Test
	@Order(order = 6)
	public void viewVideos() throws InterruptedException {
			
		Thread.sleep(3000L);
			
		WebElement viewAllLink = driver.findElement(By.cssSelector("a.list-link.last.js-nav.grid-link"));
			
		logger.log(
				Level.INFO,
				String.format("Found View All photos and videos link: %s",
						PrettyPrint.element(viewAllLink)));
			
		viewAllLink.sendKeys(Keys.ENTER);
					
	}

}
