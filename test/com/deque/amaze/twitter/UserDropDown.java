/**
 * 
 */
package com.deque.amaze.twitter;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class UserDropDown {

	private static Logger logger = Logger.getLogger(UserDropDown.class
			.getName());

	private static TwitterDriver driver = TwitterSuite.driver;

	private static WebElement dropDownToggle;
	private static WebElement dropDownMenu;
	private static By dropDownMenuSelector = By
			.cssSelector("#user-dropdown ul.dropdown-menu");

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");

		driver.injectAmaze();

		driver.waitForPageContainer();
	}

	@Test
	@Order(order = 1)
	public void openMenu() {

		dropDownToggle = driver.findElement(By.id("user-dropdown-toggle"));

		dropDownToggle.sendKeys(Keys.RETURN);

		dropDownMenu = driver.waitForElementVisible(dropDownMenuSelector);

	}

	/**
	 * Verify the profile link is automagically focused when the menu pops up
	 */
	@Test
	@Order(order = 2)
	public void focusProfileLink() {

		WebElement active = driver.getActiveElement();

		WebElement profile = dropDownMenu.findElement(By
				.cssSelector("a.account-summary"));

		assertEquals(profile, active);

	}

	@Test
	@Order(order = 3)
	public void tabToMessages() {

		WebElement profile = driver.getActiveElement();
		WebElement directMessages = dropDownMenu.findElement(By
				.cssSelector("li.messages a.js-dm-dialog"));

		profile.sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();
		assertEquals(directMessages, active);

	}

	@Test
	@Order(order = 4)
	public void tabToSignOut() {

		// signout-button

		WebElement signout = driver.findElement(By.id("signout-button"));

		while (!driver.getActiveElement().equals(signout)) {
			logger.log(Level.INFO, "Tabbing again...");
			driver.getActiveElement().sendKeys(Keys.TAB);
		}

	}

	@Test
	@Order(order = 5)
	public void tabToNewTweetButton() {

		// global-new-tweet-button

		WebElement newTweetButton = driver.findElement(By
				.id("global-new-tweet-button"));

		WebElement signout = driver.getActiveElement();

		signout.sendKeys(Keys.TAB);

		WebElement active = driver.getActiveElement();

		assertEquals(newTweetButton, active);
	}

	@Test
	@Order(order = 6)
	public void closeMenuOnTabOut() {

		dropDownMenu = driver.waitForElementHidden(dropDownMenuSelector);

	}

	@Test
	@Order(order = 7)
	public void escapeClosesMenu() {

		// open the menu
		dropDownToggle.sendKeys(Keys.RETURN);
		
		dropDownMenu = driver.waitForElementVisible(dropDownMenuSelector);

		WebElement signout = driver.getActiveElement();

		signout.sendKeys(Keys.ESCAPE);
		
		driver.waitForElementHidden(dropDownMenuSelector);
	}
}
