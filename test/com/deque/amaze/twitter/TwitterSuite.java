package com.deque.amaze.twitter;

import static org.junit.Assert.fail;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.deque.amaze.regression.twitter.TwitterDriver;

/**
 * Suite runner. Uses {@link com.deque.amaze.regression.twitter.TwitterDriver}
 * to interact with Selenium.
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(Suite.class)
@SuiteClasses({
	Discover.class, 
	ExpandedTweet.class,
	NewTweetButton.class, 
	NewDirectMessageModal.class,
	UserDropDown.class, 
	ProfileModal.class,
	TweetBoxLiveRegions.class, 
	FavoriteModal.class,
	RetweetModal.class,
	DirectMessages.class,
	MediaTweet.class,
	AccountSummary.class,
	FollowerDropDown.class	
})
public class TwitterSuite {

	// must be public and static -- is used by all test cases
	public static TwitterDriver driver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(TwitterSuite.class
				.getResourceAsStream("twitter.properties"));

		driver = new TwitterDriver(options);

		driver.login();

		if (driver.isReCaptcha()) {
			fail("Landed on the reCaptcha page -- cannot continue tests.");

			// there's got to be a better way of handling this; exiting the
			// process is so... not good.
			System.exit(1);
		}
	}

	@Before
	public static void setUp() throws Exception {

		// HACK - get a fresh page before every regression
		driver.navigateTo("https://twitter.com");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		driver.getDriver().quit();

	}

}
