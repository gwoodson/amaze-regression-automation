package com.deque.amaze.twitter;

import static com.deque.junit.Assert.pass;
import static org.junit.Assert.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.amaze.regression.twitter.UseableTweet;
import com.deque.amaze.twitter.FavoriteModal;
import com.deque.amaze.twitter.TwitterSuite;
import com.deque.junit.Order;


public class ContentTweet {

	private static Logger logger = Logger.getLogger(FavoriteModal.class
			.getName());

	private static String tweetType = "div.cards-content.clearfix";   
	
	private static TwitterDriver driver = TwitterSuite.driver;
	
	private static WebElement tweet;
	private static String author;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/");
		driver.injectAmaze();
		driver.waitForPageContainer();
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/*
	 * Find a tweet containing content
	 * print Tweeter's name
	 */
	@Test
	@Order(order = 1)
	public void findPhotoTweet() throws Exception {
		
		tweet = UseableTweet.getTweetByCSSName(driver, tweetType);
		
		//get/print Tweeter's name
		author = tweet.getAttribute("data-name").trim();

		logger.log(
				Level.INFO, 
				String.format("Found tweet with summary/content. Tweet author: %s", author));
		
	}

	/*
	 * Pre-Condition:  Tweet verified as having content.
	 * Test Scenario:  Expand Summary/content tweet.  Summary/content tweets are not displayed in a modal.
	 */
	@Test
	@Order(order = 2)
	public void expandTweetIfNecessary() {

		String className = tweet.getAttribute("class");

		className = " " + className + " ";

		if (className.contains(" opened-tweet ")) {
			pass("Tweet already expanded");
			return;
		}

		WebElement expandLink = tweet.findElement(By
				.cssSelector("a.details.with-icn.js-details"));

		expandLink.sendKeys(Keys.RETURN);

	}

	/*
	 * Pre-Condition:  	
	 * 		- Tweet verified as a Summary/content tweet.
	 * 		- Tweet is expanded
	 * 					
	 * Test Scenario:  Verify Summary/content container exists
	 */
	@Test
	@Order(order = 3)
	public void verifyModal() throws Exception {

		WebElement content = tweet.findElement(By
				.cssSelector("div.cards-content.clearfix"));
		
		logger.log(
				Level.INFO,
				String.format("Found media container: %s",
						PrettyPrint.element(content)));
		
		assertEquals(tweetType, PrettyPrint.element(content));

	}
	
}
