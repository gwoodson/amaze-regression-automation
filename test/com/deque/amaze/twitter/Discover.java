/**
 * 
 */
package com.deque.amaze.twitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static com.deque.junit.Assert.assertWebElement;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.twitter.TwitterDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * Validation of the /i/discover page
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class Discover {

	private static TwitterDriver driver = TwitterSuite.driver;

	/**
	 * Get to the /i/discover page and inject Amaze before running any tests
	 * 
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("https://twitter.com/i/discover");

		driver.injectAmaze();

		driver.waitForPageContainer();

	}

	/**
	 * Validation for the multiple headings Amaze is inserting
	 * @throws InterruptedException 
	 */
	@Test
	@Order(order = 1)
	public void headings() throws InterruptedException {
		
		Thread.sleep(5000L);

		// find the "skip to this" heading
		WebElement h1 = driver.findElement(By.id("amaze-skip-to-this"));
		assertEquals("h1", h1.getTagName().toLowerCase());

		// find the subheader
		driver.findElement(By.cssSelector("h2.amaze.subheader"));

	}

	/**
	 * Validation that the "subheading" paragraph has been removed
	 */
	@Test(expected = NoSuchElementException.class)
	@Order(order = 2)
	public void removeSubheadingParagraph() {

		driver.findElement(By
				.cssSelector("#discover-stories .header-inner p.subheader"));

	}

	/**
	 * Validation for the skip-link. Most of this is covered in
	 * {@link GlobalOverlay#skipLinkAnchor()} and
	 * {@link GlobalOverlay#skipLinkListItem()}, so coverage can be low here
	 */
	@Test
	@Order(order = 3)
	public void skipLink() {

		WebElement skipLink = driver.findElement(By
				.id("amaze-skip-to-tweets-link"));

		assertEquals("a", skipLink.getTagName().toLowerCase());

		String href = skipLink.getAttribute("href");

		assertTrue(href.endsWith("#amaze-skip-to-this"));

		WebElement h1 = driver.findElement(By.id("amaze-skip-to-this"));

		skipLink.sendKeys(Keys.RETURN);

		WebElement activeElement = driver.getActiveElement();

		assertWebElement(h1, activeElement);

	}

}
