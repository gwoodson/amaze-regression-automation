package com.deque.amaze.gui;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.deque.amaze.regression.gui.GUIDriver;
import com.deque.amaze.regression.twitter.TwitterDriver;

@RunWith(Suite.class)
@SuiteClasses({
	TextEdit.class
})
public class GUISuite {
	
	public static GUIDriver driver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(GUISuite.class
				.getResourceAsStream("gui.properties"));
		
		driver = new GUIDriver(options);
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		//driver.getDriver().quit();
	}


}
