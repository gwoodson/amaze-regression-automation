package com.deque.amaze.gui;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.logging.Logger;

import org.json.JSONException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.deque.amaze.SiteException;
import com.deque.amaze.regression.gui.GUIDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class TextEdit {
	
	private static Logger logger = Logger.getLogger(TextEdit.class.getName());

	private static GUIDriver driver = GUISuite.driver;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void launchTestPage() {
		
		driver.navigateTo("http://localhost:8000/woodson/html/michigan.html");
		
	}
	
	@Test
	@Order(order = 2)
	public void verifyTextEdit() {		
		
		
	}

}
