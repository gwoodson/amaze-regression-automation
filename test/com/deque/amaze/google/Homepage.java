/**
 * 
 */
package com.deque.amaze.google;

import static org.junit.Assert.assertEquals;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.RegressionDriver;

/**
 * @author stephenmathieson
 * 
 */
public class Homepage {

	private static RegressionDriver driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(Homepage.class.getResourceAsStream("google.properties"));
		driver = new RegressionDriver(options, "http://google.com");
		driver.injectAmaze();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		driver.getDriver().quit();
	}

	@Test
	@Ignore
	public void searchButton() {

		WebElement button = driver.findElement(By.id("gbqfsa"));

		String styleAttribute = button.getAttribute("style");

		assertEquals("color: red; background-color: blue;", styleAttribute);
	}

	@Test
	@Ignore
	public void focusRing() {

		WebElement active = driver.getActiveElement();

		assertEquals("2px solid rgb(221, 160, 221)",
				active.getCssValue("border"));

	}

	@Test
	public void redStuff() {
		WebElement body = driver.findElement(By.cssSelector("body"));
		
		String fontColor = body.getCssValue("color");

		assertEquals(fontColor, "rgba(255, 0, 0, 1)");
	}
	
}
