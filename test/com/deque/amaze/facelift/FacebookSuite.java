package com.deque.amaze.facelift;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.deque.amaze.regression.facebook.FacebookEventDriver;

/**
 * Suite runner. Uses {@link com.deque.amaze.facebook.FacebookDriver} to
 * interact with Selenium.
 * 
 * @author Greta Woodson <greta.woodson@deque.com>
 */
@SuppressWarnings({
})
@RunWith(Suite.class)
@SuiteClasses({

	NewsFeed.class,
	AboutPage.class,
	Messages.class,
	Login.class,
	PagesFeed.class,
	WelcomePage.class,
	TimeLinePage.class,
	Page404.class
	
})
public class FacebookSuite {
	// must be public and static -- is used by all test cases
	public static FacebookEventDriver eventdriver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(FacebookSuite.class
				.getResourceAsStream("facebook.properties"));

		eventdriver = new FacebookEventDriver(options);
		eventdriver.navigateTo("http://www.facebook.com/");
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		Thread.sleep(3000L);
		eventdriver.getEventDriver().quit();
		
	}

}
