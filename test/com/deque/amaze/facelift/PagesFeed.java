package com.deque.amaze.facelift;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.facebook.FacebookEventDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class PagesFeed {
	
	private static Logger logger = Logger.getLogger(PagesFeed.class.getName());

	private static FacebookEventDriver driver = FacebookSuite.eventdriver;
	
	private static ModalHelper modalhelper = new ModalHelper();
	private static Utilities utilities = new Utilities();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
			
		driver.navigateTo("https://www.facebook.com/pages/feed");
		driver.injectAmazeAsExtension("www.facebook.com", "/pages/feed");
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		if (driver.isLoggedIn()) {
			driver.logoff();
		}	
	}

	@Test
	@Order(order = 1)
	public void verifyAmazement() throws Exception {
		
		WebElement amazement = driver.findElement(By.className("amaze-offscreen"));
				
		logger.log(Level.INFO,
				String.format("Checking amazement element %s",
						amazement));
		
		assertTrue(amazement.getAttribute("class").contains("amaze-offscreen"));
		
	}
	
	@Test
	@Order(order = 2)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
						
		String expectedHeader = "div";		
		WebElement skipLink = utilities.skipLinkExistance(driver);
		
		utilities.verifySkiplink(driver, skipLink, expectedHeader);
		
	}
	
	@Test
	@Order(order = 3)
	public void verifyHelpModal() throws Exception {
		
		WebElement someAnchor = driver.getActiveElement();
		modalhelper.helpModal(driver, someAnchor); 
		
	}
	
	@Test
	@Order(order = 4)
	public void verifyAssertiveAnnouncement() throws Exception {
		
		utilities.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order = 5)
	public void verifyPoliteAnnouncement() throws Exception {
		
		utilities.verifyPoliteness(driver);
		
	}	
	
}
