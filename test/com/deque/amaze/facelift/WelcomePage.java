package com.deque.amaze.facelift;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookEventDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class WelcomePage {
	
	private static Logger logger = Logger.getLogger(NewsFeed.class.getName());

	private static FacebookEventDriver driver = FacebookSuite.eventdriver;
	
	private static ModalHelper modalhelper = new ModalHelper();
	private static Utilities utilities = new Utilities();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		if (driver.isLoggedIn()) {
			driver.logoff();
		}	
	}

	@Test
	@Order(order = 1)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
		
			
		driver.navigateTo("http://www.facebook.com/");
		
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
			
		driver.navigateTo("http://www.facebook.com/?sk=welcome");	
		driver.injectAmazeAsExtension("www.facebook.com", "/?sk=welcome");
		
		String expectedHeader = "h2";		
		WebElement skipLink = utilities.skipLinkExistance(driver);
		
		utilities.verifySkiplink(driver, skipLink, expectedHeader);
		
	}
	
	@Test
	@Order(order = 2)
	public void verifyAmazement() throws Exception {

		WebElement amazement = driver.findElement(By.className("amaze-offscreen"));
		
		logger.log(Level.INFO,
				String.format("Checking amazement element %s",
						amazement));
		
		assertTrue(amazement.getAttribute("class").contains("amaze-offscreen"));
		
	}
	
	@Test
	@Order(order = 3)
	public void verifyAssertiveAnnouncement() throws Exception {
		
		utilities.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order = 4)
	public void verifyPoliteAnnouncement() throws Exception {
		
		utilities.verifyPoliteness(driver);
		
	}
	
	@Test
	@Order(order = 5)
	public void verifyHelpModal() throws Exception {
		
		WebElement someAnchor = driver.getActiveElement();
		modalhelper.helpModal(driver, someAnchor); 
		
	}
	
	@Test
	@Order(order = 6)
	public void verifyRoleIsPresentation() throws Exception {
		
		WebElement profilePaglet = driver.findElement(By.cssSelector("form[ajaxify='/ajax/growth/profile/welcomepage/save_profile.php']"));
		assertTrue(profilePaglet.findElement(By.tagName("table")).getAttribute("role").contains("presentation")); 
		
	}
	

	@Test
	@Order(order = 7)
	public void verifyFriendUnits() throws Exception {
		
		String FriendList = "li.friendBrowserListUnit";
		
		List<WebElement> friendBrowserList = driver.findElements(By.cssSelector(FriendList));
		
		for (int index = (friendBrowserList.size() - 1); index >= 0; index--) {
			WebElement unit = friendBrowserList.get(index).findElement(By.tagName("a"));
			assertTrue(unit.getAttribute("aria-hidden").equals("true"));
			
			logger.log(
					Level.INFO,
					String.format("Found Friend: %s",
							PrettyPrint.element(unit), " No: " + index));
			
		}
		
	}
	
	@Test
	@Order(order = 8)
	public void verifyFriendButtons() throws Exception {
		
		String FriendList = "li.friendBrowserListUnit";
		
		List<WebElement> friendBrowserList = driver.findElements(By.cssSelector(FriendList));
		
		for (int index = (friendBrowserList.size() - 1); index >= 0; index--) {
			WebElement button = friendBrowserList.get(index).findElement(By.tagName("button"));
			assertTrue(button.getAttribute("type").equals("button"));
			
			logger.log(
					Level.INFO,
					String.format("Found Friend Button: %s",
							PrettyPrint.element(button), " No: " + index));
			
		}
		
	}

}
