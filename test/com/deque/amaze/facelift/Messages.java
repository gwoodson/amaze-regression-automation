/**
 * 
 */
package com.deque.amaze.facelift;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookEventDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class Messages {

	private static Logger logger = Logger.getLogger(Messages.class.getName());

	private static FacebookEventDriver driver = FacebookSuite.eventdriver;
	
	private static ModalHelper modalhelper = new ModalHelper();
	private static Utilities utilities = new Utilities();

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		driver.navigateTo("http://www.facebook.com/");
		
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
			
		driver.navigateTo("http://www.facebook.com/messages");
		driver.injectAmazeAsExtension("www.facebook.com", "/messages");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		if (driver.isLoggedIn()) {
			driver.logoff();
		}			
		
	}

	@Test
	@Order(order = 1)
	public void verifyAmazement() throws Exception {
		
		WebElement amazement = driver.findElement(By.className("amaze-offscreen"));
				
		logger.log(Level.INFO,
				String.format("Checking amazement element %s",
						amazement));
		
		assertTrue(amazement.getAttribute("class").contains("amaze-offscreen"));
		
	}
	
	@Test
	@Order(order = 2)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
			
		driver.navigateTo("http://www.facebook.com/messages");
		driver.injectAmazeAsExtension("www.facebook.com", "/messages");
		
		String expectedHeader = "a";		
		WebElement skipLink = utilities.skipLinkExistance(driver);
		
		utilities.verifySkiplink(driver, skipLink, expectedHeader);
		
	}
	
	@Test
	@Order(order = 3)
	public void rightSideBar() {

		WebElement sidebar = driver.findElement(By.id("rightCol"));

		logger.log(Level.INFO, "Found sidebar: " + PrettyPrint.element(sidebar));

		assertFalse("should be hidden", sidebar.isDisplayed());

	}
	
	@Test
	@Order(order = 4)
	public void verifyAssertiveAnnouncement() throws Exception {
		
		utilities.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order = 5)
	public void verifyPoliteAnnouncement() throws Exception {
		
		utilities.verifyPoliteness(driver);
		
	}
	
	@Test
	@Order(order = 6)
	public void verifyHelpModal() throws Exception {
		
		WebElement someAnchor = driver.getActiveElement();
		modalhelper.helpModal(driver, someAnchor); 
		
	}	

}
