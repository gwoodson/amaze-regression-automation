package com.deque.amaze.facelift;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookEventDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/*
 * The EditProfile regression class verifies the edit modals 
 * on each pagelet of the home_edit_profile page.
 * 
 */
@RunWith(OrderedRunner.class)
public class AboutPage {
	
	private static Logger logger = Logger
			.getLogger(AboutPage.class.getName());

	private static FacebookEventDriver driver = FacebookSuite.eventdriver;	
	private static ModalHelper modalhelper = new ModalHelper();
	private static Utilities utilities = new Utilities();
	private static AboutHelper abouthelper = new AboutHelper();
	
	private static String profile = "bob.deque";	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		driver.navigateTo("http://www.facebook.com/");
		
		if (!driver.isLoggedIn()) {
			driver.login();
		}		
			
		driver.navigateTo("https://www.facebook.com/" + profile + "/about");
		driver.injectAmazeAsExtension("facebook.com", "/" + profile + "/about");
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		if (driver.isLoggedIn()) {
			driver.logoff();
		}		
	}
		
	@Test
	@Order(order = 1)
	public void verifyAmazement() throws Exception {
		
		WebElement amazement = driver.findElement(By.className("amaze-offscreen"));
				
		logger.log(Level.INFO,
				String.format("Checking amazement element %s",
						amazement));
		
		assertTrue(amazement.getAttribute("class").contains("amaze-offscreen"));
		
	}
	
	@Test
	@Order(order = 2)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
		
		String expectedHeader = "a";		
		WebElement skipLink = utilities.skipLinkExistance(driver);
		
		utilities.verifySkiplink(driver, skipLink, expectedHeader);
		
	}
	
	@Test
	@Order(order = 3)
	public void verifyReorderEditModal() throws Exception {
		
		WebElement aboutEditButton = driver.findElement(By
				.cssSelector("a.uiPopoverButton._p.uiButton.uiButtonOverlay.uiButtonLarge.uiButtonNoText"));
		
		aboutEditButton.sendKeys(Keys.ENTER);
		
		WebElement trigger = driver.findElement(By.cssSelector("a[href='/ajax/timeline/reorder_apps/dialog.php']"));
		
		Thread.sleep(2500L);
		
		By modalSelector = By.cssSelector("form[action='/ajax/timeline/reorder_apps/confirm.php']");		
		By closeSelector = By.cssSelector("a[role='button']");
		
		modalhelper.verifyReOrderModal(driver, trigger, aboutEditButton, modalSelector, closeSelector, 200);
		
	}
	
	@Test
	@Order(order=4)
	public void verifyRelationshipEditModal() throws Exception {
		
		WebElement trigger = driver.findElement(By
				.cssSelector("a#relationships_edit_button[role='button']"));
		
		By modalSelector = By.cssSelector("form[action='/ajax/timeline/edit_profile/relationships.php']");		
		
		WebElement modal = abouthelper.openModal(driver, trigger, modalSelector);
		
		By closeSelector = By.cssSelector("button[name='save']");
		
		abouthelper.tabUntil(driver, modal, closeSelector, 200);
		
		abouthelper.escapeClosesModal(driver, trigger, modal);
		
		assertEquals(PrettyPrint.element(trigger),
				PrettyPrint.element(driver.getActiveElement()));
	}
	
	@Test
	@Order(order=5)
	public void verifyLivingEditModal() throws Exception {
		
		WebElement trigger = driver.findElement(By
				.cssSelector("a#hometown_edit_button[role='button']"));
		
		By modalSelector = By.cssSelector("form[action='/ajax/timeline/edit_profile/hometown.php?ref=about_tab']");		
		
		WebElement modal = abouthelper.openModal(driver, trigger, modalSelector);
		
		By closeSelector = By.cssSelector("button[name='save']");
		
		abouthelper.tabUntil(driver, modal, closeSelector, 200);
		
		abouthelper.escapeClosesModal(driver, trigger, modal);
		
		assertEquals(PrettyPrint.element(trigger),
				PrettyPrint.element(driver.getActiveElement()));
	}
	
	@Test
	@Order(order = 6)
	public void verifyAssertiveAnnouncement() throws Exception {
			
		driver.navigateTo("https://www.facebook.com/" + profile + "/about");
		driver.injectAmazeAsExtension("facebook.com", "/" + profile + "/about");
		
		utilities.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order = 7)
	public void verifyPoliteAnnouncement() throws Exception {
		
		utilities.verifyPoliteness(driver);
		
	}
	
	@Test
	@Order(order = 8)
	public void verifyHelpModal() throws Exception {
		
		WebElement someAnchor = driver.getActiveElement();
		modalhelper.helpModal(driver, someAnchor); 
		
	}	

	
}
