/**
 * 
 */
package com.deque.amaze.facelift;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.security.SecureRandom;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookEventDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 * @modified by Greta Woodson <greta.woodson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class NewsFeed {

	private static Logger logger = Logger.getLogger(NewsFeed.class.getName());

	private static FacebookEventDriver driver = FacebookSuite.eventdriver;
	
	private static ModalHelper modalhelper = new ModalHelper();
	private static Utilities utilities = new Utilities();
	private static WebElement postbox;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		

		driver.navigateTo("http://www.facebook.com/");
		
		if (!driver.isLoggedIn()) {
			driver.login();
		}
		
		driver.navigateTo("http://www.facebook.com/?sk=nf");
		driver.injectAmazeAsExtension("www.facebook.com", "/?sk=nf");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		if (driver.isLoggedIn()) {
			driver.logoff();
		}					
		
	}
	
	private void verifyAccessKeyP(CharSequence... keys) {
		
		postbox = driver.findElement(By
				.cssSelector("#pagelet_composer textarea"));

		WebElement logo = driver.findElement(By.cssSelector("#pageLogo a"));

		logo.sendKeys(keys);

		WebElement active = driver.getActiveElement();

		assertEquals(postbox, active);

	}

	@Test
	@Order(order = 1)
	public void verifySkiplink() throws InterruptedException, MalformedURLException, Exception {
		String expectedHeader = "div";		
		WebElement skipLink = utilities.skipLinkExistance(driver);
		
		utilities.verifySkiplink(driver, skipLink, expectedHeader);
		
	}	
	
	@Test
	@Order(order = 2)
	public void verifyLikesLinkTitle() throws Exception {
		
		By cssLikesLink = By.cssSelector("a[title='Like this']");
		String title = "Like this";
		
		WebElement likesLink = modalhelper.getTriggerLink(driver, cssLikesLink);
		assertEquals(title, likesLink.getAttribute("title"));
		
	}
	
	@Test
	@Order(order = 3)
	public void verifyAmazement() throws Exception {

		WebElement amazement = driver.findElement(By.className("amaze-offscreen"));
		
		logger.log(Level.INFO,
				String.format("Checking amazement element %s",
						amazement));
		
		assertTrue(amazement.getAttribute("class").contains("amaze-offscreen"));
		
	}
	
	@Test
	@Order(order = 4)
	public void verifySharesModal() throws Exception {

		By cssTrigger = By.cssSelector("a[title='Share this item']");
		By modalSelector = By.cssSelector("form[action='/ajax/sharer/submit/']");		
		By closeSelector = By.cssSelector("button[type='submit']");
		
		modalhelper.verifyModal(driver, modalSelector,
				closeSelector, cssTrigger); 
		
		assertEquals(PrettyPrint.element(driver.findElement(By.cssSelector("a[title='Share this item']"))),
				PrettyPrint.element(driver.getActiveElement()));
		
	}
	
	@Test
	@Order(order = 5)
	public void verifyHelpModal() throws Exception {
		
		Thread.sleep(2500L);
		
		WebElement someAnchor = driver.getActiveElement();
		modalhelper.helpModal(driver, someAnchor); 
		
	}
	
	@Test
	@Order(order = 6)
	public void verifyAssertiveAnnouncement() throws Exception {
			
		utilities.verifyAssertiveness(driver);
		
	}
	
	@Test
	@Order(order = 7)
	public void verifyPoliteAnnouncement() throws Exception {
		
		utilities.verifyPoliteness(driver);
		
	}
	
	@Test
	@Ignore
	@Order(order = 8)
	public void altShiftP() {

		logger.log(Level.INFO, "Verifing ALT+SHIFT+P");
		verifyAccessKeyP(Keys.ALT, Keys.SHIFT, "P");

	}

	@Test
	@Ignore
	@Order(order = 9)
	public void ctrlP() {

		logger.log(Level.INFO, "Verifying CTRL+P");
		verifyAccessKeyP(Keys.CONTROL, "P");

	}
	
}
