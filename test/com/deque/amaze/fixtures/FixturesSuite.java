package com.deque.amaze.fixtures;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.deque.amaze.regression.Driver;
import com.deque.amaze.regression.fixtures.FixturesDriver;

/**
 * Suite runner. Uses {@link com.deque.amaze.facebook.FacebookDriver} to
 * interact with Selenium.
 * 
 * @author Greta Woodson <greta.woodson@deque.com>
 */
@SuppressWarnings({
	"deprecation"
})
@RunWith(Suite.class)
@SuiteClasses({	
	CssVerification.class

})
public class FixturesSuite  {
	
	public static FixturesDriver driver;
		
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(FixturesSuite.class
				.getResourceAsStream("fixtures.properties"));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		//driver.getDriver().quit();

	}

}
