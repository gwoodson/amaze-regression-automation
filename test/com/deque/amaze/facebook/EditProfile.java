package com.deque.amaze.facebook;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.SiteException;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/*
 * The EditProfile regression class verifies the edit modals 
 * on each pagelet of the home_edit_profile page.
 * 
 */
@RunWith(OrderedRunner.class)
public class EditProfile {
	
	private static Logger logger = Logger
			.getLogger(EditProfile.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;	
	private static String editButton = "a.profileEditButton.uiButton.uiButtonOverlay";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		driver.navigateTo("http://facebook.com");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.navigateTo("https://www.facebook.com/greta.woodson?sk=info&edit=eduwork&ref=home_edit_profile");

		driver.injectAmazeAsExtension("facebook.com", "/greta.woodson?sk=info&edit=eduwork&ref=home_edit_profile");
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	/*
	 * The openEditModal is a common utility that will open the edit modal
	 * for each pagelet on the profile page and takes the following parameters:
	 * 		FacebookDriver driver
	 * 		String pageletname  (By.id)
	 */
	public WebElement openEditModal(FacebookDriver driver, String pageletname) throws InterruptedException{		
		
		driver.waitForElementVisible(By.id(pageletname));
		
		WebElement pagelet = driver.findElement(By.id(pageletname));
		
		Thread.sleep(2000L);
		
		driver.waitForElementVisible(By.cssSelector(editButton));
		
		WebElement editbutton = pagelet.findElement(By.cssSelector(editButton));
		
		editbutton.sendKeys(Keys.ENTER);
		
		Thread.sleep(2000L);
		
		WebElement modal = driver.findElement(By.cssSelector("div.uiContextualLayer"));
		
		return modal;
	}

	
	@Test
	@Order(order = 1)
	public void editBasicInfo() {
		
		String pagelet = "pagelet_basic";
		try {
			WebElement editModal = openEditModal(driver, pagelet);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	@Order(order = 2)
	public void editAboutYou() {
		
		String pagelet = "pagelet_bio";
		try {
			WebElement modal = openEditModal(driver, pagelet);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	@Order(order = 3)
	public void editLiving() {
		
		String pagelet = "pagelet_hometown";
		try {
			WebElement modal = openEditModal(driver, pagelet);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	@Order(order = 4)
	public void editRelationships() {
		
		String pagelet = "pagelet_relationships";
		try {
			WebElement modal = openEditModal(driver, pagelet);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	@Order(order = 5)
	public void editContactInfo() {
		
		String pagelet = "pagelet_contact";
		try {
			WebElement modal = openEditModal(driver, pagelet);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	@Order(order = 6)
	public void editQuotes() {
		
		String pagelet = "pagelet_quotes";
		try {
			WebElement modal = openEditModal(driver, pagelet);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	@Order(order = 7)
	public void editWorkEducation() throws InterruptedException {
		
		//"label.uiButton uiButtonOverlay"
		WebElement donebutton = driver.findElement(By.id("pagelet_eduwork")).findElement(By.cssSelector("label.uiButton.uiButtonOverlay"));
		String label = donebutton.getText();
		
		if (label.equalsIgnoreCase("Done Editing")) {
			
			donebutton.sendKeys(Keys.ENTER);
			
			Thread.sleep(5000L);
			
			logger.log(
					Level.INFO,
					String.format("Send keys to Work Edu edit button#%d: %s", PrettyPrint.element(donebutton)));
			
		}
				
	}
		
}
