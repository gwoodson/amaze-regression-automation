/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class TimelinePhotoSharesModal {

	private static Logger logger = Logger
			.getLogger(TimelinePhotoSharesModal.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	private static WebElement sharesLink;
	private static WebElement modal;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		//

		driver.navigateTo("http://facebook.com/");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		// get to the news feed
		driver.navigateTo("http://facebook.com?sk=nf");

		// find a link to a timeline
		WebElement photoLink = driver.findElement(By
				.cssSelector("div[role='article'] div.photoRedesign"
						+ " a[rel='theater']"));
		String href = photoLink.getAttribute("href");

		// bit of a hack here -- should probably use URL instead
		int slashIndex = href.lastIndexOf('/');
		String path = href.substring(slashIndex);

		logger.log(Level.INFO, String.format("found photo page at: %s", path));

		driver.navigateTo(href);

		driver.injectAmazeAsExtension("facebook.com", path);

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	/*
	 * findSharesLink = the link on the timeline photo that displays the number of shares
	 * example:  300 shares
	 * when this link is selected a modal is displayed listing all who have shared
	 */
	@Test
	@Order(order = 1)
	public void findSharesLink() throws InterruptedException {
		
		Thread.sleep(5000L);
		
		sharesLink = driver.findElement(By
				.cssSelector("a[rel='dialog'].UFIShareLink"));

	}

	@Test
	@Order(order = 2)
	public void linkOpensModal() {

		sharesLink.sendKeys(Keys.RETURN);

		modal = driver.waitForElementVisible(By
				.cssSelector("div.uiLayerPageWrapper.shareOverlay.uiLayer"));

	}

	@Test
	@Order(order = 3)
	public void titleTabIndex() {

		WebElement title = modal.findElement(By.tagName("h2"));
		String tabIndex = title.getAttribute("tabindex");
		assertEquals("-1", tabIndex);

	}

	@Test
	@Order(order = 4)
	public void autoFocusTitle() {

		WebElement expected = modal.findElement(By.tagName("h2"));
		WebElement actual = driver.getActiveElement();

		// human readable
		assertEquals(PrettyPrint.element(expected), PrettyPrint.element(actual));
		// deep
		assertEquals(expected, actual);
	}

	@Test
	@Order(order = 5)
	public void cylindricalTabbing() throws Exception {

		int counter = 0;

		WebElement first = modal.findElement(By
				.cssSelector("ul.uiList.uiStream"
						+ " li.uiStreamStory h5 a.passiveName"));

		// tab into the next element
		first.sendKeys(Keys.TAB);

		// keep tabbing until we reach the same element
		while (!driver.getActiveElement().equals(first)) {
			logger.log(Level.INFO, String.format("Tab %d", counter));

			driver.getActiveElement().sendKeys(Keys.TAB);

			logger.log(
					Level.INFO,
					"Currently active element: "
							+ PrettyPrint.element(driver.getActiveElement()));

			counter += 1;

			if (counter > 500) {
				fail("reached tab limit.  this may not be an actual"
						+ " error, but a really popular post instead.");
			}
		}

	}

	@Test
	@Order(order = 6)
	public void escapeClosesModal() throws Exception {

		modal.findElement(By.tagName("h2")).sendKeys(Keys.ESCAPE);
		try {

			// *sometimes* it takes a bit for the modal to close
			if (modal.isDisplayed()) {
				logger.log(Level.INFO,
						"Modal is visible.  Waiting for it to hide.");
				driver.waitForElementHidden(modal);

			}

		} catch (StaleElementReferenceException e) {
			logger.log(Level.INFO, "Reference to the modal has gone"
					+ " stale.  This *probably* means it's no"
					+ " longer in the DOM.");
		}

		// sleep so javascript can fire
		Thread.sleep(500);
	}

	@Test
	@Order(order = 7)
	public void refocusSharesLink() {

		WebElement actual = driver.getActiveElement();

		// human readable
		assertEquals(PrettyPrint.element(sharesLink),
				PrettyPrint.element(actual));
		// deep
		assertEquals(sharesLink, actual);

	}
}
