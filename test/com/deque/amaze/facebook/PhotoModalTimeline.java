/**
 * 
 */
package com.deque.amaze.facebook;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.Modal;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * TODO too much code duplicate between this regression and
 * {@link com.deque.amaze.facebook.PhotoModalNewsFeed}. There should a better
 * way of sharing code between the two.
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class PhotoModalTimeline {

	private static Logger logger = Logger.getLogger(PhotoModalTimeline.class
			.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com/");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		// get to the news feed
		driver.navigateTo("http://facebook.com/?sk=nf");

		// find a link to a timeline
		/*WebElement timelineLink = driver.findElement(By
				.cssSelector("h5.uiStreamHeadline .actorName a"));*/
		/*		WebElement timelineLink = driver.findElement(By
				.cssSelector("a._4g5p._4_ic"));
		
		String href = timelineLink.getAttribute("href");

		logger.log(Level.INFO, String.format("Found link: %s", href));

		// bit of a hack here -- should probably use URL instead
		int slashIndex = href.lastIndexOf('/');
		String path = href.substring(slashIndex);

		driver.navigateTo(href);
		logger.log(
				Level.INFO,
				String.format("Found href element: %s",
						href));
*/
		driver.injectAmazeAsExtension("facebook.com", "/?sk=nf");
	}

	@Test
	@Order(order = 1)
	public void photoModal() throws Exception {

		/*WebElement trigger = driver
				.findElement(By
						.cssSelector("#timeline_tab_content .photoUnit .uiScaledThumb a"));
		*/
		WebElement trigger = driver
				.findElement(By
						.cssSelector("div.photoRedesignSquare.photoRedesignLink")).findElement(By.tagName("a"));
		
		By modal = By.cssSelector("div#photos_snowlift");
		By close = By.cssSelector("a.closeTheater");

		//new Modal(driver, trigger, modal, close, 100).verify();
	}
}
