package com.deque.amaze.facebook;

import java.io.File;
import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class TestBase {
	
	//initialize the property files
	public static Properties CONFIG = null;
	public static Properties OR = null;
	
	public static WebDriver dr = null;
	public static EventFiringWebDriver driver = null;
	
	//isLoggedIn set "true" following successful logon
	public static boolean isLoggedIn = false;
	
	public static void initialize() throws IOException{
		
		if (driver == null) {
			//load CONFIG property file
			CONFIG = new Properties();
			FileInputStream fn = new FileInputStream(System.getProperty("user.dir")+"//test//com//deque//amaze//facebook//facebook.properties");
			CONFIG.load(fn);
		
			//load "OBJECT REPOSITORY" property file
			OR = new Properties();
			FileInputStream xpath = new FileInputStream(System.getProperty("user.dir")+"//test//com//deque//amaze//facebook//OR.properties");
			OR.load(xpath);
		
			//Initialize the webdriver and eventfiring webdriver
			if(CONFIG.getProperty("browser").equals("firefox")) {				
				
				ProfilesIni profilesIni = new ProfilesIni();
				String profileName="garbo";
				
				// Clone the named profile                
				FirefoxProfile profile = profilesIni.getProfile(profileName);
				dr = new FirefoxDriver(new FirefoxBinary(new File("C://Program Files (x86)//Mozilla Firefox//firefox.exe")),profile);
							
			} else if (CONFIG.getProperty("browser").equals("ie")){	
				//This method of starting the IE driver is deprecated and will be removed in selenium 2.26. 
				//Please download the IEDriverServer.exe from http://code.google.com/p/selenium/downloads/list 
				//     and ensure that it is in your PATH.

				System.setProperty("webdriver.ie.driver", "C://Users//Garbo//IEDriverServer_x64_2.29.0//IEDriverServer.exe");
				dr = new InternetExplorerDriver();
			
			} else if (CONFIG.getProperty("browser").equals("chrome")){	
		
				System.setProperty("webdriver.chrome.driver", "C://Users//Garbo//work//chromedriver.exe");
				dr = new ChromeDriver();
			
			} else {
							
			}
		
			driver = new EventFiringWebDriver(dr);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						
		}
		
	}
	
	public static WebElement getObject(String xpathKey) {
		
		try {			
			return driver.findElement(By.xpath(xpathKey));
			
			
		} catch(Throwable t) {
			//report error
			return null;
			
		}
		
	}
	
	public static void doLogin(String username, String password) throws InterruptedException {
		
		if (isLoggedIn) {
			return;
		}
		
		Thread.sleep(5000L);
		
		//initialize username and password input fields
		getObject(OR.getProperty("username_signin_input")).clear();
		getObject(OR.getProperty("password_signin_input")).clear();
		
		//set username 
		getObject(OR.getProperty("username_signin_input")).sendKeys(username);
		
		//set password
		WebElement passwordInput = getObject(OR.getProperty("password_signin_input"));		
		passwordInput.sendKeys(password);
		passwordInput.submit();
		
		Thread.sleep(5000L);
		
		//Verify successful logon
		//Set isLoggedIn boolean
		try {			
			if ( driver.getTitle().trim().equals("Facebook") ) {
				isLoggedIn = true;
								
			} else {
				isLoggedIn = false;
				
			}			
			
		} catch (Throwable t) {
			isLoggedIn = false;
						
		}
		
	}

}
