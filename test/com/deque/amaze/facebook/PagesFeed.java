/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.assertFalse;

import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.facebook.FacebookDriver;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class PagesFeed {

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(PagesFeed.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com/pages/feed");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.waitForElementVisible(By.id("rightCol"));

		driver.injectAmazeAsExtension("facebook.com", "/pages/feed");
	}

	@Test
	public void hideRightColumn() {

		WebElement right = driver.findElement(By.id("rightCol"));

		assertFalse(right.isDisplayed());

	}

}
