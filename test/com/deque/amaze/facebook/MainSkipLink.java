/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.facebook.FacebookDriver;

/**
 * Test the "main/global" overlay's skip link
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 * @deprecated Use {@link com.deque.amaze.facebook.SkipLink} instead
 */
public class MainSkipLink extends TestBase {

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(MainSkipLink.class
			.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.injectAmazeAsExtension("facebook.com", "/");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	public void skipLinkExistance() {

		driver.findElement(By.id("amaze-skip-link"));

	}

	@Test
	public void skipLinkClassName() {

		WebElement link = driver.findElement(By.id("amaze-skip-link"));

		String className = link.getAttribute("class");

		assertTrue(className.contains("offscreen"));

	}

	@Test
	public void skipLinkHref() {

		WebElement link = driver.findElement(By.id("amaze-skip-link"));

		String href = link.getAttribute("href");

		assertTrue(href.endsWith("#amaze-skip-to-this"));

	}

	@Test
	public void skipToThisExistance() {

		driver.findElement(By.id("amaze-skip-to-this"));

	}

	@Test
	public void skipToThisHeading() {

		WebElement heading = driver.findElement(By.id("amaze-skip-to-this"));

		assertEquals("h2", heading.getTagName().toLowerCase());

	}

	@Test
	public void skipToThisHeadingTabIndex() {

		WebElement heading = driver.findElement(By.id("amaze-skip-to-this"));
		String tabIndex = heading.getAttribute("tabindex");

		assertEquals("-1", tabIndex);

	}

	@Test
	public void linkFocusesHeading() {

		WebElement link = driver.findElement(By.id("amaze-skip-link"));
		WebElement heading = driver.findElement(By.id("amaze-skip-to-this"));

		link.sendKeys(Keys.RETURN);

		WebElement actual = driver.getActiveElement();
		assertEquals(heading, actual);

	}

}
