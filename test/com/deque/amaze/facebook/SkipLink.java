/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;

/**
 * TODO timeline page (facebook.com/foo.bar), timeline photo page
 * (facebook.com/photo.php?fbid=...&)
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class SkipLink {

	private static Logger logger = Logger.getLogger(SkipLink.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	private static void navigate(String path) throws Exception {

		logger.log(Level.INFO, String.format("navigating to %s", path));

		driver.navigateTo("http://facebook.com");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.navigateTo("http://facebook.com" + path);

		driver.injectAmazeAsExtension("facebook.com", path);

		genericTests();
	}

	private static void genericTests() {

		logger.log(Level.INFO, "verifying link existance and properties");

		// will throw if it's not present
		WebElement link = driver.findElement(By.id("amaze-skip-link"));

		String tagName = link.getTagName().toLowerCase();
		assertEquals("should be an anchor", "a", tagName);

		String className = " " + link.getAttribute("class") + " ";
		assertTrue("skip link should be offscreen",
				className.contains(" offscreen "));

		String href = link.getAttribute("href");
		assertTrue("skip link's HREF should point to #amaze-skip-to-this",
				href.endsWith("#amaze-skip-to-this"));

		logger.log(Level.INFO, "verifying content existance and properties");

		// will throw if it's not present
		WebElement content = driver.findElement(By.id("amaze-skip-to-this"));

		/*
		 * for the messages page, the skip link does not point to an h2. i'm
		 * removing this assertion because it was stupid to begin with. 
		 * tagName = content.getTagName(); 
		 * assertEquals("should be an h2", "h2", tagName);
		 */

		String tabIndex = content.getAttribute("tabindex");
		assertEquals("should be focusable", "-1", tabIndex);

		String ariaHidden = content.getAttribute("aria-hidden");
		assertEquals("should be null", null, ariaHidden);
		
		logger.log(Level.INFO, "verifying skip-link functionallity");

		// should focus the content
		link.sendKeys(Keys.RETURN);

		WebElement active = driver.getActiveElement();
		String actual = PrettyPrint.element(active);
		String expected = PrettyPrint.element(content);

		assertEquals(expected, actual);
	}

	@Test
	public void pageSkWelcome() throws Exception {

		navigate("/?sk=welcome");

	}

	@Test
	public void pageSkNf() throws Exception {

		navigate("?sk=nf");

	}

	@Test
	public void pagePageFeed() throws Exception {

		navigate("/pages/feed");

	}

	@Test
	public void page404() throws Exception {

		SecureRandom random = new SecureRandom();

		String probably404 = "/foo-" + new BigInteger(130, random).toString(32);

		navigate(probably404);

	}

	@Test
	public void messages() throws Exception {

		navigate("/messages");

	}

}
