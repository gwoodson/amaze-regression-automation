package com.deque.amaze.facebook;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class HelpModal {

	private static Logger logger = Logger.getLogger(HelpModal.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	private static WebElement from;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.injectAmazeAsExtension("facebook.com", "/");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	@Order(order = 1)
	public void openHelpModal() throws InterruptedException {

		from = driver.getActiveElement();

		from.sendKeys(Keys.ALT, Keys.CONTROL, "h");

		Thread.sleep(500);

		WebElement activeElement = driver.getActiveElement();
		WebElement expectedElement = driver.findElement(By
				.xpath("//*[@id='amaze-help-modal']/div/table/caption/h3"));

		String expected = PrettyPrint.element(expectedElement);
		String actual = PrettyPrint.element(activeElement); // *[@id="amaze-help-modal"]/div/table/caption/h3

		logger.log(Level.INFO,
				String.format("%s -> Open Help Modal -> %s", expected, actual));

		assertEquals(expected, actual);
	}

	@Test
	@Order(order = 2)
	public void tabToKey() throws InterruptedException {

		from = driver.findElement(By.cssSelector("h3[tabindex='0']")); 

		from.sendKeys(Keys.TAB);
		
		WebElement activeElement = driver.getActiveElement();
		WebElement expectedElement = driver.findElement(By
				.xpath("//*[@id='amaze-access-function']"));

		String expected = PrettyPrint.element(expectedElement);
		String actual = PrettyPrint.element(activeElement);

		logger.log(Level.INFO,
				String.format("%s -> tabToKey Element -> %s", expected, actual));

		assertEquals(expected, actual);
	}

	@Test
	@Order(order = 3)
	public void tabToFunction() throws InterruptedException {

		//from = driver.findElement(By.xpath("//*[@id='amaze-access-keycode']"));

		//from.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();
		
		activeElement.sendKeys(Keys.TAB);
		
		WebElement expectedElement = driver.findElement(By
				.xpath("//*[@id='amaze-access-function']"));

		String expected = PrettyPrint.element(expectedElement);
		String actual = PrettyPrint.element(activeElement);

		logger.log(Level.INFO, String.format(
				"%s -> tabToFunction Element -> %s", PrettyPrint.element(expectedElement), PrettyPrint.element(activeElement)));

		assertEquals(expected, actual);
	}

	@Test
	@Order(order = 4)
	public void tabToClose() throws InterruptedException {

		from = driver.findElement(By.xpath("//*[@id='amaze-access-function']"));

		from.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();
		WebElement expectedElement = driver.findElement(By
				.xpath("//*[@id='amaze-help-modal']/div/button"));

		String expected = PrettyPrint.element(expectedElement);
		String actual = PrettyPrint.element(activeElement);

		logger.log(Level.INFO,
				String.format("%s -> tabToClose -> %s", expected, actual));

		assertEquals(expected, actual);
	}

	@Test
	@Order(order = 5)
	public void tabToTitle() throws InterruptedException {

		from = driver.findElement(By
				.xpath("//*[@id='amaze-help-modal']/div/button"));

		from.sendKeys(Keys.TAB);

		WebElement activeElement = driver.getActiveElement();
		WebElement expectedElement = driver.findElement(By
				.xpath("//*[@id='amaze-help-modal']/div/table/caption/h3"));

		String expected = PrettyPrint.element(expectedElement);
		String actual = PrettyPrint.element(activeElement);

		logger.log(Level.INFO,
				String.format("%s -> tabToTitle -> %s", expected, actual));

		assertEquals(expected, actual);
	}

	/*
	 * Verify focus of source element on close of helpmodal fb landing page
	 * search element = //*[@id="q"] #amaze-help-modal button[type="button"]
	 */
	@Test
	@Order(order = 6)
	public void verifyFocusOnClose() throws InterruptedException {

		WebElement closebutton = driver.findElement(By
				.xpath("//*[@id='amaze-help-modal']/div/button"));

		closebutton.sendKeys(Keys.ENTER);

		// driver.waitForElementVisible(By.xpath("//*[@id='q']"));

		WebElement searchbox = driver.findElement(By.xpath("//*[@id='q']"));

		searchbox.sendKeys(Keys.ALT, Keys.CONTROL, "h");

		driver.waitForElementVisible(By.id("amaze-help-modal"));

		closebutton = driver.findElement(By
				.xpath("//*[@id='amaze-help-modal']/div/button"));

		closebutton.sendKeys(Keys.ENTER);

		driver.waitForElementHidden(By.id("amaze-help-modal"));

		WebElement activeElement = driver.getActiveElement();
		WebElement expectedElement = driver.findElement(By
				.xpath("//*[@id='q']"));

		String expected = PrettyPrint.element(expectedElement);
		String actual = PrettyPrint.element(activeElement);

		logger.log(Level.INFO, String.format(
				"%s -> verify focus following close of help modal -> %s",
				expected, actual));

		assertEquals(expected, actual);

	}

}
