/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class ShareModal {

	private static Logger logger = Logger.getLogger(ShareModal.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	private static WebElement shareButton;
	private static WebElement modal;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		// ?sk=nf to force the news feed
		driver.navigateTo("http://facebook.com/");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.navigateTo("http://facebook.com?sk=nf");

		driver.injectAmazeAsExtension("facebook.com", "/");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	public void findShareButton() {

		shareButton = driver.findElement(By.cssSelector("a.share_action_link"));

	}

	public void buttonOpensModal() throws InterruptedException {  //gw added throws where there was none

		shareButton.sendKeys(Keys.RETURN);

		modal = driver.waitForElementVisible(By
				.cssSelector("div.uiLayer div[role='dialog'] form"));

		logger.log(Level.INFO,
				String.format("Found modal: %s", PrettyPrint.element(modal)));

	}
	
	public void cancelExists() throws InterruptedException {
		
		modal.findElement(By.cssSelector("a.layerCancel"));

	}

	public void tabUntilCancel() {

		WebElement cancel = modal.findElement(By.cssSelector("a.layerCancel"));

		logger.log(
				Level.INFO,
				String.format("Found cancel button: %s",
						PrettyPrint.element(cancel)));

		int counter = 0;

		while (!driver.getActiveElement().equals(cancel)) {
			logger.log(Level.INFO, String.format("Tab %d", counter));

			driver.getActiveElement().sendKeys(Keys.TAB);

			logger.log(
					Level.INFO,
					"Currently active element: "
							+ PrettyPrint.element(driver.getActiveElement()));

			counter += 1;

			if (counter > 100) {
				fail("reached tab limit");
			}
		}
	}

	public void cancelFocusesButton() throws Exception {

		WebElement cancel = modal.findElement(By.cssSelector("a.layerCancel"));

		cancel.sendKeys(Keys.RETURN);
		// *sometimes* it takes a bit for the modal to close
		if (cancel.isDisplayed()) {
			logger.log(Level.INFO, "Modal is visible.  Waiting for it to hide.");
			try {
				driver.waitForElementHidden(cancel);
			} catch (StaleElementReferenceException e) {
				logger.log(Level.INFO,
						"Reference to the cancel button has gone stale.  This "
								+ "*probably* means it's no longer in the DOM.");
			}
		}

		WebElement actual = driver.getActiveElement();

		assertEquals(PrettyPrint.element(shareButton),
				PrettyPrint.element(actual));

	}
}
