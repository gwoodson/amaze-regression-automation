/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
public class Messages {

	private static Logger logger = Logger.getLogger(Messages.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.navigateTo("http://facebook.com/messages");

		driver.injectAmazeAsExtension("facebook.com", "/messages");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	public void rightSideBar() {

		WebElement sidebar = driver.findElement(By.id("rightCol"));

		logger.log(Level.INFO, "Found sidebar: " + PrettyPrint.element(sidebar));

		assertFalse("should be hidden", sidebar.isDisplayed());

	}

}
