package com.deque.amaze.facebook;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.deque.amaze.SiteException;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.amaze.regression.twitter.TwitterDriver;

/**
 * 
 * @author greta.woodson@deque.com
 */
public class NewsFeedUtility {
	
	private static Logger logger = Logger.getLogger(NewsFeedUtility.class
			.getName());
	
	private static List<WebElement> newsfeedList;
	private static List<WebElement> videoList;
	private static WebElement liked;

	/**
	 * 
	 */
	public NewsFeedUtility() {

	}
	
	public static List<WebElement> findNewsfeedLinks(FacebookDriver driver) {

		// find the newsfeed lineitem
		newsfeedList = driver.findElements(By
					.cssSelector("div.storyInnerContent"));

			logger.log(Level.INFO,
					String.format("Found newsfeed link: %s", newsfeedList.size()));
			
		return newsfeedList;
						
	}
	
	/**
	 * Check if Newsfeed line item type is known
	 * 
	 * @return WebElement
	 */
	public static WebElement isPhotoLink(FacebookDriver driver){

		// find a link to a timeline
		WebElement photoLink = driver.findElement(By
				.cssSelector("div[role='article'] div.photoRedesign"
						+ " a[rel='theater']"));	
		
		return photoLink;	
		
	}
	
	public static WebElement findSharedNewsfeed(FacebookDriver driver){

		// find a link to a timeline
		WebElement article = driver.findElement(By
				.cssSelector("div[role='article'] div.mvm.uiStreamAttachments.fbMainStreamAttachment"));	
		
		return article;	
		
	}

	/**
	 * Check if Newsfeed line item type is known
	 * 
	 * @param newsfeed link 
	 * @return boolean
	 */
	public boolean hasBeenLiked(FacebookDriver driver)
			throws InterruptedException {	

		try {
			Thread.sleep(3000L);

			driver.findElement(By.cssSelector("a.UFILikeLink").linkText("Unlike"));
			
			logger.log(
					Level.INFO,
					String.format("Checking newsfeed link#%d: %s", PrettyPrint.element(liked)));
			
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static WebElement getLikedLink(FacebookDriver driver)
			throws InterruptedException {	
		
		WebElement link=null;

		try {
			Thread.sleep(5000L);

			link = driver.findElement(By.cssSelector("a.UFILikeLink").linkText("Like"));
			//link = driver.findElement(By.cssSelector("a[rel='dialog']"));
						
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return link;
		}
		
		return link;
	}
	
	
}
