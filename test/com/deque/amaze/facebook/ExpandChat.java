package com.deque.amaze.facebook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class ExpandChat {

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(ExpandChat.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com/?sk=welcome");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.injectAmazeAsExtension("facebook.com", "/?sk=welcome");
	}	

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {		

	}
	
	/*
	 * Open Chat verifies user is able to:
	 * 		Tab to chat link
	 * 		Send Return keys to Open Chat session
	 * 		Verify active element contains href/link "https://www.facebook.com/ajax/messaging/composer.php"
	 */
	@Test
	@Order(order = 1)
	public void openChat() throws Exception {

		Thread.sleep(3000L);
		
		int i = 0;
	
		while (i < 7) {			
			driver.getActiveElement().sendKeys(Keys.TAB);
			i++;			
		}
		
		WebElement activeElement = driver.getActiveElement();
		logger.log(
				Level.INFO,
				String.format("Found active element: %s",
						PrettyPrint.element(activeElement)));
		
		driver.getActiveElement().sendKeys(Keys.RETURN);
		
		activeElement = driver.getActiveElement();
		logger.log(
				Level.INFO,
				String.format("Found active element: %s",
						PrettyPrint.element(activeElement)));
		assertEquals(activeElement.getAttribute("href"), "https://www.facebook.com/ajax/messaging/composer.php");		
		
	}
	
	/*
	 * Send Chat verifies user is able to complete the following via keyboard:
	 * 		populate "To" field
	 * 		populate "Message"
	 * 		send
	 */
	@Test
	@Order(order = 2)
	public void sendChat() throws Exception {
		
		driver.getActiveElement().sendKeys(Keys.RETURN);
		
		WebElement to = driver.getActiveElement();
		
		to.sendKeys("Greta Woodson");
		to.sendKeys(Keys.RETURN);
		to.sendKeys(Keys.TAB);
		
		WebElement message = driver.getActiveElement();
		
		message.sendKeys("Test");
		message.sendKeys(Keys.ENTER);
		
		Thread.sleep(3000L);		
		
	}
	
	/*
	 * Send Chat verifies user is able to complete the following via keyboard:
	 * 		populate "To" field
	 * 		populate "Message"
	 * 		send
	 */
	@Test
	@Order(order = 3)
	public void selectFriend() throws Exception {
		
		driver.navigateTo("http://facebook.com/?sk=welcome");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.injectAmazeAsExtension("facebook.com", "/?sk=welcome");
		
		int i = 0;
		
		while (i < 7) {			
			driver.getActiveElement().sendKeys(Keys.TAB);
			i++;			
		}
		
		WebElement activeElement = driver.getActiveElement();
		logger.log(
				Level.INFO,
				String.format("Found active element: %s",
						PrettyPrint.element(activeElement)));
		
		driver.getActiveElement().sendKeys(Keys.RETURN);
		
		int j = 0;
		
		while (j < 4) {			
			driver.getActiveElement().sendKeys(Keys.TAB);
			j++;			
		}
		
		activeElement = driver.getActiveElement();
		
		activeElement.sendKeys(Keys.ENTER);		
		
		logger.log(
				Level.INFO,
				String.format("Found active element: %s",
						PrettyPrint.element(driver.getActiveElement())));
		
		assertEquals("message_body", driver.getActiveElement().getAttribute("name"));		
		
		WebElement message = driver.getActiveElement();
		
		message.sendKeys("The only problem with socialism is... eventually you run out of other peoples money.");
		message.sendKeys(Keys.TAB);
		driver.getActiveElement().sendKeys(Keys.ENTER);

	}
	
	@Test
	@Order(order = 4)
	public void selectfromFriendList() throws Exception {
		
		WebElement startHere = driver.findElement(By.cssSelector("div.fbChatSidebarBody"));		
		WebElement friend = startHere.findElement(By.tagName("a"));
		friend.sendKeys(Keys.TAB);
		driver.getActiveElement().sendKeys(Keys.ENTER);
		
		WebElement message = driver.getActiveElement();
		
		message.sendKeys("This chat was opened by sending keys from left navbar.");
		message.sendKeys(Keys.TAB);
		driver.getActiveElement().sendKeys(Keys.ENTER);
	}


}
