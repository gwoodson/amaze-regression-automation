/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class GenericAccessKeys {

	private static Logger logger = Logger.getLogger(GenericAccessKeys.class
			.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.navigateTo("http://facebook.com/?sk=nf");

		driver.injectAmazeAsExtension("facebook.com", "/");

		logger.log(Level.INFO, "at the news feed");

	}

	/**
	 * Run a generic verification for an accesskey combo
	 * 
	 * @param expectedElement
	 *            A selector for the element we're expecting to receive focus
	 * @param keyStroke
	 *            The keystroke which should focus the element
	 */
	private void tests(By expectedElement, CharSequence... keyStroke) {

		WebElement logo = driver.findElement(By.cssSelector("#pageLogo a"));

		logo.sendKeys(keyStroke);

		WebElement actual = driver.getActiveElement();
		WebElement expected = driver.findElement(expectedElement);

		// human-readable assertion
		assertEquals(PrettyPrint.element(expected), PrettyPrint.element(actual));
		assertEquals(expected, actual);

	}

	@Test
	@Order(order = 1)
	public void accessKeyP() {

		tests(By.cssSelector("#pagelet_composer textarea"), Keys.ALT, "p");
	}

	@Test
	@Order(order = 2)
	public void accessKeyH() throws InterruptedException {

		Thread.sleep(5000L);
		
		WebElement active = driver.getActiveElement();
		active.sendKeys(Keys.CONTROL, Keys.ALT, "h");
		
		Thread.sleep(3000L);
		
		//tests(By.cssSelector("#amaze-help-modal h3"), Keys.CONTROL, Keys.ALT, "h");
		driver.findElement(By.cssSelector("#__amaze-help-modal-close")).sendKeys(Keys.ENTER);
	}

	@Test
	@Order(order = 3)
	public void accessKeyQ() {

		tests(By.cssSelector("#q"), Keys.ALT, "q");
	}

	@Ignore
	@Test
	@Order(order = 4)
	public void accessKeyS() throws InterruptedException {

		tests(By.cssSelector("#chatFriendsOnline .inputsearch"), Keys.ALT, "s");
	}

	@Test
	@Order(order = 5)
	public void accessKeyC() {

		// will only verify the "buddy list" trigger is focused
		tests(By.cssSelector("#fbDockChatBuddylistNub a.fbNubButton"),
				Keys.ALT, "c");
	}

}
