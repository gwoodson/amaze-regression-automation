/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.text.html.Option;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * Very basic tests for ACCESS+P keystroke. Upon this keystroke, focus should be
 * given to the "postbox", or the textarea used to post a message to your
 * "wall".
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class AccessKeyP {
	
	private static Logger logger = Logger.getLogger(AccessKeyP.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;
	
	private static WebElement postbox;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.navigateTo("http://facebook.com/?sk=nf");

		driver.injectAmazeAsExtension("facebook.com", "/");

		logger.log(Level.INFO, "at the news feed");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	@Order(order = 1)
	public void findPostBox() {

		postbox = driver.findElement(By
				.cssSelector("#pagelet_composer textarea"));

	}

	private void genericTests(CharSequence... keys) {

		WebElement logo = driver.findElement(By.cssSelector("#pageLogo a"));

		logo.sendKeys(keys);

		WebElement active = driver.getActiveElement();

		// human-readable assertion
		assertEquals(PrettyPrint.element(postbox), PrettyPrint.element(active));
		assertEquals(postbox, active);

	}

	@Ignore
	@Test
	@Order(order = 2)
	public void altShiftP() {

		logger.log(Level.INFO, "Verifing ALT+SHIFT+P");
		genericTests(Keys.ALT, Keys.SHIFT, "P");

	}

	@Ignore
	@Test
	@Order(order = 3)
	public void ctrlP() {

		logger.log(Level.INFO, "Verifying CTRL+P");
		genericTests(Keys.CONTROL, "P");

	}
	
}
