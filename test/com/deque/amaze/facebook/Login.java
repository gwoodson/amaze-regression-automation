/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class Login {

	private static Logger logger = Logger.getLogger(Login.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		driver.navigateTo("http://facebook.com");

		driver.injectAmazeAsExtension("facebook.com", "/");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	/**
	 * Make an assertion that the sending a TAB key event on an element focuses
	 * the correct element
	 * 
	 * @param from
	 *            The element to send the TAB from
	 * @param to
	 *            The expected element to focus
	 */
	private static void assertTab(WebElement from, WebElement to) {

		from.sendKeys(Keys.TAB);

		// WebElement activeElement = driver.getActiveElement();

		WebElement activeElement = driver.getActiveElement();
		String expected = PrettyPrint.element(to);
		String actual = PrettyPrint.element(activeElement);

		logger.log(Level.INFO,
				String.format("%s -> TAB -> %s", expected, actual));

		assertEquals(expected, actual);
	}

	@Test
	@Order(order = 1)
	public void autoFocusEmail() {

		WebElement expected = driver.findElement(By.id("email"));
		WebElement activeElement = driver.getActiveElement();

		assertEquals(PrettyPrint.element(expected),
				PrettyPrint.element(activeElement));

	}

	@Test
	@Order(order = 2)
	public void tabEmailToPassword() {

		WebElement email = driver.findElement(By.id("email"));
		WebElement expected = driver.findElement(By.id("pass"));

		assertTab(email, expected);
	}

	@Test
	@Order(order = 3)
	public void tabPasswordToPersist() {

		WebElement password = driver.findElement(By.id("pass"));
		WebElement expected = driver.findElement(By.id("persist_box"));

		assertTab(password, expected);
	}

	@Test
	@Order(order = 4)
	public void tabPersistToForgot() {

		WebElement persist = driver.findElement(By.id("persist_box"));
		WebElement expected = driver.findElement(By
				.cssSelector("#login_form a[rel=\"nofollow\"]"));

		assertTab(persist, expected);

	}

	@Test
	@Order(order = 5)
	public void tabForgotToLogin() {

		WebElement forgot = driver.findElement(By
				.cssSelector("#login_form a[rel='nofollow']"));
		WebElement expected = driver.findElement(By
				.cssSelector("#loginbutton input"));

		assertTab(forgot, expected);

	}

	@Test
	@Order(order = 6)
	public void h1TabIndex() {

		WebElement h1 = driver.findElement(By.tagName("h1"));

		String tabIndex = h1.getAttribute("tabindex");

		assertEquals("-1", tabIndex);
	}

	@Test
	@Order(order = 7)
	public void tabLoginToH1() {

		WebElement login = driver.findElement(By
				.cssSelector("#loginbutton input"));
		WebElement expected = driver.findElement(By.tagName("h1"));

		assertTab(login, expected);

	}

	@Test
	@Ignore
	public void tabH1ToFirstname() {

		WebElement h1 = driver.findElement(By.tagName("h1"));
		WebElement expected = driver.findElement(By.id("firstname"));

		assertTab(h1, expected);

	}

}
