/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * TODO too much code duplicate between this regression and
 * {@link com.deque.amaze.facebook.PhotoModalTimeline}. There should a better
 * way of sharing code between the two.
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class PhotoModalNewsFeed {

	private static Logger logger = Logger.getLogger(PhotoModalNewsFeed.class
			.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	private static WebElement modalLink;
	private static WebElement modal;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		// ?sk=nf to force the news feed
		driver.navigateTo("http://facebook.com/");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		driver.navigateTo("http://facebook.com?sk=nf");

		driver.injectAmazeAsExtension("facebook.com", "/");

	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	@Order(order = 1)
	public void findPhotoLink() throws Exception {

		modalLink = driver.findElement(By
				.cssSelector("#contentCol a.uiPhotoThumb"));
	}

	@Test
	@Order(order = 2)
	public void linkOpensModal() throws Exception {

		//modalLink.click();
		modalLink.sendKeys(Keys.ENTER);
		

		modal = driver.waitForElementVisible(By.id("photos_snowlift"));

		logger.log(
				Level.INFO,
				String.format("modal is visible: %s",
						PrettyPrint.element(modal)));

	}

	@Test
	@Order(order = 3)
	public void escapeClosesModal() {

		modal.sendKeys(Keys.ESCAPE);

		modal = driver.waitForElementHidden(modal);

		logger.log(
				Level.INFO,
				String.format("modal is invisible: %s",
						PrettyPrint.element(modal)));

	}

	@Test
	@Order(order = 4)
	public void linkFocusedOnModalClose() throws Exception {

		// sleep, waiting for all necessary javascript to fire
		Thread.sleep(1000);

		WebElement active = driver.getActiveElement();

		// human-readable assertion
		assertEquals(PrettyPrint.element(modalLink),
				PrettyPrint.element(active));
		// deep assertion
		assertEquals(modalLink, active);
	}
}
