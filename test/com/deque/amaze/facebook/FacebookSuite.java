package com.deque.amaze.facebook;

import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.deque.amaze.regression.facebook.FacebookDriver;

/**
 * Suite runner. Uses {@link com.deque.amaze.facebook.FacebookDriver} to
 * interact with Selenium.
 * 
 * @author Greta Woodson <greta.woodson@deque.com>
 */
@SuppressWarnings({
	"deprecation"
})
@RunWith(Suite.class)
@SuiteClasses({

		Login.class, 
		MainSkipLink.class, 
//		PagesFeed.class, 
		ShareModal.class,
		SkipLink.class, 
		PhotoModalNewsFeed.class, 
		PhotoModalTimeline.class,
		TimelinePhotoSharesModal.class, 
		TimelinePhotoLikesModal.class,
		TimelineSharedModal.class, 
		Messages.class,
//		AccessKeyP.class,
		HelpModal.class,
		GenericAccessKeys.class,
		//EditProfile.class.	
		//ExpandChat.class

})
public class FacebookSuite extends TestBase {
	// must be public and static -- is used by all test cases
	public static FacebookDriver driver;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		Properties options = new Properties();
		options.load(FacebookSuite.class
				.getResourceAsStream("facebook.properties"));

		driver = new FacebookDriver(options);

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		//driver.getDriver().quit();

	}

}
