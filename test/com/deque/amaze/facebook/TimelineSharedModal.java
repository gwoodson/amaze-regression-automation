package com.deque.amaze.facebook;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.deque.amaze.SiteException;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

@RunWith(OrderedRunner.class)
public class TimelineSharedModal {
	
	private static Logger logger = Logger
			.getLogger(TimelineSharedModal.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;
	
	private static WebElement sharedarticle;
	private static WebElement sharedlink;
	
	private static List<WebElement> newsfeedList;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		//

		driver.navigateTo("http://facebook.com/");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		// get to the news feed
		driver.navigateTo("http://facebook.com?sk=nf");
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	@Order(order = 1)
	public void findNWLinks() throws Exception, InterruptedException {

		Thread.sleep(5000L);
		
		newsfeedList = NewsFeedUtility.findNewsfeedLinks(driver);	
			
	}
	
	@Test
	@Order(order = 2)
	public void findSharedArticleLink() throws Exception, InterruptedException {

		Thread.sleep(5000L);
		
		sharedarticle = NewsFeedUtility.findSharedNewsfeed(driver);
		
		logger.log(
				Level.INFO,
				String.format("Found sharedarticle : %s",
						PrettyPrint.element(sharedarticle)));
		
		sharedlink = sharedarticle.findElement(By.cssSelector("div.shareRedesign")).findElement(By.cssSelector("a.shareRedesignMedia"));
		
		String href = sharedlink.getAttribute("href");

		// bit of a hack here -- should probably use URL instead
		int slashIndex = href.lastIndexOf('/');
		String path = href.substring(slashIndex);

		logger.log(Level.INFO, String.format("found shared page at: %s", path));

		try {
			driver.navigateTo(href);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			driver.injectAmazeAsExtension("facebook.com", path);
		} catch (JSONException | IOException | SiteException
				| InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

}
