/**
 * 
 */
package com.deque.amaze.facebook;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.deque.amaze.SiteException;
import com.deque.amaze.regression.common.PrettyPrint;
import com.deque.amaze.regression.facebook.FacebookDriver;
import com.deque.amaze.regression.twitter.UseableTweet;
import com.deque.junit.Order;
import com.deque.junit.OrderedRunner;

/**
 * 
 * @author Stephen Mathieson <stephen.mathieson@deque.com>
 */
@RunWith(OrderedRunner.class)
public class TimelinePhotoLikesModal {

	private static Logger logger = Logger
			.getLogger(TimelinePhotoLikesModal.class.getName());

	private static FacebookDriver driver = FacebookSuite.driver;

	private static WebElement likesLink;
	private static WebElement photoLink;
		
	private static List<WebElement> newsfeedList;
	

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		//

		driver.navigateTo("http://facebook.com/");

		if (!driver.isLoggedIn()) {
			driver.login();
		}

		// get to the news feed
		driver.navigateTo("http://facebook.com?sk=nf");
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	@Order(order = 1)
	public void findNWLinks() throws Exception, InterruptedException {

		Thread.sleep(5000L);
		
		newsfeedList = NewsFeedUtility.findNewsfeedLinks(driver);	
			
	}
	
	@Test
	@Order(order = 2)
	public void findPhotoLink() throws Exception, InterruptedException {

		Thread.sleep(5000L);
		
		photoLink = NewsFeedUtility.isPhotoLink(driver);
		
		String href = photoLink.getAttribute("href");

		// bit of a hack here -- should probably use URL instead
		int slashIndex = href.lastIndexOf('/');
		String path = href.substring(slashIndex);

		logger.log(Level.INFO, String.format("found photo page at: %s", path));

		try {
			driver.navigateTo(href);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			driver.injectAmazeAsExtension("facebook.com", path);
		} catch (JSONException | IOException | SiteException
				| InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	@Order(order = 3)
	public void findLikesLink() throws Exception, InterruptedException {

		Thread.sleep(3000L);
		
		likesLink = NewsFeedUtility.getLikedLink(driver);
		
		logger.log(
				Level.INFO,
				String.format("Found summary likeslink : %s",
						PrettyPrint.element(likesLink)));
		
		likesLink.click();
		
	}

	@Ignore
	@Test
	@Order(order = 4)
	public void escapeClosesModal() throws Exception {

		// ESCAPE on the first anchor
		driver.findElement(By.tagName("a")).sendKeys(Keys.ESCAPE);

		// sleep so javascript can fire
		Thread.sleep(5000L);
	}
	
}
